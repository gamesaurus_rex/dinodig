//Maya ASCII 2018 scene
//Name: Pteranodon_Fossil.0002.ma
//Last modified: Wed, Mar 07, 2018 06:02:10 PM
//Codeset: 1252
requires maya "2018";
requires "stereoCamera" "10.0";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2018";
fileInfo "version" "2018";
fileInfo "cutIdentifier" "201706261615-f9658c4cfc";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "AD859A41-4908-A586-1404-FF96140C579B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 35.910524110323728 95.120832153748466 44.141568788510881 ;
	setAttr ".r" -type "double3" -73.538352729616378 -1.8000000000027203 -2.9832420637449188e-16 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "13FBACB5-493A-02C8-03E5-0DBBE5CD0C01";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 91.226228777625579;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 36.722526550292969 7.6339998245239258 18.303232192993164 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "FDEA9B5A-4029-EA51-54D5-9DA2556C93A9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 41.864240423388047 1007.2413287066757 23.41887390346853 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "CC49A4DF-4ACF-F8B0-964A-E686ADA9F5F4";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.10732888215182;
	setAttr ".ow" 68.596977325190835;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 36.855498313903809 8.1339998245239258 17.363855838775635 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "41197363-4B5A-4401-B375-D98FCD223377";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "03B88BA7-40F5-5428-8E49-6CB976F40B76";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "97AA5F14-4CB5-9C35-456F-D69D2A43896D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E43D5728-43CB-2713-B7E7-32A6FBB16795";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pPlane1";
	rename -uid "A1AE8510-4691-9346-6410-2A81A0A61383";
	setAttr ".t" -type "double3" 40 2 29.452464920314419 ;
	setAttr ".s" -type "double3" 56.4 1 37.4 ;
createNode mesh -n "pPlaneShape1" -p "pPlane1";
	rename -uid "EE08094E-423E-6581-E37F-03BB69C5851E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "TileFloor";
	rename -uid "48CCA379-49B5-CD99-37C4-BAA1B66EC0E7";
createNode transform -n "pCube1" -p "TileFloor";
	rename -uid "E7535696-4FB6-5D55-6BFD-CA87B671A80E";
	setAttr ".t" -type "double3" -5 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "E350DAED-44B0-7B87-4E5F-1F9FC06BCF42";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2" -p "TileFloor";
	rename -uid "2B6EFCD3-4BAC-98F1-B6C9-9692121B183F";
	setAttr ".t" -type "double3" 5 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "923620E5-4606-F48D-787B-17A8FDABFAD4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3" -p "TileFloor";
	rename -uid "2E219804-4D4E-5E92-CF00-44BB13937E16";
	setAttr ".t" -type "double3" 15 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "0EAA1651-47C9-BFBC-F69E-C3AB51727624";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube4" -p "TileFloor";
	rename -uid "870D30A6-45B2-8734-83C0-2F96100FE2AA";
	setAttr ".t" -type "double3" 25 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	rename -uid "4DD28A66-462E-DD48-6A88-92999A974AEC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5" -p "TileFloor";
	rename -uid "EBB0CF06-417E-1347-AB9C-9BB6D3BAB892";
	setAttr ".t" -type "double3" 35 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "5B05793C-4377-BD73-13EF-8E887FF0B75B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube6" -p "TileFloor";
	rename -uid "30A7C5E7-4464-68C6-DDE8-789A0DB580AC";
	setAttr ".t" -type "double3" 45 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "A404EEE4-463E-5E86-07E2-0499A1A0917E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube7" -p "TileFloor";
	rename -uid "CA63AC3E-4277-BE86-80C1-33B5E4E84D74";
	setAttr ".t" -type "double3" 55 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	rename -uid "CF9A7B62-4B00-65EC-DC3E-0D84C30B7CA1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube8" -p "TileFloor";
	rename -uid "697CDF7F-46B6-03C2-C712-65A0A6A51F30";
	setAttr ".t" -type "double3" 65 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	rename -uid "A666D311-452B-AA26-E06B-DE8088D82FF8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube9" -p "TileFloor";
	rename -uid "F734F235-4B34-5CFD-989B-7A9CB2116698";
	setAttr ".t" -type "double3" 75 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape9" -p "pCube9";
	rename -uid "5BE0A48A-4583-5A30-BE99-79B87B42F76A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube10" -p "TileFloor";
	rename -uid "64362CCA-4017-5C90-3EEA-58826B73588A";
	setAttr ".t" -type "double3" 85 0 0 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape10" -p "pCube10";
	rename -uid "8FF8D79B-4401-30C6-1A78-A2A362D8983C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube11" -p "TileFloor";
	rename -uid "B1D01A85-4681-FD53-C9F2-21895F44EFA9";
	setAttr ".t" -type "double3" -5 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape11" -p "pCube11";
	rename -uid "A0F753B8-4F5D-065A-AAAF-EDB80CF95DAD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube12" -p "TileFloor";
	rename -uid "D2614D7E-4BA9-1C57-2304-E897A006A6B3";
	setAttr ".t" -type "double3" 5 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape12" -p "pCube12";
	rename -uid "EF2EE2A3-46B6-C0E8-F778-6BBAC7851A98";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube13" -p "TileFloor";
	rename -uid "C23672B6-4552-7DD2-8918-2E9BE32C5E9C";
	setAttr ".t" -type "double3" 15 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape13" -p "pCube13";
	rename -uid "AC0D6FA9-4658-E32D-CDCB-5BB0433F24BD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube14" -p "TileFloor";
	rename -uid "9B613E34-458D-461E-E328-4A9B09EED28D";
	setAttr ".t" -type "double3" 25 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape14" -p "pCube14";
	rename -uid "0C25E725-4F11-DC04-9C3E-329C6BCB94A1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube15" -p "TileFloor";
	rename -uid "7043C851-4590-07BC-4088-E1B202A42ADD";
	setAttr ".t" -type "double3" 35 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape15" -p "pCube15";
	rename -uid "15453F72-43FF-B1F9-EA62-35B1793475C1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube16" -p "TileFloor";
	rename -uid "DBB859C1-4110-827A-B919-65981F97EF08";
	setAttr ".t" -type "double3" 45 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape16" -p "pCube16";
	rename -uid "27FCFEDD-4722-D62C-EA5C-B292DF52F5E2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube17" -p "TileFloor";
	rename -uid "AA965E14-409A-86C2-A44B-31A43B8C9423";
	setAttr ".t" -type "double3" 55 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape17" -p "pCube17";
	rename -uid "2064FCFD-4B57-14AB-AB59-42B6258F8422";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube18" -p "TileFloor";
	rename -uid "3B931A02-4E1A-33E3-6772-C6A06305F9FF";
	setAttr ".t" -type "double3" 65 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape18" -p "pCube18";
	rename -uid "D9524A54-4820-171B-B6D1-258524F6EAC6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube19" -p "TileFloor";
	rename -uid "FFDB931D-4B89-AE4A-4584-D7B41ED6E1CF";
	setAttr ".t" -type "double3" 75 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape19" -p "pCube19";
	rename -uid "F2C6B0A3-4BFC-0186-EC83-ED8059A1E128";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube20" -p "TileFloor";
	rename -uid "99545EAC-4EDA-5995-E842-A0B6088314EF";
	setAttr ".t" -type "double3" 85 0 10 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape20" -p "pCube20";
	rename -uid "964E7417-4425-BC6C-37AC-9F8BBFEB17A1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube21" -p "TileFloor";
	rename -uid "90F84CF5-40C7-53B0-E6B5-ACAFE2B38A3C";
	setAttr ".t" -type "double3" -5 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape21" -p "pCube21";
	rename -uid "59487739-42C5-6516-EC90-948463865919";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube22" -p "TileFloor";
	rename -uid "4C35D87F-4236-ED81-2E48-1AB15658A809";
	setAttr ".t" -type "double3" 5 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape22" -p "pCube22";
	rename -uid "56B7F251-4B87-9A49-D318-BA9FDF560695";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube23" -p "TileFloor";
	rename -uid "5725336B-403D-851D-2DA5-EE99A058326A";
	setAttr ".t" -type "double3" 15 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape23" -p "pCube23";
	rename -uid "EB17BD8C-4471-16F1-AD77-C08E55141706";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube24" -p "TileFloor";
	rename -uid "FE52D532-4035-4310-31A3-8890393082F6";
	setAttr ".t" -type "double3" 25 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape24" -p "pCube24";
	rename -uid "BD660D69-4162-54AD-69AC-8CBA35736BD1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube25" -p "TileFloor";
	rename -uid "62604A2E-42F5-8D1D-C995-98BE5EE3247A";
	setAttr ".t" -type "double3" 35 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape25" -p "pCube25";
	rename -uid "F407C7FB-4454-E463-FE6D-F9A694F8BACE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube26" -p "TileFloor";
	rename -uid "3C5DC64D-4607-B330-CEE5-5BBCC7AB3620";
	setAttr ".t" -type "double3" 45 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape26" -p "pCube26";
	rename -uid "A5A1BA28-421B-1D08-72CA-60A0FDA4CE9B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube27" -p "TileFloor";
	rename -uid "92D41571-4FC5-2F61-8650-ADB25B1B8CFE";
	setAttr ".t" -type "double3" 55 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape27" -p "pCube27";
	rename -uid "21E82F09-4504-829E-8C0F-DBB07E98FDB8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube28" -p "TileFloor";
	rename -uid "C9991AF4-4BB9-4B46-4FFF-00ABCDE45E09";
	setAttr ".t" -type "double3" 65 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape28" -p "pCube28";
	rename -uid "98FC04AA-463D-9D46-3597-D49995E8488D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube29" -p "TileFloor";
	rename -uid "FB31D29F-4755-4E33-27EF-1C81097C9135";
	setAttr ".t" -type "double3" 75 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape29" -p "pCube29";
	rename -uid "16BFC4ED-4925-A801-CDAA-7284516DE839";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube30" -p "TileFloor";
	rename -uid "9A3ED659-4B29-E404-853C-B0949B3931ED";
	setAttr ".t" -type "double3" 85 0 20 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape30" -p "pCube30";
	rename -uid "C913C449-4747-4F08-FB07-5D92BF6C2E3E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube31" -p "TileFloor";
	rename -uid "8ACAE049-410F-4989-C2EE-8DB20AF6C93D";
	setAttr ".t" -type "double3" -5 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape31" -p "pCube31";
	rename -uid "8B1A7109-4DE8-B69F-C5EC-7D8CA103D7F7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube32" -p "TileFloor";
	rename -uid "2B364E5C-41B0-ED65-A63D-109608824586";
	setAttr ".t" -type "double3" 5 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape32" -p "pCube32";
	rename -uid "B77EA5F2-454B-9AB6-2807-70A37AFB0701";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube33" -p "TileFloor";
	rename -uid "2463B0C6-4B88-B7C5-C498-D9AB1F1968F1";
	setAttr ".t" -type "double3" 15 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape33" -p "pCube33";
	rename -uid "7FD6F14C-4EED-EDF0-EAAC-1285130700A8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube34" -p "TileFloor";
	rename -uid "A39E0975-41C4-AA8D-5CB5-8192E6719BD9";
	setAttr ".t" -type "double3" 25 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape34" -p "pCube34";
	rename -uid "E0BDBC7D-4DED-613C-2A46-048CB60AAAFE";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube35" -p "TileFloor";
	rename -uid "4C915395-4BD4-E306-9515-73ABAB591B74";
	setAttr ".t" -type "double3" 35 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape35" -p "pCube35";
	rename -uid "33FD287F-499F-2EE8-4F01-D08123364CEC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube36" -p "TileFloor";
	rename -uid "CC299D41-4AC2-B893-0F56-5EBB1DA64652";
	setAttr ".t" -type "double3" 45 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape36" -p "pCube36";
	rename -uid "3EE92F0A-4651-4AA7-2A74-DFAC6CE19092";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube37" -p "TileFloor";
	rename -uid "0BF113BB-412B-D1F9-B1ED-84A8E33C5E25";
	setAttr ".t" -type "double3" 55 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape37" -p "pCube37";
	rename -uid "8B251FEA-4C93-A388-4A21-3BB8A834D4C4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube38" -p "TileFloor";
	rename -uid "004AAEF7-4E60-3B2B-9B4A-BCB19BB326AD";
	setAttr ".t" -type "double3" 65 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape38" -p "pCube38";
	rename -uid "6FBF9507-426A-6BFE-B22E-B59D97F2781C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube39" -p "TileFloor";
	rename -uid "3B62B051-4CD8-E92D-FA2F-ED925D94E833";
	setAttr ".t" -type "double3" 75 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape39" -p "pCube39";
	rename -uid "AD031B90-406A-89F9-C4D8-76B46A2F8863";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube40" -p "TileFloor";
	rename -uid "93B1B0DB-45D3-8046-80BB-869BAB8A4299";
	setAttr ".t" -type "double3" 85 0 30 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape40" -p "pCube40";
	rename -uid "443C9B9C-472A-E806-636F-23BAA416080A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube41" -p "TileFloor";
	rename -uid "C4859E9E-4C8C-98CA-0CDE-2BBAF093F9B0";
	setAttr ".t" -type "double3" -5 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape41" -p "pCube41";
	rename -uid "D4EBC2A1-4D9C-0F22-C190-F38CAE425F44";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube42" -p "TileFloor";
	rename -uid "46343EEF-441A-75AA-2454-F9A5F489C542";
	setAttr ".t" -type "double3" 5 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape42" -p "pCube42";
	rename -uid "B1DE3962-4A6B-7FB8-A9EB-46BE7A4DFD11";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube43" -p "TileFloor";
	rename -uid "D79A335C-45F9-94CB-F0AC-A3ADE8B9FB33";
	setAttr ".t" -type "double3" 15 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape43" -p "pCube43";
	rename -uid "C24D41D3-4D6D-1F55-1C4B-A9A14E150D1D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube44" -p "TileFloor";
	rename -uid "8EFD439F-4FD8-F86E-30A4-1AA5CF0B5C79";
	setAttr ".t" -type "double3" 25 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape44" -p "pCube44";
	rename -uid "46D0D47C-4EAB-2981-2552-DE9EE4EF8484";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube45" -p "TileFloor";
	rename -uid "1FCE1144-470A-D303-0055-C2BAE430C236";
	setAttr ".t" -type "double3" 35 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape45" -p "pCube45";
	rename -uid "8B295A8A-4B78-B30B-E643-C6AD65C3C7F0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube46" -p "TileFloor";
	rename -uid "BEC36894-4ED2-4171-CC93-46B48A2267F5";
	setAttr ".t" -type "double3" 45 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape46" -p "pCube46";
	rename -uid "6425A4D5-4983-85B1-5D76-E89349353702";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube47" -p "TileFloor";
	rename -uid "CC33E113-429D-6D93-AF14-4881AE284A0D";
	setAttr ".t" -type "double3" 55 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape47" -p "pCube47";
	rename -uid "28CA754A-4268-86A9-979A-0AB2530D1941";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube48" -p "TileFloor";
	rename -uid "A3056835-455B-6A28-2C02-07B5088B86D5";
	setAttr ".t" -type "double3" 65 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape48" -p "pCube48";
	rename -uid "482249E0-4B96-34BF-F200-08BAA7B350E9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube49" -p "TileFloor";
	rename -uid "D4BE303F-4426-622B-013F-A1B6DA5C54EF";
	setAttr ".t" -type "double3" 75 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape49" -p "pCube49";
	rename -uid "66A6AD08-46B2-D208-C2E3-06B0498E994C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube50" -p "TileFloor";
	rename -uid "00621437-4D82-F48C-5567-61976B83EA54";
	setAttr ".t" -type "double3" 85 0 40 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape50" -p "pCube50";
	rename -uid "D04FA550-42A8-1B24-3C71-5E826A744D1A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube51" -p "TileFloor";
	rename -uid "ECCEA2B5-46DF-95C7-1ACE-659CB91E2F6E";
	setAttr ".t" -type "double3" -5 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape51" -p "pCube51";
	rename -uid "2C9CB500-4D45-BBD3-34B7-8687E805B040";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube52" -p "TileFloor";
	rename -uid "FCE22B53-432B-DDF6-57C2-3BA2348C06EC";
	setAttr ".t" -type "double3" 5 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape52" -p "pCube52";
	rename -uid "55B1CAAE-40C0-95F3-22CB-8A9247588B4E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube53" -p "TileFloor";
	rename -uid "FAA5E22D-4446-E9F4-C2B4-5586D5D85E8E";
	setAttr ".t" -type "double3" 15 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape53" -p "pCube53";
	rename -uid "0C133809-4B52-2F17-343F-CBBE748D44A0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube54" -p "TileFloor";
	rename -uid "403B4D52-4F4F-F6C4-E94B-64BB2CF79EF2";
	setAttr ".t" -type "double3" 25 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape54" -p "pCube54";
	rename -uid "4F7D7723-4A10-A088-B339-CC849678C46C";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube55" -p "TileFloor";
	rename -uid "E47C6C49-4F9A-1713-2627-0F9D0C637407";
	setAttr ".t" -type "double3" 35 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape55" -p "pCube55";
	rename -uid "9FA5668A-495A-4726-C98F-8F8C51E5F7CF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube56" -p "TileFloor";
	rename -uid "B1728BB2-4321-2142-7CE8-0BAC38FC67E5";
	setAttr ".t" -type "double3" 45 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape56" -p "pCube56";
	rename -uid "85DE9E16-43FB-BBD6-EA26-6BAEEAEC8242";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube57" -p "TileFloor";
	rename -uid "F7BBB6D2-4018-DA8D-AA13-2F8A6A5DFBEA";
	setAttr ".t" -type "double3" 55 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape57" -p "pCube57";
	rename -uid "A0570AD3-4643-D17D-E7FB-ACA875B14D26";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube58" -p "TileFloor";
	rename -uid "ED42FD1C-42FE-8427-2D73-A4BBED2ACF25";
	setAttr ".t" -type "double3" 65 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape58" -p "pCube58";
	rename -uid "6CD601F9-4F7D-98AC-0EF3-8E8BDEFC879B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube59" -p "TileFloor";
	rename -uid "2196F9A1-4CD2-A90A-88BB-4EB54939B5D2";
	setAttr ".t" -type "double3" 75 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape59" -p "pCube59";
	rename -uid "67BA88C4-4183-A8CF-0698-209EC0D5146E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube60" -p "TileFloor";
	rename -uid "F18E177A-4F69-C958-A07F-0E954DB1F95F";
	setAttr ".t" -type "double3" 85 0 50 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape60" -p "pCube60";
	rename -uid "EFCF4314-40A4-7C96-E769-DD968334FC3F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube61" -p "TileFloor";
	rename -uid "0CE7FA05-467E-7173-A50A-BCBE30102D30";
	setAttr ".t" -type "double3" -5 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape61" -p "pCube61";
	rename -uid "3CD00302-420A-B9D4-A167-9A9CF69EC961";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube62" -p "TileFloor";
	rename -uid "CF7ED847-40D1-AB6D-5513-C88F1BDCB38C";
	setAttr ".t" -type "double3" 5 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape62" -p "pCube62";
	rename -uid "03C42B8B-42AE-54F2-E305-008BF7100AF5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube63" -p "TileFloor";
	rename -uid "19FEC387-414B-49CB-9176-DCBE5103847F";
	setAttr ".t" -type "double3" 15 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape63" -p "pCube63";
	rename -uid "3BC15135-4F08-843D-E175-3A9775BD6397";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube64" -p "TileFloor";
	rename -uid "F6728A1F-4C0C-36CE-F149-4D911FD1FE46";
	setAttr ".t" -type "double3" 25 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape64" -p "pCube64";
	rename -uid "4D66EC2B-42AB-8FF9-A6E7-FC849C507EE9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube65" -p "TileFloor";
	rename -uid "AE5015B1-4BE2-66D1-3500-A6A4E0A946BD";
	setAttr ".t" -type "double3" 35 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape65" -p "pCube65";
	rename -uid "0002108C-420A-C89A-DE12-0EA3EBC66F7F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube66" -p "TileFloor";
	rename -uid "1EFA7844-4FED-2042-A3B2-0186138790C4";
	setAttr ".t" -type "double3" 45 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape66" -p "pCube66";
	rename -uid "B1F43402-495F-DC5D-A3CF-D48BA72AF594";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube67" -p "TileFloor";
	rename -uid "62F0DD0A-45C2-AD88-53D6-7294B16BE2D7";
	setAttr ".t" -type "double3" 55 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape67" -p "pCube67";
	rename -uid "FA7F7807-45EB-5DE4-3118-208952BBF89D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube68" -p "TileFloor";
	rename -uid "D243FCEB-48BA-FF1B-F917-EBA382A026A0";
	setAttr ".t" -type "double3" 65 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape68" -p "pCube68";
	rename -uid "CB0381F0-4DB5-1457-3F13-8688FDE6FBAF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube69" -p "TileFloor";
	rename -uid "1A67F2A6-4E89-5DAC-1C8D-35A033B44881";
	setAttr ".t" -type "double3" 75 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape69" -p "pCube69";
	rename -uid "D5BA251E-476B-4678-1F41-70ADB875F4EF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube70" -p "TileFloor";
	rename -uid "51EF0C6C-4FAC-31D9-DCAE-E8B785F2ACF2";
	setAttr ".t" -type "double3" 85 0 60 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape70" -p "pCube70";
	rename -uid "52690135-4660-700B-99E2-1096B5DFB16F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube71" -p "TileFloor";
	rename -uid "040FD2C6-4E2E-A147-DAB5-D1A99674D293";
	setAttr ".t" -type "double3" -5 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape71" -p "pCube71";
	rename -uid "8D16ACFF-4881-481E-9758-E3B2E3DBA0F7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube72" -p "TileFloor";
	rename -uid "63996623-4A33-0053-87E8-918D0149E1DC";
	setAttr ".t" -type "double3" 5 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape72" -p "pCube72";
	rename -uid "1F399BA9-4539-B5D1-8144-78BDC49305A8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube73" -p "TileFloor";
	rename -uid "A27C1A74-49A4-563D-7A1C-E2BE573733EE";
	setAttr ".t" -type "double3" 15 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape73" -p "pCube73";
	rename -uid "1DC2D806-49BB-7267-282A-94900FD18D84";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube74" -p "TileFloor";
	rename -uid "9701A81A-4A81-F39E-5806-3F957AD03CA6";
	setAttr ".t" -type "double3" 25 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape74" -p "pCube74";
	rename -uid "7FCFE3AB-4E58-9614-AD80-67978CD9EE38";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube75" -p "TileFloor";
	rename -uid "7CAA9979-4B57-2DFA-E4FD-E5943C9C9017";
	setAttr ".t" -type "double3" 35 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape75" -p "pCube75";
	rename -uid "8A4F2F14-4E07-3FBA-A3DC-399A5B8DD930";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube76" -p "TileFloor";
	rename -uid "45FE3F83-4EF3-9FD0-8617-59A603F09279";
	setAttr ".t" -type "double3" 45 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape76" -p "pCube76";
	rename -uid "1AD86423-42FF-3CF3-1C9C-F1BB8DC61B12";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube77" -p "TileFloor";
	rename -uid "C36BF397-46EC-662E-CED7-1BBA2CE308DD";
	setAttr ".t" -type "double3" 55 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape77" -p "pCube77";
	rename -uid "23FC41B9-42F7-DD4C-DBA9-5BAD20926489";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube78" -p "TileFloor";
	rename -uid "A0D736BD-4FDF-E704-2BB0-F68655D2830E";
	setAttr ".t" -type "double3" 65 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape78" -p "pCube78";
	rename -uid "3B2846FB-4444-42E2-9BC9-79A6E1046698";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube79" -p "TileFloor";
	rename -uid "C4E595EA-41B0-F484-8C45-E89B8DECF660";
	setAttr ".t" -type "double3" 75 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape79" -p "pCube79";
	rename -uid "DAC36313-42B4-0BB9-678F-5098C76B43A8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube80" -p "TileFloor";
	rename -uid "2D4639B4-4928-139D-455E-D8B9BEDA30A1";
	setAttr ".t" -type "double3" 85 0 70 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape80" -p "pCube80";
	rename -uid "DB8E7199-4135-0347-5592-E8B70ADB71F6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube81" -p "TileFloor";
	rename -uid "4B8CFD06-4FC2-9530-6656-249C0273F238";
	setAttr ".t" -type "double3" 85 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape81" -p "pCube81";
	rename -uid "758C1753-471C-7F36-D395-30B3A6ED9526";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube82" -p "TileFloor";
	rename -uid "88E53231-4342-8133-58AA-8CA3E1E04B68";
	setAttr ".t" -type "double3" 75 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape82" -p "pCube82";
	rename -uid "92B1999E-49F6-4516-0435-14868C40F579";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube83" -p "TileFloor";
	rename -uid "77963F02-4DF7-BF38-EF4B-AABE1EE6BFF9";
	setAttr ".t" -type "double3" 65 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape83" -p "pCube83";
	rename -uid "F6837BF4-4FE9-EE52-BFDB-4E9717918B00";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube84" -p "TileFloor";
	rename -uid "A6545A8C-4EE9-AF67-75BD-DFB865637AE0";
	setAttr ".t" -type "double3" 55 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape84" -p "pCube84";
	rename -uid "2DD5128A-402D-A28A-6083-EAB13BBA57AD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube85" -p "TileFloor";
	rename -uid "E977FC50-4F2F-3641-E195-76B6F884E6A0";
	setAttr ".t" -type "double3" 45 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape85" -p "pCube85";
	rename -uid "4BD7BBE8-4557-0E18-D577-BA9B05634F2B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube86" -p "TileFloor";
	rename -uid "7BE939DD-437E-C61A-EE36-C9BB143EDAD9";
	setAttr ".t" -type "double3" 35 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape86" -p "pCube86";
	rename -uid "277BB573-4E1C-5AD6-F17C-249E4461264E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube87" -p "TileFloor";
	rename -uid "8C1F3143-44AE-96AD-FE1E-FC91066D4C17";
	setAttr ".t" -type "double3" 25 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape87" -p "pCube87";
	rename -uid "7A66EC11-4B2A-ADF2-8A1F-A79FECF3480E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube88" -p "TileFloor";
	rename -uid "6BB20166-4671-3A1B-4D14-BF9C92F5CE1B";
	setAttr ".t" -type "double3" 15 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape88" -p "pCube88";
	rename -uid "878280EE-44E1-73AC-0824-C492580C62C5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube89" -p "TileFloor";
	rename -uid "73C4762E-45CF-08A4-8C0A-4DAD715303AA";
	setAttr ".t" -type "double3" 5 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape89" -p "pCube89";
	rename -uid "2E8E0236-447D-BAC3-2BE2-40BDA7850D58";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube90" -p "TileFloor";
	rename -uid "AEFDD884-4C28-D6BF-B23E-1BBCD2DD734C";
	setAttr ".t" -type "double3" -5 0 80 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape90" -p "pCube90";
	rename -uid "6CA7AF44-47A2-D22D-00FD-81A667DB57AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube91" -p "TileFloor";
	rename -uid "FCDDA016-4DF6-EA8D-DA1D-8581171F406D";
	setAttr ".t" -type "double3" 85 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape91" -p "pCube91";
	rename -uid "F0261D42-4DCC-B50D-6EB2-F1BCADC8485F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube92" -p "TileFloor";
	rename -uid "3FC93A48-46DE-CEC4-7ECC-55B6DDDE777A";
	setAttr ".t" -type "double3" 75 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape92" -p "pCube92";
	rename -uid "8D105387-4767-9729-98D1-87A2E087E6BB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube93" -p "TileFloor";
	rename -uid "B1A1EE42-4F47-FA2B-EEE0-83A0BE175E0B";
	setAttr ".t" -type "double3" 65 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape93" -p "pCube93";
	rename -uid "3AD7233C-48B5-E3F0-BE60-849CA2106E2E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube94" -p "TileFloor";
	rename -uid "BC93990C-4287-235B-6975-DF8AD375119A";
	setAttr ".t" -type "double3" 55 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape94" -p "pCube94";
	rename -uid "25F7A1FE-4B0C-8441-B630-E4A9A0B3428A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube95" -p "TileFloor";
	rename -uid "2AE191A1-4168-4729-2599-54B2575E537B";
	setAttr ".t" -type "double3" 45 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape95" -p "pCube95";
	rename -uid "36F61BD3-41B4-9AA7-8C91-7EA1F1FD8030";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube96" -p "TileFloor";
	rename -uid "DF70FFF0-4179-6D9F-5620-1A90E93EBCB1";
	setAttr ".t" -type "double3" 35 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape96" -p "pCube96";
	rename -uid "7E3EED5C-4F08-24F7-516D-D0B649276F72";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube97" -p "TileFloor";
	rename -uid "30AA2653-4C39-6D12-09DA-1AA43B06B3FE";
	setAttr ".t" -type "double3" 25 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape97" -p "pCube97";
	rename -uid "6DA23603-4ED2-1DB2-7033-1F912F4E82AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube98" -p "TileFloor";
	rename -uid "ADBADD92-43C0-EF5E-A9B2-D5B04B323A67";
	setAttr ".t" -type "double3" 15 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape98" -p "pCube98";
	rename -uid "B34C0BFE-4404-FB5E-B25E-7B961D01F7D3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube99" -p "TileFloor";
	rename -uid "3C88AC07-4BB5-E134-ABA0-CE85A2435C99";
	setAttr ".t" -type "double3" 5 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape99" -p "pCube99";
	rename -uid "4E889442-4162-8BC6-0C77-32AF410018EC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube100" -p "TileFloor";
	rename -uid "FDE0D290-4FA4-801B-5744-42A37EC3B5EE";
	setAttr ".t" -type "double3" -5 0 90 ;
	setAttr ".s" -type "double3" 10 1 10 ;
createNode mesh -n "pCubeShape100" -p "pCube100";
	rename -uid "F646F464-4C7D-0C2B-8A2C-C4BAE54537EB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube101";
	rename -uid "C5E99161-4CEF-6B34-995C-DFB5075674B8";
	setAttr ".t" -type "double3" 37.121340891327158 8.1339380822017979 24.156188993103534 ;
	setAttr ".r" -type "double3" 0 29.222544078811005 0 ;
	setAttr ".s" -type "double3" 2.7207298908439737 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape101" -p "pCube101";
	rename -uid "39EE90E5-429B-B429-1E42-CEB6A03D2B01";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.53135472536087036 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[24:27]" -type "float3"  -0.025162265 0 -0.0051645376 
		-0.025162265 0 -0.0051645376 -0.025162265 0 0.21697371 -0.025162265 0 0.21697371;
	setAttr ".dr" 1;
createNode transform -n "pCube102";
	rename -uid "A2743C4D-4250-ABFA-8219-FE9D3552B1F5";
	setAttr ".t" -type "double3" 33.841509801640377 8.1339380822017979 23.883535447665125 ;
	setAttr ".r" -type "double3" 0 -29.292628582577844 0 ;
	setAttr ".s" -type "double3" 4.1375860591821416 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape102" -p "pCube102";
	rename -uid "AED86EF2-4E39-2614-876B-C6BE9DDF385B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube103";
	rename -uid "F5437885-44D4-69C0-48FD-15907E7DED81";
	setAttr ".t" -type "double3" 29.462966284993581 8.1339380822017979 21.126299131368977 ;
	setAttr ".r" -type "double3" 0 -35.15935952799078 0 ;
	setAttr ".s" -type "double3" 5.7497657001350531 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape103" -p "pCube103";
	rename -uid "69AF88B0-4321-6D50-728E-66A25C6AC7B1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.38375362753868103 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape4" -p "pCube103";
	rename -uid "1FB22256-4575-0125-A819-7882A3594313";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube104";
	rename -uid "83E41058-4DC3-C8B5-83BF-53B8D6596E63";
	setAttr ".t" -type "double3" 23.596073577062985 8.1339380822017979 20.767000728287041 ;
	setAttr ".r" -type "double3" 0 19.666313595861038 0 ;
	setAttr ".s" -type "double3" 6.7473682163431556 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape104" -p "pCube104";
	rename -uid "B3371629-4734-38DA-C36A-AE81092FAD11";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube105";
	rename -uid "24DE1E87-409C-68E4-FD95-DCA6DD3822FE";
	setAttr ".t" -type "double3" 17.818607866397329 8.1339380822017979 23.958831942424915 ;
	setAttr ".r" -type "double3" 0 40.960087781038709 0 ;
	setAttr ".s" -type "double3" 6.1785061976341078 1 0.38884489852227727 ;
createNode mesh -n "pCubeShape105" -p "pCube105";
	rename -uid "4935C934-40DD-04A0-841E-0F97146144B7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube106";
	rename -uid "5DC1CC05-40CE-4DBC-FCBD-73BA2DE6243E";
	setAttr ".t" -type "double3" 14.00854066427762 8.1339380822017979 27.697682935159214 ;
	setAttr ".r" -type "double3" 0 49.588563073186563 0 ;
	setAttr ".s" -type "double3" 4.0890865517501016 1 0.38884489852227727 ;
createNode mesh -n "pCubeShape106" -p "pCube106";
	rename -uid "FF70B00E-473E-4C13-E980-93ADC99E6C34";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube107";
	rename -uid "186C3F63-4C3A-93D9-C711-D68796A4AF3A";
	setAttr ".t" -type "double3" 11.907662487407872 8.1339380822017979 30.475115101190404 ;
	setAttr ".r" -type "double3" 0 59.38305955975656 0 ;
	setAttr ".s" -type "double3" 2.7414358341946237 1 0.38884489852227727 ;
createNode mesh -n "pCubeShape107" -p "pCube107";
	rename -uid "491FA2BF-4879-46DE-3F68-55A85A3A89AB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape3" -p "pCube107";
	rename -uid "3EAF4772-4DCC-6314-79F0-CE9AD24DD7F9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.25 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 -0.29778957 ;
	setAttr ".pt[2]" -type "float3" 0 0 -0.29778957 ;
	setAttr ".pt[4]" -type "float3" 0 0 0.29778957 ;
	setAttr ".pt[6]" -type "float3" 0 0 0.29778957 ;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube108";
	rename -uid "DBDEC35D-4F0E-7B07-766A-CBA5611032E2";
	setAttr ".t" -type "double3" 68.356030282481854 8.1339380822017979 30.801219831522857 ;
	setAttr ".r" -type "double3" 0 -56.930379755953489 0 ;
	setAttr ".s" -type "double3" 2.7414358341946237 1 0.38884489852227727 ;
createNode mesh -n "pCubeShape108" -p "pCube108";
	rename -uid "EB3B1761-44C4-D5D3-134A-3DB19E8B9F61";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape2" -p "pCube108";
	rename -uid "54628904-47EF-C81C-DC52-65B056E450BD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.75 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0 -0.36367661 ;
	setAttr ".pt[3]" -type "float3" 0 0 -0.36367661 ;
	setAttr ".pt[5]" -type "float3" 0 0 0.36367658 ;
	setAttr ".pt[7]" -type "float3" 0 0 0.36367661 ;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube109";
	rename -uid "9648AD82-4079-9A1A-2455-72A978DB3D3B";
	setAttr ".t" -type "double3" 66.228753124397628 8.1339380822017979 27.927693226060896 ;
	setAttr ".r" -type "double3" 0 -50.558136882431526 0 ;
	setAttr ".s" -type "double3" 4.0890865517501016 1 0.38884489852227727 ;
createNode mesh -n "pCubeShape109" -p "pCube109";
	rename -uid "64527783-4684-483E-3AE4-4CB10F5F4C2F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube110";
	rename -uid "AB96057E-4B71-7BA9-280B-77AFFD26CAA6";
	setAttr ".t" -type "double3" 62.447359611486377 8.1339380822017979 24.236889453041965 ;
	setAttr ".r" -type "double3" 0 -39.976242118468058 0 ;
	setAttr ".s" -type "double3" 6.1785061976341078 1 0.38884489852227727 ;
createNode mesh -n "pCubeShape110" -p "pCube110";
	rename -uid "E07CC820-45C9-EB19-EC68-DEA1D4E00E07";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube111";
	rename -uid "1FCAE86F-4A85-3291-434D-41AD4393CDCD";
	setAttr ".t" -type "double3" 56.597398151028635 8.1339380822017979 20.900916579757936 ;
	setAttr ".r" -type "double3" 0 -19.861242275815012 0 ;
	setAttr ".s" -type "double3" 6.7473682163431556 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape111" -p "pCube111";
	rename -uid "B58F0E6C-4C24-0F54-89EF-FBACFEEC7E70";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube113";
	rename -uid "A44D5090-4639-B42A-7C2C-909743A9EC2A";
	setAttr ".t" -type "double3" 46.422765996567009 8.1339380822017979 23.969404079420634 ;
	setAttr ".r" -type "double3" 0 26.058950869386983 0 ;
	setAttr ".s" -type "double3" 4.1375860591821416 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape113" -p "pCube113";
	rename -uid "7D96707A-44C9-C096-FD42-3B958F730C81";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube114";
	rename -uid "19CA42F4-4D21-4F0A-D49B-21836C7ADC54";
	setAttr ".t" -type "double3" 43.060645657123281 8.1339380822017979 24.212521676713546 ;
	setAttr ".r" -type "double3" 0 -31.500317508241213 0 ;
	setAttr ".s" -type "double3" 2.9015505373896762 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape114" -p "pCube114";
	rename -uid "D642DDB2-42A5-0251-D1C6-CA859664C6FD";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.46988746523857117 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[24:27]" -type "float3"  4.6566129e-10 0 -3.7252903e-09 
		4.6566129e-10 0 -3.7252903e-09 -0.00087890727 0 0.15174529 -0.00087890727 0 0.15174529;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape1" -p "pCube114";
	rename -uid "F5BB4EA9-4C10-1A57-7250-0582AC175D26";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube115";
	rename -uid "479E7FD2-476E-335F-2704-3E9210011F8D";
	setAttr ".t" -type "double3" 39.923102985061952 8.134 25.069523312541261 ;
	setAttr ".r" -type "double3" 0 -4.1274641051566023 0 ;
	setAttr ".s" -type "double3" 0.60943012643720129 1 4.96024169686787 ;
createNode mesh -n "pCubeShape115" -p "pCube115";
	rename -uid "7B6D97D7-46E2-DCB1-2173-05BB7F329815";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "pCube116";
	rename -uid "8F21536B-4524-49ED-5AFA-61991F698ECA";
	setAttr ".t" -type "double3" 50.782057955436862 8.1339380822017979 21.339246531593268 ;
	setAttr ".r" -type "double3" -1.5004053126109053e-17 -34.101495243619524 -180.14728673805888 ;
	setAttr ".s" -type "double3" 5.7497657001350531 1 0.43951687645969528 ;
createNode mesh -n "pCubeShape116" -p "pCube116";
	rename -uid "3E633874-4FD8-CF70-F469-F8BAF9BCD815";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.38375362753868103 0.6249995231628418 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 94 ".uvst[0].uvsp[0:93]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.4368645 0 0.4368645 1 0.4368645 0.75 0.4368645 0.5
		 0.4368645 0.25 0.42814019 0 0.42814019 1 0.42814019 0.75 0.42814019 0.5 0.42814019
		 0.25 0.41807479 0 0.41807479 1 0.41807479 0.75 0.41807479 0.5 0.41807479 0.25 0.4093985
		 0 0.4093985 1 0.4093985 0.75 0.4093985 0.5 0.4093985 0.25 0.3915776 0 0.3915776 1
		 0.3915776 0.75 0.3915776 0.5 0.3915776 0.25 0.38375363 0 0.38375363 1 0.38375363
		 0.75 0.38375363 0.5 0.38375363 0.25 0.42814019 0.75 0.42814019 0.5 0.4368645 0.5
		 0.4368645 0.75 0.42814019 0.75 0.42814019 0.5 0.4368645 0.5 0.4368645 0.75 0.42814019
		 0.75 0.42814019 0.5 0.42814019 0.5 0.42814019 0.75 0.42814019 0.75 0.42814019 0.5
		 0.42814019 0.5 0.42814019 0.75 0.42814019 0.74999553 0.42814019 0.50000346 0.4093985
		 0.75 0.4093985 0.5 0.41807479 0.5 0.41807479 0.75 0.4093985 0.75 0.4093985 0.5 0.41807479
		 0.5 0.41807479 0.75 0.4093985 0.75 0.4093985 0.5 0.4093985 0.5 0.4093985 0.75 0.4093985
		 0.75 0.4093985 0.5 0.4093985 0.5 0.4093985 0.75 0.4093985 0.74999559 0.4093985 0.50000352
		 0.38375363 0.75 0.38375363 0.5 0.3915776 0.5 0.3915776 0.75 0.38375363 0.75 0.38375363
		 0.5 0.3915776 0.5 0.3915776 0.75 0.38375363 0.75 0.38375363 0.5 0.38375363 0.5 0.38375363
		 0.75 0.38375363 0.74999559 0.38375363 0.50000346;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 48 ".pt";
	setAttr ".pt[32]" -type "float3" -0.0022584575 -7.4423428e-05 -0.062904693 ;
	setAttr ".pt[33]" -type "float3" -0.0022584761 -7.4423428e-05 -0.062904581 ;
	setAttr ".pt[34]" -type "float3" 0.00043972279 -9.9227815e-05 0.13208696 ;
	setAttr ".pt[35]" -type "float3" 0.00043973708 -9.9227815e-05 0.13208669 ;
	setAttr ".pt[36]" -type "float3" -0.0080083599 -7.4415155e-05 0.029285833 ;
	setAttr ".pt[37]" -type "float3" -0.0080083832 -7.4415155e-05 0.029286131 ;
	setAttr ".pt[38]" -type "float3" 0.00069256761 -2.4752619e-05 0.051679626 ;
	setAttr ".pt[39]" -type "float3" 0.00069257134 -2.4752619e-05 0.051679417 ;
	setAttr ".pt[40]" -type "float3" 0.0035836389 0.00014893177 -0.062670887 ;
	setAttr ".pt[41]" -type "float3" 0.0035836133 0.00014893177 -0.062670752 ;
	setAttr ".pt[42]" -type "float3" 0.0097899986 0.00014893977 -0.071518764 ;
	setAttr ".pt[43]" -type "float3" 0.0097900322 0.00014893977 -0.071519032 ;
	setAttr ".pt[44]" -type "float3" -0.017786689 -2.4871013e-05 -0.16264474 ;
	setAttr ".pt[45]" -type "float3" -0.017786717 -2.4871013e-05 -0.16264459 ;
	setAttr ".pt[46]" -type "float3" -0.014582759 -2.4866727e-05 -0.17320657 ;
	setAttr ".pt[47]" -type "float3" -0.014582737 -2.4866727e-05 -0.17320684 ;
	setAttr ".pt[48]" -type "float3" -0.020822877 -2.4894456e-05 -0.28365824 ;
	setAttr ".pt[49]" -type "float3" -0.020822903 -2.4894456e-05 -0.28365818 ;
	setAttr ".pt[50]" -type "float3" 0.0035707881 5.6304641e-05 -0.14805228 ;
	setAttr ".pt[51]" -type "float3" 0.0035707725 5.6304641e-05 -0.14805222 ;
	setAttr ".pt[54]" -type "float3" -0.0011655116 0.00013077434 -0.079585463 ;
	setAttr ".pt[55]" -type "float3" -0.0011655395 0.00013077434 -0.079585351 ;
	setAttr ".pt[56]" -type "float3" 0.0012582903 7.4473377e-05 -0.11447531 ;
	setAttr ".pt[57]" -type "float3" 0.0012583005 7.4473377e-05 -0.11447543 ;
	setAttr ".pt[58]" -type "float3" -0.00037797994 0.00012669573 -0.20730959 ;
	setAttr ".pt[59]" -type "float3" -0.00037797994 0.00012669573 -0.20730959 ;
	setAttr ".pt[60]" -type "float3" -0.00037797994 0.00012669573 -0.20730959 ;
	setAttr ".pt[61]" -type "float3" -0.00037797994 0.00012669573 -0.20730959 ;
	setAttr ".pt[62]" -type "float3" -0.015510221 2.8154605e-05 -0.34411824 ;
	setAttr ".pt[63]" -type "float3" -0.015510221 2.8154605e-05 -0.34411824 ;
	setAttr ".pt[64]" -type "float3" -0.015510221 2.8154605e-05 -0.34411824 ;
	setAttr ".pt[65]" -type "float3" -0.015510221 2.8154605e-05 -0.34411824 ;
	setAttr ".pt[66]" -type "float3" -0.029040514 -7.0386508e-05 -0.44997564 ;
	setAttr ".pt[67]" -type "float3" -0.029040514 -7.0386508e-05 -0.44997564 ;
	setAttr ".pt[68]" -type "float3" -0.0048716473 9.8540891e-05 -0.28590208 ;
	setAttr ".pt[69]" -type "float3" -0.0048716483 9.8540891e-05 -0.28590205 ;
	setAttr ".pt[70]" -type "float3" -0.0043212073 5.6309207e-05 -0.17238168 ;
	setAttr ".pt[71]" -type "float3" -0.0043212073 5.6309207e-05 -0.17238168 ;
	setAttr ".pt[72]" -type "float3" -0.0063516861 9.854119e-05 -0.21574271 ;
	setAttr ".pt[73]" -type "float3" -0.0063516875 9.854119e-05 -0.21574271 ;
	setAttr ".pt[74]" -type "float3" -0.0044842036 5.6309364e-05 -0.20185311 ;
	setAttr ".pt[75]" -type "float3" -0.0044842032 5.6309364e-05 -0.20185311 ;
	setAttr ".pt[76]" -type "float3" -0.019525107 -4.2231375e-05 -0.33099139 ;
	setAttr ".pt[77]" -type "float3" -0.019525114 -4.2231375e-05 -0.3309913 ;
	setAttr ".pt[78]" -type "float3" -0.020449765 -4.223243e-05 -0.30802363 ;
	setAttr ".pt[79]" -type "float3" -0.020449758 -4.223243e-05 -0.30802372 ;
	setAttr ".pt[80]" -type "float3" -0.029681178 -0.00025339145 -0.17345557 ;
	setAttr ".pt[81]" -type "float3" -0.029681178 -0.00025339145 -0.17345557 ;
	setAttr -s 82 ".vt[0:81]"  -0.5 -0.5 0.5000034 0.5 -0.5 0.49999958 -0.5 0.5 0.5000034
		 0.5 0.5 0.49999958 -0.5 0.5 -0.50000042 0.50000048 0.5 -0.50000042 -0.5 -0.5 -0.50000042
		 0.50000048 -0.5 -0.50000042 -0.25254202 -0.5 0.49999958 -0.25254202 -0.5 -0.4999966
		 -0.25254202 0.5 -0.4999966 -0.25254202 0.5 0.49999958 -0.28743935 -0.5 0.5000034
		 -0.28743887 -0.5 -0.49998897 -0.28743887 0.5 -0.49998897 -0.28743935 0.5 0.5000034
		 -0.32770109 -0.5 0.50002247 -0.32770109 -0.5 -0.49999279 -0.32770109 0.5 -0.49999279
		 -0.32770109 0.5 0.50002247 -0.36240625 -0.5 0.49999958 -0.36240578 -0.5 -0.4999966
		 -0.36240578 0.5 -0.4999966 -0.36240625 0.5 0.49999958 -0.43368959 -0.5 0.49999958
		 -0.43368959 -0.5 -0.4999966 -0.43368959 0.5 -0.4999966 -0.43368959 0.5 0.49999958
		 -0.46498537 -0.5 0.49999958 -0.46498537 -0.5 -0.4999966 -0.46498537 0.5 -0.4999966
		 -0.46498537 0.5 0.49999958 -0.47123623 -0.5 -2.091160297 -0.47123623 0.5 -2.091160297
		 -0.43633938 0.5 -2.091160297 -0.43633938 -0.5 -2.091160297 -0.4331646 -0.5 -1.76157045
		 -0.4331646 0.5 -1.76157045 -0.39826822 0.5 -1.76157427 -0.39826822 -0.5 -1.76157427
		 -0.55455303 -0.5 -2.036079884 -0.55455303 0.5 -2.036079884 -0.55981684 0.5 -2.29102373
		 -0.55981684 -0.5 -2.29102373 -0.6520648 -0.5 -2.070774555 -0.6520648 0.5 -2.070774555
		 -0.65732908 0.5 -2.25562334 -0.65732908 -0.5 -2.25562334 -0.68257666 -0.5 -1.94063234
		 -0.68257666 0.5 -1.94063234 -0.49522638 -0.5 -1.46596193 -0.49522638 0.5 -1.46596193
		 -0.46052122 0.5 -1.4659543 -0.46052122 -0.5 -1.4659543 -0.46612549 -0.5 -1.25432634
		 -0.46612549 0.5 -1.25432634 -0.4314208 0.5 -1.25431108 -0.4314208 -0.5 -1.25431108
		 -0.55991602 -0.5 -1.36241198 -0.55991602 0.5 -1.36241198 -0.56512785 0.5 -1.54106951
		 -0.56512785 -0.5 -1.54106951 -0.63019514 -0.5 -1.025864124 -0.63019514 0.5 -1.025864124
		 -0.63884449 0.5 -1.1586957 -0.63884449 -0.5 -1.1586957 -0.65918398 -0.5 -0.7902264
		 -0.65918398 0.5 -0.7902264 -0.54723692 -0.5 -0.89692348 -0.54723692 0.5 -0.89692348
		 -0.51594114 0.5 -0.89691967 -0.51594114 -0.5 -0.89691967 -0.52116156 -0.5 -0.77108806
		 -0.52116156 0.5 -0.77108806 -0.4898653 0.5 -0.77108425 -0.4898653 -0.5 -0.77108425
		 -0.59106779 -0.5 -0.48570675 -0.59106779 0.5 -0.48570675 -0.604496 0.5 -0.59289593
		 -0.604496 -0.5 -0.59289593 -0.61441183 -0.5 -0.32029384 -0.61441183 0.5 -0.32029384;
	setAttr -s 163 ".ed[0:162]"  0 28 0 2 31 0 4 30 0 6 29 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 7 0 10 5 0 11 3 0 8 9 1 9 10 0 10 11 1 11 8 1
		 12 8 0 13 9 1 14 10 1 15 11 0 12 13 1 13 14 0 14 15 1 15 12 1 16 12 0 17 13 0 18 14 0
		 19 15 0 16 17 1 17 18 1 18 19 1 19 16 1 20 16 0 21 17 1 22 18 1 23 19 0 20 21 1 21 22 0
		 22 23 1 23 20 1 24 20 0 25 21 0 26 22 0 27 23 0 24 25 1 25 26 1 26 27 1 27 24 1 28 24 0
		 29 25 1 30 26 1 31 27 0 28 29 1 29 30 0 30 31 1 31 28 1 13 36 0 14 37 0 32 33 1 10 38 0
		 33 34 0 9 39 0 35 34 0 32 35 0 36 32 1 37 33 1 38 34 0 39 35 0 36 37 1 37 38 1 38 39 1
		 39 36 1 36 40 0 37 41 0 40 41 1 33 42 0 41 42 1 32 43 0 43 42 1 40 43 1 40 44 0 41 45 0
		 44 45 0 42 46 0 45 46 1 43 47 0 47 46 1 44 47 1 44 48 0 45 49 0 48 49 0 46 49 0 47 48 0
		 21 54 0 22 55 0 50 51 1 18 56 0 51 52 0 17 57 0 53 52 1 50 53 0 54 50 1 55 51 1 56 52 0
		 57 53 0 54 55 1 55 56 1 56 57 1 57 54 1 54 58 0 55 59 0 58 59 1 51 60 0 59 60 1 50 61 0
		 61 60 1 58 61 1 58 62 0 59 63 0 62 63 1 60 64 0 63 64 1 61 65 0 65 64 1 62 65 1 62 66 0
		 63 67 0 66 67 0 64 67 0 65 66 0 29 72 0 30 73 0 68 69 1 26 74 0 69 70 0 25 75 0 71 70 1
		 68 71 0 72 68 1 73 69 1 74 70 0 75 71 0 72 73 0 73 74 1 74 75 1 75 72 1 72 76 0 73 77 0
		 76 77 1 69 78 0 77 78 1 68 79 0 79 78 1 76 79 1 76 80 0 77 81 0 80 81 0 78 81 0 79 80 0;
	setAttr -s 83 -ch 326 ".fc[0:82]" -type "polyFaces" 
		f 4 19 12 5 -16
		mu 0 4 18 14 1 3
		f 4 18 15 7 -15
		mu 0 4 17 18 3 5
		f 4 17 14 9 -14
		mu 0 4 16 17 5 7
		f 4 16 13 11 -13
		mu 0 4 15 16 7 9
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 24 21 -17 -21
		mu 0 4 20 21 16 15
		f 4 62 64 -67 -68
		mu 0 4 44 45 46 47
		f 4 26 23 -19 -23
		mu 0 4 22 23 18 17
		f 4 27 20 -20 -24
		mu 0 4 23 19 14 18
		f 4 32 29 -25 -29
		mu 0 4 25 26 21 20
		f 4 33 30 -26 -30
		mu 0 4 26 27 22 21
		f 4 34 31 -27 -31
		mu 0 4 27 28 23 22
		f 4 35 28 -28 -32
		mu 0 4 28 24 19 23
		f 4 40 37 -33 -37
		mu 0 4 30 31 26 25
		f 4 99 101 -104 -105
		mu 0 4 62 63 64 65
		f 4 42 39 -35 -39
		mu 0 4 32 33 28 27
		f 4 43 36 -36 -40
		mu 0 4 33 29 24 28
		f 4 48 45 -41 -45
		mu 0 4 35 36 31 30
		f 4 49 46 -42 -46
		mu 0 4 36 37 32 31
		f 4 50 47 -43 -47
		mu 0 4 37 38 33 32
		f 4 51 44 -44 -48
		mu 0 4 38 34 29 33
		f 4 56 53 -49 -53
		mu 0 4 40 41 36 35
		f 4 136 138 -141 -142
		mu 0 4 80 81 82 83
		f 4 58 55 -51 -55
		mu 0 4 42 43 38 37
		f 4 59 52 -52 -56
		mu 0 4 43 39 34 38
		f 4 3 -57 -1 -11
		mu 0 4 6 41 40 8
		f 4 2 -58 -4 -9
		mu 0 4 4 42 41 6
		f 4 1 -59 -3 -7
		mu 0 4 2 43 42 4
		f 4 0 -60 -2 -5
		mu 0 4 0 39 43 2
		f 4 73 70 -65 -70
		mu 0 4 49 50 46 45
		f 4 74 71 66 -71
		mu 0 4 50 51 47 46
		f 4 75 68 67 -72
		mu 0 4 51 48 44 47
		f 4 25 61 -73 -61
		mu 0 4 21 22 49 48
		f 4 22 63 -74 -62
		mu 0 4 22 17 50 49
		f 4 -18 65 -75 -64
		mu 0 4 17 16 51 50
		f 4 -22 60 -76 -66
		mu 0 4 16 21 48 51
		f 4 72 77 -79 -77
		mu 0 4 48 49 53 52
		f 4 69 79 -81 -78
		mu 0 4 49 45 54 53
		f 4 -63 81 82 -80
		mu 0 4 45 44 55 54
		f 4 -69 76 83 -82
		mu 0 4 44 48 52 55
		f 4 78 85 -87 -85
		mu 0 4 52 53 57 56
		f 4 80 87 -89 -86
		mu 0 4 53 54 58 57
		f 4 -83 89 90 -88
		mu 0 4 54 55 59 58
		f 4 -84 84 91 -90
		mu 0 4 55 52 56 59
		f 4 86 93 -95 -93
		mu 0 4 56 57 61 60
		f 3 88 95 -94
		mu 0 3 57 58 61
		f 4 -91 96 94 -96
		mu 0 4 58 59 60 61
		f 3 -92 92 -97
		mu 0 3 59 56 60
		f 4 110 107 -102 -107
		mu 0 4 67 68 64 63
		f 4 111 108 103 -108
		mu 0 4 68 69 65 64
		f 4 112 105 104 -109
		mu 0 4 69 66 62 65
		f 4 41 98 -110 -98
		mu 0 4 31 32 67 66
		f 4 38 100 -111 -99
		mu 0 4 32 27 68 67
		f 4 -34 102 -112 -101
		mu 0 4 27 26 69 68
		f 4 -38 97 -113 -103
		mu 0 4 26 31 66 69
		f 4 109 114 -116 -114
		mu 0 4 66 67 71 70
		f 4 106 116 -118 -115
		mu 0 4 67 63 72 71
		f 4 -100 118 119 -117
		mu 0 4 63 62 73 72
		f 4 -106 113 120 -119
		mu 0 4 62 66 70 73
		f 4 115 122 -124 -122
		mu 0 4 70 71 75 74
		f 4 117 124 -126 -123
		mu 0 4 71 72 76 75
		f 4 -120 126 127 -125
		mu 0 4 72 73 77 76
		f 4 -121 121 128 -127
		mu 0 4 73 70 74 77
		f 4 123 130 -132 -130
		mu 0 4 74 75 79 78
		f 3 125 132 -131
		mu 0 3 75 76 79
		f 4 -128 133 131 -133
		mu 0 4 76 77 78 79
		f 3 -129 129 -134
		mu 0 3 77 74 78
		f 4 147 144 -139 -144
		mu 0 4 85 86 82 81
		f 4 148 145 140 -145
		mu 0 4 86 87 83 82
		f 4 149 142 141 -146
		mu 0 4 87 84 80 83
		f 4 57 135 -147 -135
		mu 0 4 41 42 85 84
		f 4 54 137 -148 -136
		mu 0 4 42 37 86 85
		f 4 -50 139 -149 -138
		mu 0 4 37 36 87 86
		f 4 -54 134 -150 -140
		mu 0 4 36 41 84 87
		f 4 146 151 -153 -151
		mu 0 4 84 85 89 88
		f 4 143 153 -155 -152
		mu 0 4 85 81 90 89
		f 4 -137 155 156 -154
		mu 0 4 81 80 91 90
		f 4 -143 150 157 -156
		mu 0 4 80 84 88 91
		f 4 152 159 -161 -159
		mu 0 4 88 89 93 92
		f 3 154 161 -160
		mu 0 3 89 90 93
		f 4 -157 162 160 -162
		mu 0 4 90 91 92 93
		f 3 -158 158 -163
		mu 0 3 91 88 92;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape4" -p "pCube116";
	rename -uid "8051B020-42BA-88B9-08B8-6C83F7B11816";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pCube117";
	rename -uid "C6A30C00-472A-656C-F823-1097934740F8";
	setAttr ".t" -type "double3" 38.321050989219941 8.134 27.312649650486385 ;
	setAttr ".r" -type "double3" 0 27.84379501171129 0 ;
	setAttr ".s" -type "double3" 2.4344514908385229 1 0.20021932370991291 ;
createNode mesh -n "pCubeShape117" -p "pCube117";
	rename -uid "B0DD5DE2-474A-132C-D89D-CF9067F6E59E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.39430782198905945 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube118";
	rename -uid "FA7A44DD-4E52-150E-7DD0-DBB785E23259";
	setAttr ".t" -type "double3" 41.269327023849499 8.134 27.286088605129365 ;
	setAttr ".r" -type "double3" -180 152.59665299216405 0 ;
	setAttr ".s" -type "double3" 2.4344514908385229 1 0.20021932370991291 ;
createNode mesh -n "pCubeShape118" -p "pCube118";
	rename -uid "29D128C7-4BF1-F89C-CA5B-C4BBAD7E66BF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.39430782198905945 0.125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 57 ".uvst[0].uvsp[0:56]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.39645314 0 0.39645314 1 0.39645314 0.75 0.39645314
		 0.5 0.39645314 0.25 0.375 0 0.39645314 0 0.39645314 0.25 0.375 0.25 0.375 0 0.39645314
		 0 0.39645314 0.25 0.375 0.25 0.37929064 0.25 0.38572657 0.25 0.3921625 0.25 0.37929064
		 0 0.38572657 0 0.3921625 0 0.375 0 0.37929064 0 0.37929064 0.25 0.375 0.25 0.37505949
		 4.4685185e-06 0.37504798 0.24999653 0.37929064 0.25 0.37929064 0 0.38572657 0 0.38572657
		 0.25 0.37936181 0.24999653 0.3793785 4.4694443e-06 0.38572657 0.25 0.38572657 0 0.3921625
		 0 0.3921625 0.25 0.39211565 0.24999653 0.392102 4.4813e-06 0.3921625 0.25 0.3921625
		 0 0.39645314 0 0.39645314 0.25 0.39642668 0.24999653 0.39641839 4.4852659e-06;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 50 ".vt[0:49]"  -0.53979301 -0.5 0.24441528 0.50000095 -0.5 0.5
		 -0.53979301 0.5 0.24441528 0.50000095 0.5 0.5 -0.5 0.5 -0.5 0.50000381 0.5 -0.49996948
		 -0.5 -0.5 -0.5 0.50000381 -0.5 -0.49996948 -0.4505682 -0.5 0.26634216 -0.41418552 -0.5 -0.5
		 -0.41418552 0.5 -0.5 -0.4505682 0.5 0.26634216 -1.38195705 -0.5 17.24893188 -1.3428793 -0.5 17.65197754
		 -1.3428793 0.5 17.65197754 -1.38195705 0.5 17.24893188 -1.75555229 -0.5 21.162323
		 -1.61766481 -0.5 22.5713501 -1.61766481 0.5 22.5713501 -1.75555229 0.5 21.162323
		 -1.72798157 0.5 21.44403076 -1.68661404 0.5 21.86676025 -1.64524555 0.5 22.28945923
		 -1.72798157 -0.5 21.44403076 -1.68661404 -0.5 21.86676025 -1.64524555 -0.5 22.28945923
		 -1.93414545 -0.5 22.42597961 -1.90158987 -0.5 22.61351013 -1.90158987 0.5 22.61351013
		 -1.93414545 0.5 22.42597961 -2.036731243 -0.5 24.31459045 -2.036731243 0.5 24.31459045
		 -1.87503195 -0.5 23.16906738 -1.87503195 0.5 23.16906738 -1.84787369 -0.5 23.45623779
		 -1.84787369 0.5 23.45623779 -1.99591923 -0.5 25.2897644 -1.99591923 0.5 25.2897644
		 -1.80150032 0.5 24.17144775 -1.80150032 -0.5 24.17144775 -1.77327204 -0.5 24.44622803
		 -1.77327204 0.5 24.44622803 -1.89494514 0.5 26.11233521 -1.89494514 -0.5 26.11233521
		 -1.72589016 0.5 24.92681885 -1.72589016 -0.5 24.92681885 -1.70278263 -0.5 25.26290894
		 -1.70278263 0.5 25.26290894 -1.81579399 0.5 26.63497925 -1.81579399 -0.5 26.63497925;
	setAttr -s 103 ".ed[0:102]"  0 8 1 2 11 1 4 10 0 6 9 0 0 2 1 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0 8 1 0 9 7 0 10 5 0 11 3 0 8 9 1 9 10 1 10 11 1 11 8 0
		 0 12 0 8 13 0 12 13 1 11 14 0 14 13 1 2 15 0 15 14 1 12 15 1 12 16 0 13 17 0 16 23 1
		 14 18 0 18 17 1 15 19 0 19 20 1 16 19 1 20 21 1 21 22 1 22 18 1 23 24 1 24 25 1 25 17 1
		 22 25 0 23 20 0 21 24 0 15 20 1 22 14 1 15 21 1 23 12 1 13 25 1 24 12 1 16 26 0 23 27 0
		 26 27 1 20 28 0 27 28 1 19 29 0 29 28 1 26 29 1 26 30 0 27 30 0 28 31 0 29 31 0 30 31 0
		 23 32 0 20 33 0 32 33 1 24 34 0 32 34 1 21 35 0 35 34 1 33 35 1 32 36 0 33 37 0 36 37 0
		 34 36 0 35 37 0 21 38 0 24 39 0 38 39 1 25 40 0 39 40 1 22 41 0 41 40 1 38 41 1 38 42 0
		 39 43 0 42 43 0 40 43 0 41 42 0 22 44 0 25 45 0 44 45 1 17 46 0 45 46 1 18 47 0 47 46 1
		 44 47 1 44 48 0 45 49 0 48 49 0 46 49 0 47 48 0;
	setAttr -s 55 -ch 206 ".fc[0:54]" -type "polyFaces" 
		f 4 19 12 5 -16
		mu 0 4 18 14 1 3
		f 4 18 15 7 -15
		mu 0 4 17 18 3 5
		f 4 17 14 9 -14
		mu 0 4 16 17 5 7
		f 4 16 13 11 -13
		mu 0 4 15 16 7 9
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 3 -17 -1 -11
		mu 0 4 6 16 15 8
		f 4 2 -18 -4 -9
		mu 0 4 4 17 16 6
		f 4 1 -19 -3 -7
		mu 0 4 2 18 17 4
		f 4 0 21 -23 -21
		mu 0 4 0 14 20 19
		f 4 -20 23 24 -22
		mu 0 4 14 18 21 20
		f 4 -2 25 26 -24
		mu 0 4 18 2 22 21
		f 4 -5 20 27 -26
		mu 0 4 2 0 19 22
		f 4 22 49 -41 50
		mu 0 4 19 20 32 31
		f 4 -25 31 32 -30
		mu 0 4 20 21 25 24
		f 4 -27 47 37 46
		mu 0 4 21 22 28 29
		f 4 -28 28 35 -34
		mu 0 4 22 19 23 26
		f 3 -46 33 34
		mu 0 3 27 22 26
		f 3 -47 38 -32
		mu 0 3 21 29 25
		f 3 -48 45 36
		mu 0 3 28 22 27
		f 3 -49 -31 -29
		mu 0 3 19 30 23
		f 3 -50 29 -42
		mu 0 3 32 20 24
		f 3 -51 -40 48
		mu 0 3 19 31 30
		f 4 30 52 -54 -52
		mu 0 4 23 30 34 33
		f 4 43 54 -56 -53
		mu 0 4 30 27 35 34
		f 4 -35 56 57 -55
		mu 0 4 27 26 36 35
		f 4 -36 51 58 -57
		mu 0 4 26 23 33 36
		f 3 53 60 -60
		mu 0 3 33 34 37
		f 4 55 61 -64 -61
		mu 0 4 34 35 38 37
		f 3 -58 62 -62
		mu 0 3 35 36 38
		f 4 -59 59 63 -63
		mu 0 4 36 33 37 38
		f 4 -44 64 66 -66
		mu 0 4 27 30 40 39
		f 4 39 67 -69 -65
		mu 0 4 30 31 41 40
		f 4 -45 69 70 -68
		mu 0 4 31 28 42 41
		f 4 -37 65 71 -70
		mu 0 4 28 27 39 42
		f 4 -67 72 74 -74
		mu 0 4 39 40 44 43
		f 3 68 75 -73
		mu 0 3 40 41 44
		f 4 -71 76 -75 -76
		mu 0 4 41 42 43 44
		f 3 -72 73 -77
		mu 0 3 42 39 43
		f 4 44 78 -80 -78
		mu 0 4 28 31 46 45
		f 4 40 80 -82 -79
		mu 0 4 31 32 47 46
		f 4 -43 82 83 -81
		mu 0 4 32 29 48 47
		f 4 -38 77 84 -83
		mu 0 4 29 28 45 48
		f 4 79 86 -88 -86
		mu 0 4 45 46 50 49
		f 3 81 88 -87
		mu 0 3 46 47 50
		f 4 -84 89 87 -89
		mu 0 4 47 48 49 50
		f 3 -85 85 -90
		mu 0 3 48 45 49
		f 4 42 91 -93 -91
		mu 0 4 29 32 52 51
		f 4 41 93 -95 -92
		mu 0 4 32 24 53 52
		f 4 -33 95 96 -94
		mu 0 4 24 25 54 53
		f 4 -39 90 97 -96
		mu 0 4 25 29 51 54
		f 4 92 99 -101 -99
		mu 0 4 51 52 56 55
		f 3 94 101 -100
		mu 0 3 52 53 56
		f 4 -97 102 100 -102
		mu 0 4 53 54 55 56
		f 3 -98 98 -103
		mu 0 3 54 51 55;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube119";
	rename -uid "AB3D9383-4675-394C-CF3D-269A122FC398";
	setAttr ".t" -type "double3" 40.072732140389448 8.134 21.992629752141664 ;
	setAttr ".s" -type "double3" 0.38024690508130543 1 1 ;
createNode transform -n "transform1" -p "pCube119";
	rename -uid "3DAD3B91-4027-E252-E895-E59399075628";
	setAttr ".v" no;
createNode mesh -n "pCubeShape119" -p "transform1";
	rename -uid "4B6AFB99-42DC-4693-AC32-0E87AFACCD25";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[28:31]" -type "float3"  0.30443659 0 0.02894032 0.30443731 
		0 0.028940409 0.30443731 0 0.028940409 0.30443659 0 0.02894032;
	setAttr ".dr" 1;
createNode transform -n "pCube120";
	rename -uid "5044C9AB-4F2F-4FBB-DFBF-7EBC43AE9083";
	setAttr ".t" -type "double3" 38.225246169993184 8.134 18.292894329658136 ;
createNode transform -n "transform2" -p "pCube120";
	rename -uid "94E9AD66-427A-2EE4-A65C-C89D2D389C41";
	setAttr ".v" no;
createNode mesh -n "pCubeShape120" -p "transform2";
	rename -uid "A65FF263-4F18-61CE-0441-C9B4F525A1A5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.875 0.12499967484836816 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt";
	setAttr ".pt[52]" -type "float3" -0.066657186 0 -0.013331451 ;
	setAttr ".pt[53]" -type "float3" -0.066657186 0 -0.013331458 ;
	setAttr ".dr" 1;
createNode transform -n "pCube121";
	rename -uid "015F4E4E-4019-13BB-74B8-33BE3CC2D540";
	setAttr ".rp" -type "double3" 39.192934730479024 8.134 18.999779081152585 ;
	setAttr ".sp" -type "double3" 39.192934730479024 8.134 18.999779081152585 ;
createNode mesh -n "pCube121Shape" -p "pCube121";
	rename -uid "7C3C9FED-44E6-ABD2-12D8-96BA27D99A26";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.38727149367332458 0.25286570191383362 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "92A97086-4371-791B-650F-25B3893EFF90";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "9CD7BEB4-4E8D-A00E-6340-EFA753CA8110";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "9D73B8F8-46A7-58E4-2365-76902E69668C";
createNode displayLayerManager -n "layerManager";
	rename -uid "CBED479C-438F-63FF-6086-B3957B1CF813";
	setAttr ".cdl" 1;
	setAttr -s 2 ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
	rename -uid "10CA14F9-48EC-0A29-4803-BAA722C9C6DA";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "824E05F5-474C-E8F7-7696-D6848541B7B1";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "BB1C2137-49A4-6B17-265D-D4B5CCA5E1EA";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "3625AFE6-41BF-1D5D-C57B-AEB0039080FC";
	setAttr ".cuv" 4;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "6A91E54A-471D-B25B-5917-09995522C931";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n"
		+ "            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n"
		+ "            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n"
		+ "            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1319\n            -height 700\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n"
		+ "            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n"
		+ "            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n"
		+ "            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 656\n            -height 4\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n"
		+ "            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 656\n            -height 4\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 656\n            -height 652\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n"
		+ "            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n"
		+ "            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n"
		+ "                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n"
		+ "                -classicMode 1\n                -valueLinesToggle 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n"
		+ "                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n"
		+ "                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n"
		+ "                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n"
		+ "                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n"
		+ "                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n"
		+ "                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n"
		+ "                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n"
		+ "                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -highlightConnections 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"straight\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n"
		+ "\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1319\\n    -height 700\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1319\\n    -height 700\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "C6A95AAF-45C4-5B37-6284-308529F36707";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyPlane -n "polyPlane1";
	rename -uid "7A2E0C85-4245-52C0-34FD-C5A9F0D5EB0A";
	setAttr ".sw" 1;
	setAttr ".sh" 1;
	setAttr ".cuv" 2;
createNode file -n "file1";
	rename -uid "3EDC9D85-40D2-A011-0BEB-7D90333B5B73";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "B3C176D3-4B51-AFBF-4C73-E0B32A75B9E8";
createNode lambert -n "lambert2";
	rename -uid "07A4ABEE-41F0-E193-391F-DE840B4E3585";
createNode shadingEngine -n "lambert2SG";
	rename -uid "D20FFB0B-44BC-7939-D291-CD9EEB0E868B";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "14E985A3-4A76-8F22-B07A-2DA3224ADF61";
createNode file -n "file2";
	rename -uid "0CC8A01B-44BA-AE78-9C5F-B9B95AF8B2A8";
	setAttr ".ftn" -type "string" "C:/Users/Paolo/Documents/SHERIDAN/Year 2/SprintWeek/PaoloModels//sourceimages/28721747_428336220951210_1253070185_n.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture2";
	rename -uid "888CD6E0-4CA5-6061-2405-D6B8A3D18708";
createNode displayLayer -n "ref_plane01";
	rename -uid "12E41A48-463A-0AF3-0D06-ED953F75CB6A";
	setAttr ".c" 22;
	setAttr ".do" 1;
createNode polyCube -n "polyCube2";
	rename -uid "E024EAAE-4D5C-50F9-A969-6896562556AB";
	setAttr ".cuv" 4;
createNode polyCube -n "polyCube3";
	rename -uid "36F0BC01-46F2-4460-FCA4-9296E83AABFA";
	setAttr ".cuv" 4;
createNode polySplit -n "polySplit1";
	rename -uid "0A1598D6-4722-CD9E-2A6F-DD9BA8EC693E";
	setAttr -s 5 ".e[0:4]"  0.90832603 0.90832603 0.091674402 0.091674402
		 0.90832603;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483641 -2147483637 -2147483638 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "6BBF567C-4F2E-853E-E2C1-B2874BA0B267";
	setAttr -s 5 ".e[0:4]"  0.779787 0.779787 0.220213 0.220213 0.779787;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483641 -2147483634 -2147483633 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit3";
	rename -uid "1697D088-4C3F-C7E4-01AB-35A5E280E69E";
	setAttr -s 5 ".e[0:4]"  0.89961702 0.89961702 0.100383 0.100383 0.89961702;
	setAttr -s 5 ".d[0:4]"  -2147483642 -2147483641 -2147483626 -2147483625 -2147483642;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "A4EAC414-4ABA-F7DB-4794-B384A3396D60";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 39.781322 8.1339998 22.800676 ;
	setAttr ".rs" 52431;
	setAttr ".lt" -type "double3" -0.67099190364706429 -1.5452671597346638e-16 1.2618063925129588 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 39.764957056245557 7.634 22.573902879623343 ;
	setAttr ".cbx" -type "double3" 39.797686290034697 8.634 23.02745061711251 ;
createNode polySplit -n "polySplit4";
	rename -uid "D0863260-42FE-AE98-0279-EB91E21B644C";
	setAttr -s 5 ".e[0:4]"  0.70125699 0.70125699 0.70125699 0.70125699
		 0.70125699;
	setAttr -s 5 ".d[0:4]"  -2147483612 -2147483611 -2147483609 -2147483607 -2147483612;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "85C8AFFA-4BE1-55B6-0971-6A96A432ABAD";
	setAttr ".ics" -type "componentList" 1 "f[19]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 38.653336 8.1339998 23.51948 ;
	setAttr ".rs" 50840;
	setAttr ".lt" -type "double3" -0.025048982666102976 -1.4606895237537363e-06 0.69175859755116964 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 38.458130899924477 7.634 23.433077683755609 ;
	setAttr ".cbx" -type "double3" 38.848537628508502 8.634 23.605882716141135 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "A24A2B32-448A-62B2-E0C7-8B91F626FBD3";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[12]" -type "float3" -1.3969839e-09 0 0 ;
	setAttr ".tk[13]" -type "float3" -1.3969839e-09 0 0 ;
	setAttr ".tk[14]" -type "float3" -1.3969839e-09 0 0 ;
	setAttr ".tk[15]" -type "float3" -1.3969839e-09 0 0 ;
	setAttr ".tk[16]" -type "float3" -4.6566129e-10 0 -7.4505806e-09 ;
	setAttr ".tk[17]" -type "float3" -4.6566129e-10 0 -7.4505806e-09 ;
	setAttr ".tk[18]" -type "float3" -4.6566129e-10 0 -7.4505806e-09 ;
	setAttr ".tk[19]" -type "float3" -4.6566129e-10 0 -7.4505806e-09 ;
	setAttr ".tk[20]" -type "float3" 4.6566129e-09 0 7.0780516e-08 ;
	setAttr ".tk[21]" -type "float3" 4.6566129e-09 0 7.0780516e-08 ;
	setAttr ".tk[22]" -type "float3" 4.6566129e-09 0 7.0780516e-08 ;
	setAttr ".tk[23]" -type "float3" 4.6566129e-09 0 7.0780516e-08 ;
	setAttr ".tk[28]" -type "float3" 0.045328192 0 0.026023287 ;
	setAttr ".tk[29]" -type "float3" 0.045329794 0 0.026023332 ;
	setAttr ".tk[30]" -type "float3" -0.035205919 0 -0.0087919403 ;
	setAttr ".tk[31]" -type "float3" -0.035209097 0 -0.0087920297 ;
createNode polySplit -n "polySplit5";
	rename -uid "4D2CCF00-45A0-540E-AEDB-D3B31D424718";
	setAttr -s 5 ".e[0:4]"  0.62633002 0.62633002 0.62633002 0.62633002
		 0.62633002;
	setAttr -s 5 ".d[0:4]"  -2147483596 -2147483595 -2147483593 -2147483591 -2147483596;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "3187089E-4AA3-D734-CC65-83A2A076BB7C";
	setAttr ".ics" -type "componentList" 1 "f[26]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 39.072437 8.1339998 24.06201 ;
	setAttr ".rs" 59338;
	setAttr ".lt" -type "double3" 0.31371683704613501 -7.4165163346649452e-07 0.43408944147060602 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 39.020994413239066 7.6339990463256839 23.917504134749048 ;
	setAttr ".cbx" -type "double3" 39.123881645783975 8.634 24.206516916963576 ;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "42221C61-4E58-4879-E469-7C9B0656FDB8";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[22]" -type "float2" -9.8657556e-06 -6.2496906e-06 ;
	setAttr ".uvtk[23]" -type "float2" 8.4962776e-06 9.862053e-06 ;
	setAttr ".uvtk[55]" -type "float2" 2.5039137e-12 -5.2489277e-06 ;
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "52571372-4A0A-49CA-5161-D0A7B79FC3C8";
	setAttr ".ics" -type "componentList" 2 "vtx[12]" "vtx[37]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak2";
	rename -uid "310ABE2C-4B19-2D02-0A9E-518B996969A8";
	setAttr ".uopa" yes;
	setAttr ".tk[37]" -type "float3"  0.25328827 0 -0.01243639;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "59E80ECB-446C-799F-E959-8183053DDDBB";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[30]" -type "float2" 2.4710746e-06 -8.0943346e-06 ;
	setAttr ".uvtk[31]" -type "float2" 6.8085537e-06 -2.4620979e-06 ;
	setAttr ".uvtk[56]" -type "float2" 2.0432822e-12 -5.0097869e-06 ;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "0C39D2AB-4FA1-AAAC-395B-02BD9B09F392";
	setAttr ".ics" -type "componentList" 2 "vtx[16]" "vtx[37]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak3";
	rename -uid "A416A147-4EAD-D51F-6F2F-0A9229092937";
	setAttr ".uopa" yes;
	setAttr ".tk[37]" -type "float3"  0.050743103 0 0.0020437241;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "3DC09BC1-499C-CF45-3A65-5AAE416A6C1A";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[28]" -type "float2" -9.9745594e-06 5.7622483e-06 ;
	setAttr ".uvtk[29]" -type "float2" 8.4962803e-06 -9.8209193e-06 ;
	setAttr ".uvtk[54]" -type "float2" -3.1728231e-12 5.5265828e-06 ;
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "9C882777-46CD-1DF2-0405-0EBC6FE3F732";
	setAttr ".ics" -type "componentList" 2 "vtx[15]" "vtx[36]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak4";
	rename -uid "09C0BD8D-44DE-76E0-A60E-6E93970BBB54";
	setAttr ".uopa" yes;
	setAttr ".tk[36]" -type "float3"  0.25328827 -4.7683716e-07 -0.012436867;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "D4032496-432B-224F-2A00-23A231202A75";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[36]" -type "float2" 2.0819255e-06 7.1785871e-06 ;
	setAttr ".uvtk[37]" -type "float2" 6.8085524e-06 2.4262065e-06 ;
	setAttr ".uvtk[57]" -type "float2" -2.0995428e-12 4.844891e-06 ;
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "9EF79402-417A-C206-D217-3CADF7F8FE6F";
	setAttr ".ics" -type "componentList" 2 "vtx[19]" "vtx[36]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak5";
	rename -uid "FF9CAB56-4FE9-922B-639F-3DBCC4223881";
	setAttr ".uopa" yes;
	setAttr ".tk[36]" -type "float3"  0.050743103 0 0.0020432472;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "8BF19243-4674-34F9-A079-1891A3E4CD36";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.389183 8.1339998 22.844542 ;
	setAttr ".rs" 60406;
	setAttr ".lt" -type "double3" -0.18086903289348014 0 1.0655267794379732 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.372820496623092 7.634 22.617767616515327 ;
	setAttr ".cbx" -type "double3" 40.405545071608714 8.634 23.071315393774135 ;
createNode polyTweak -n "polyTweak6";
	rename -uid "EB1A48AB-4E43-D7D7-F250-F58E8943955A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[36:39]" -type "float3"  0.27013677 0 -0.0074505378
		 0.27013853 0 -0.0074508134 0.075421058 0 -0.0042737061 0.075421058 0 -0.0042737061;
createNode polySplit -n "polySplit6";
	rename -uid "BF8C3435-476E-BDAD-6198-93B7389FAE71";
	setAttr -s 5 ".e[0:4]"  0.697442 0.697442 0.697442 0.697442 0.697442;
	setAttr -s 5 ".d[0:4]"  -2147483576 -2147483575 -2147483573 -2147483571 -2147483576;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "D6B4CEAE-44D3-02FD-4304-A484D5DDC2B0";
	setAttr ".ics" -type "componentList" 1 "f[38]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 41.405392 8.1339998 23.268272 ;
	setAttr ".rs" 63447;
	setAttr ".lt" -type "double3" 0.17455251260345156 -2.0771661918965235e-06 0.68642408020726009 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 41.221342320227762 7.634 23.233164002287353 ;
	setAttr ".cbx" -type "double3" 41.589443662797457 8.634 23.303381296842527 ;
createNode polyTweak -n "polyTweak7";
	rename -uid "56AEB813-4B7D-3AA7-6201-BFB148DFAA7B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[44:47]" -type "float3"  0.050882995 0 0.023497548
		 0.050883912 0 0.023497142 0.029361757 0 -0.0068879724 0.029359307 0 -0.0068875263;
createNode polySplit -n "polySplit7";
	rename -uid "C042558B-4AB2-6EEE-4D9B-29B9B163AFAB";
	setAttr -s 5 ".e[0:4]"  0.74301302 0.74301302 0.74301302 0.74301302
		 0.74301302;
	setAttr -s 5 ".d[0:4]"  -2147483560 -2147483559 -2147483557 -2147483555 -2147483560;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "25AF45BC-4FFE-9BEB-0501-D887CBCFA17B";
	setAttr ".ics" -type "componentList" 1 "f[46]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 41.278332 8.1339998 23.952557 ;
	setAttr ".rs" 47346;
	setAttr ".lt" -type "double3" 0.20320008946552479 4.9510411945275091e-06 0.96167921515319144 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 41.269928182846478 7.634 23.846489498175849 ;
	setAttr ".cbx" -type "double3" 41.286736694116257 8.634 24.058622436109097 ;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "54D53223-4EB9-EB76-ABAC-6D86EED034B8";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[32]" -type "float2" -7.4029135e-06 -4.9827627e-06 ;
	setAttr ".uvtk[33]" -type "float2" -5.2965779e-06 -6.2496861e-06 ;
	setAttr ".uvtk[77]" -type "float2" 1.4654944e-14 2.5766456e-06 ;
createNode polyMergeVert -n "polyMergeVert5";
	rename -uid "9DC1D0EC-4A8F-D4F8-6CF2-C4AA6B49C62B";
	setAttr ".ics" -type "componentList" 2 "vtx[17]" "vtx[55]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak8";
	rename -uid "4C2BAC93-4240-D61E-0F0E-2F812EA5AA56";
	setAttr ".uopa" yes;
	setAttr ".tk[55]" -type "float3"  -0.10253143 2.6702881e-05 0.016180038;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "8CEE6D14-4FC1-FCBD-4BDA-0ABC7EF1EBBB";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[24]" -type "float2" -7.1888899e-06 8.4062203e-06 ;
	setAttr ".uvtk[25]" -type "float2" 8.1909975e-06 -8.0643867e-06 ;
	setAttr ".uvtk[74]" -type "float2" 8.8817842e-16 1.9375989e-06 ;
createNode polyMergeVert -n "polyMergeVert6";
	rename -uid "B873837F-46EE-1824-D35D-B6B1F5A9DF3A";
	setAttr ".ics" -type "componentList" 2 "vtx[13]" "vtx[52]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak9";
	rename -uid "0A2064C1-42FA-7F36-F524-E095C52DEDC4";
	setAttr ".uopa" yes;
	setAttr ".tk[52]" -type "float3"  -0.049949646 2.6702881e-05 -0.012509346;
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "B16C4290-44F7-3EF6-43CB-D5B92998D996";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[34]" -type "float2" -7.4029149e-06 5.027015e-06 ;
	setAttr ".uvtk[35]" -type "float2" -5.3060271e-06 5.5053379e-06 ;
	setAttr ".uvtk[76]" -type "float2" -1.2101431e-14 1.1425961e-05 ;
createNode polyMergeVert -n "polyMergeVert7";
	rename -uid "37BFFE88-4B6B-E27E-B7BC-2FB524CC7394";
	setAttr ".ics" -type "componentList" 2 "vtx[18]" "vtx[53]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak10";
	rename -uid "F03FED4B-40C8-3854-5651-4EAB93D89307";
	setAttr ".uopa" yes;
	setAttr ".tk[53]" -type "float3"  -0.10253906 2.6702881e-05 0.016180038;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "EDB6AD1C-4FB1-FE70-BCB6-DBAD01EC2FCC";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[26]" -type "float2" -7.1888999e-06 -8.4900075e-06 ;
	setAttr ".uvtk[27]" -type "float2" 8.2866718e-06 7.3913648e-06 ;
	setAttr ".uvtk[75]" -type "float2" -1.5210055e-14 1.1768549e-05 ;
createNode polyMergeVert -n "polyMergeVert8";
	rename -uid "409122BE-4B0A-D2C7-6267-B6B49B50278B";
	setAttr ".ics" -type "componentList" 2 "vtx[14]" "vtx[52]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak11";
	rename -uid "E8ED2446-4CAA-91D8-62E3-F6B16A34183E";
	setAttr ".uopa" yes;
	setAttr ".tk[52]" -type "float3"  -0.049957275 2.6702881e-05 -0.012509823;
createNode polySplit -n "polySplit8";
	rename -uid "8BA16B64-4DD4-6C4E-D4AA-EEBADD293C1C";
	setAttr -s 5 ".e[0:4]"  0.916785 0.916785 0.916785 0.916785 0.916785;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit9";
	rename -uid "78F1E6FC-40C1-8DAD-AF38-A3B19BE868EA";
	setAttr -s 5 ".e[0:4]"  0.68218702 0.68218702 0.68218702 0.68218702
		 0.68218702;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "258AD2C3-4AEB-ACE4-46AD-AC9F141EFB3C";
	setAttr ".ics" -type "componentList" 1 "f[7]";
	setAttr ".ix" -type "matrix" 2.3744627400460288 0 -1.3282687360112662 0 0 1 0 0 0.2145734965883162 0 0.38357958659807601 0
		 37.121340891327158 8.1339380822017979 24.156188993103534 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 37.657776 8.1339378 23.604303 ;
	setAttr ".rs" 52080;
	setAttr ".lt" -type "double3" -0.090057278203238583 6.8511171649295631e-17 0.55943617128624989 ;
	setAttr ".ls" -type "double3" 0.81564861297264191 1 1 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 37.311856509879043 7.6339380822017979 23.410796712335248 ;
	setAttr ".cbx" -type "double3" 38.0036946003097 8.6339380822017979 23.797809273283228 ;
createNode polyTweak -n "polyTweak12";
	rename -uid "2A314FA6-43FE-66DE-60E0-CDB53F9C8C4E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  0.0073486548 0 -0.1675899
		 0.0073486548 0 -0.1675899 -0.071959831 0 -0.15266564 -0.071959831 0 -0.15266564;
createNode polySplit -n "polySplit10";
	rename -uid "DF62DE44-4B87-224A-F495-A196FBAEC503";
	setAttr -s 5 ".e[0:4]"  0.62723202 0.62723202 0.62723202 0.62723202
		 0.62723202;
	setAttr -s 5 ".d[0:4]"  -2147483620 -2147483619 -2147483617 -2147483615 -2147483620;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "4945BBA5-4865-B711-0DFF-C5BF67E14F55";
	setAttr ".ics" -type "componentList" 1 "f[14]";
	setAttr ".ix" -type "matrix" 2.3744627400460288 0 -1.3282687360112662 0 0 1 0 0 0.2145734965883162 0 0.38357958659807601 0
		 37.121340891327158 8.1339380822017979 24.156188993103534 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 37.062523 8.1339378 23.347076 ;
	setAttr ".rs" 43559;
	setAttr ".lt" -type "double3" -5.8286708792820718e-16 0 0.28053576210432468 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 37.005406186429333 7.6339380822017979 23.243822435398439 ;
	setAttr ".cbx" -type "double3" 37.119642384429127 8.6339380822017979 23.450331722812816 ;
createNode polySplit -n "polySplit11";
	rename -uid "7C1F3A0D-4AFD-DD3F-3ED0-22872709FD95";
	setAttr -s 5 ".e[0:4]"  0.080734201 0.080734201 0.080734201 0.080734201
		 0.080734201;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit12";
	rename -uid "46BEBC23-4F04-3CED-AFAE-5D8259BAF45C";
	setAttr -s 5 ".e[0:4]"  0.325059 0.325059 0.325059 0.325059 0.325059;
	setAttr -s 5 ".d[0:4]"  -2147483636 -2147483633 -2147483634 -2147483635 -2147483636;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace9";
	rename -uid "313E11FA-4CF4-222B-8F03-418CEB7815B4";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 2.4739701257220279 0 1.5160697009244832 0 0 1 0 0 -0.229649013814859 0 0.37474846917198978 0
		 43.060645657123281 8.1339380822017979 24.212521676713546 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 42.507851 8.1339378 23.616024 ;
	setAttr ".rs" 34114;
	setAttr ".lt" -type "double3" 0.021940991413775812 -8.6727650231017532e-17 0.70818500723147859 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 42.138219083580118 7.6339380822017979 23.389511255993988 ;
	setAttr ".cbx" -type "double3" 42.877479970932434 8.6339380822017979 23.842536545483405 ;
createNode polyTweak -n "polyTweak13";
	rename -uid "83FDF482-4452-C173-9746-46A3087BF6BE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  0.080196641 0 0.11353464 0.080196641
		 0 0.11353464 -0.04387695 0 0.035910793 -0.04387695 0 0.035910793;
createNode polySplit -n "polySplit13";
	rename -uid "D00BB3C6-4924-C1D8-7294-F6B987613FD1";
	setAttr -s 5 ".e[0:4]"  0.71032202 0.71032202 0.71032202 0.71032202
		 0.71032202;
	setAttr -s 5 ".d[0:4]"  -2147483620 -2147483619 -2147483617 -2147483615 -2147483620;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace10";
	rename -uid "9F4C35DF-43C9-DE0F-971D-228768403BFF";
	setAttr ".ics" -type "componentList" 1 "f[16]";
	setAttr ".ix" -type "matrix" 2.4739701257220279 0 1.5160697009244832 0 0 1 0 0 -0.229649013814859 0 0.37474846917198978 0
		 43.060645657123281 8.1339380822017979 24.212521676713546 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 43.110027 8.1339378 23.290594 ;
	setAttr ".rs" 55615;
	setAttr ".lt" -type "double3" -2.55351295663786e-15 1.7763568394002505e-15 0.37443218950912815 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 43.070640357172238 7.6339380822017979 23.1971110048319 ;
	setAttr ".cbx" -type "double3" 43.149414347207518 8.6339380822017979 23.384076303933586 ;
createNode polySplit -n "polySplit14";
	rename -uid "14C367F3-4E65-7FCB-BACE-918F290870DF";
	setAttr -s 5 ".e[0:4]"  0.630992 0.630992 0.630992 0.630992 0.630992;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "84D8D994-4F9B-48E8-C60B-CD9F48EEE1B8";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[3]" -type "float2" 0.021144945 0.0049174791 ;
	setAttr ".uvtk[5]" -type "float2" -1.3802838e-05 -0.1766874 ;
	setAttr ".uvtk[11]" -type "float2" -0.24983208 -1.2498751e-05 ;
createNode polyMergeVert -n "polyMergeVert9";
	rename -uid "0313FDC5-4629-9CF5-507B-BCB18C926EAE";
	setAttr ".ics" -type "componentList" 2 "vtx[3]" "vtx[5]";
	setAttr ".ix" -type "matrix" 1.4958855812571832 0 2.2973455901960489 0 0 1 0 0 -0.32585519666296325 0 0.21217621429967515 0
		 68.356030282481854 8.1339380822017979 30.801219831522857 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak14";
	rename -uid "AB3EDF4C-44A4-4B88-E505-98A9E2CCE2C2";
	setAttr ".uopa" yes;
	setAttr ".tk[5]" -type "float3"  0 0 0.27264681;
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "D252F393-4533-4AA5-D5AA-C6BED9BEB09B";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[1]" -type "float2" 0.001331414 0.0043468326 ;
	setAttr ".uvtk[6]" -type "float2" 0.0013222856 0.10419864 ;
	setAttr ".uvtk[8]" -type "float2" -3.3849748e-05 -0.049492702 ;
	setAttr ".uvtk[9]" -type "float2" -0.24966392 2.7564702e-05 ;
createNode polyMergeVert -n "polyMergeVert10";
	rename -uid "5BBF1CEC-403A-4538-5EBC-B18483DDBF05";
	setAttr ".ics" -type "componentList" 2 "vtx[1]" "vtx[6]";
	setAttr ".ix" -type "matrix" 1.4958855812571832 0 2.2973455901960489 0 0 1 0 0 -0.32585519666296325 0 0.21217621429967515 0
		 68.356030282481854 8.1339380822017979 30.801219831522857 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak15";
	rename -uid "14F759D0-459A-9D34-E8A0-86ACC3002608";
	setAttr ".uopa" yes;
	setAttr ".tk[6]" -type "float3"  0 0 0.27264678;
createNode polySplit -n "polySplit15";
	rename -uid "14A1BA48-4F0F-D6D8-BD30-7AB4735743AF";
	setAttr -s 5 ".e[0:4]"  0.31725901 0.31725901 0.31725901 0.31725901
		 0.31725901;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "9CBEC59A-4B4C-6A89-AAC9-94B4A4873367";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[2]" -type "float2" -0.011069423 0.00089004711 ;
	setAttr ".uvtk[4]" -type "float2" 1.6870341e-05 -0.20937406 ;
	setAttr ".uvtk[13]" -type "float2" 0.24992362 -1.249875e-05 ;
createNode polyMergeVert -n "polyMergeVert11";
	rename -uid "57C75513-44DD-F45F-4089-23A396729C68";
	setAttr ".ics" -type "componentList" 2 "vtx[2]" "vtx[4]";
	setAttr ".ix" -type "matrix" 1.3962019922090048 0 -2.359256329854384 0 0 1 0 0 0.33463660784158994 0 0.19803710712660405 0
		 11.907662487407872 8.1339380822017979 30.475115101190404 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak16";
	rename -uid "B5603FFF-4304-0D42-D7DE-F7B21D6BE592";
	setAttr ".uopa" yes;
	setAttr ".tk[4]" -type "float3"  0 0 0.40442085;
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "8798B3F3-42B5-A51B-8BBB-C8AB3020E03A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.00067390472 0.0023112274 ;
	setAttr ".uvtk[5]" -type "float2" -0.00077222177 0.11577363 ;
	setAttr ".uvtk[7]" -type "float2" 3.9360886e-05 -0.027217207 ;
	setAttr ".uvtk[11]" -type "float2" 0.24984698 2.6362066e-05 ;
createNode polyMergeVert -n "polyMergeVert12";
	rename -uid "8DFAC15F-4D1C-AFF3-170D-A887F05B1D83";
	setAttr ".ics" -type "componentList" 2 "vtx[0]" "vtx[5]";
	setAttr ".ix" -type "matrix" 1.3962019922090048 0 -2.359256329854384 0 0 1 0 0 0.33463660784158994 0 0.19803710712660405 0
		 11.907662487407872 8.1339380822017979 30.475115101190404 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak17";
	rename -uid "FDC8AB8A-4A22-FDD5-98A1-FFA67CF4A39E";
	setAttr ".uopa" yes;
	setAttr ".tk[5]" -type "float3"  0 0 0.40442085;
createNode polySplit -n "polySplit16";
	rename -uid "34ADC5C8-4C5F-2693-30BE-FD8F2A96D5D9";
	setAttr -s 5 ".e[0:4]"  0.247458 0.247458 0.247458 0.247458 0.247458;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit18";
	rename -uid "144D19AC-4D30-4E6E-8898-2EBC163400B6";
	setAttr -s 5 ".e[0:4]"  0.85897702 0.85897702 0.85897702 0.85897702
		 0.85897702;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit19";
	rename -uid "36A4A0A1-480A-60A2-A744-849656703D6D";
	setAttr -s 5 ".e[0:4]"  0.810588 0.810588 0.810588 0.810588 0.810588;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit20";
	rename -uid "4E9ACCD4-4C6C-AB8C-E5A0-619E2408DDBF";
	setAttr -s 5 ".e[0:4]"  0.798576 0.798576 0.798576 0.798576 0.798576;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit21";
	rename -uid "0FBE313C-4156-3914-3B9D-8AA41CEE602E";
	setAttr -s 5 ".e[0:4]"  0.48192799 0.48192799 0.48192799 0.48192799
		 0.48192799;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit22";
	rename -uid "8B7E1DE3-4CBC-CBEF-3570-CCBB5EFD9BCE";
	setAttr -s 5 ".e[0:4]"  0.52803999 0.52803999 0.52803999 0.52803999
		 0.52803999;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace11";
	rename -uid "C80F8EAC-4049-947C-90B2-9A813DA3FF80";
	setAttr ".ics" -type "componentList" 1 "f[7]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 28.320358 8.1339378 20.052691 ;
	setAttr ".rs" 54462;
	setAttr ".lt" -type "double3" -1.056792674228328 3.962134111387086e-16 0.69933931845952846 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 28.238336981442561 7.6339380822017979 19.994918383541272 ;
	setAttr ".cbx" -type "double3" 28.40238001402351 8.6339380822017979 20.110463841361376 ;
createNode polySplit -n "polySplit23";
	rename -uid "D5A68292-4A46-6666-0EF1-4F89131782A3";
	setAttr -s 5 ".e[0:4]"  0.792862 0.792862 0.792862 0.792862 0.792862;
	setAttr -s 5 ".d[0:4]"  -2147483588 -2147483587 -2147483585 -2147483583 -2147483588;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace12";
	rename -uid "27F7DE11-432F-C9D5-A542-B7AC758FBC95";
	setAttr ".ics" -type "componentList" 1 "f[30]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 27.824846 8.1339378 18.936857 ;
	setAttr ".rs" 65450;
	setAttr ".lt" -type "double3" -0.47523218488719099 -7.3938025508433328e-18 0.25054275788771452 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 27.777072850176012 7.6339380822017979 18.814614143491735 ;
	setAttr ".cbx" -type "double3" 27.872618304063323 8.6339380822017979 19.059100455249141 ;
createNode polyExtrudeFace -n "polyExtrudeFace13";
	rename -uid "FB955257-4453-CA83-0854-BAB2A3EE2FE6";
	setAttr ".ics" -type "componentList" 1 "f[30]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 27.391375 8.1339378 18.504021 ;
	setAttr ".rs" 43136;
	setAttr ".lt" -type "double3" 0.14604918909057243 5.2627789248468995e-15 0.54131193908322417 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 27.37148280330015 7.6339380822017979 18.449497454604927 ;
	setAttr ".cbx" -type "double3" 27.411265077760525 8.6339380822017979 18.558542554951053 ;
createNode polyTweak -n "polyTweak18";
	rename -uid "B05E006A-4685-179D-68C5-8384BE623306";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk";
	setAttr ".tk[32]" -type "float3" 9.3132257e-09 0 0 ;
	setAttr ".tk[33]" -type "float3" 9.3132257e-09 0 0 ;
	setAttr ".tk[34]" -type "float3" -9.3132257e-09 0 -1.4901161e-08 ;
	setAttr ".tk[35]" -type "float3" -9.3132257e-09 0 0 ;
	setAttr ".tk[40]" -type "float3" -0.028414058 0 -0.15318131 ;
	setAttr ".tk[41]" -type "float3" -0.028414058 0 -0.15318131 ;
	setAttr ".tk[42]" -type "float3" 0.0043930421 0 -0.078546979 ;
	setAttr ".tk[43]" -type "float3" 0.0043930421 0 -0.078546979 ;
createNode polyExtrudeFace -n "polyExtrudeFace14";
	rename -uid "3CEA07C2-4F5C-F6F1-9DCE-D8BB58355D90";
	setAttr ".ics" -type "componentList" 1 "f[30]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 26.932905 8.1339378 18.181288 ;
	setAttr ".rs" 40888;
	setAttr ".lt" -type "double3" -0.024810404048379829 -1.80979606818331e-17 0.14778106384986761 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 26.921886080256705 7.6339380822017979 18.139361039705037 ;
	setAttr ".cbx" -type "double3" 26.943923674215057 8.6339380822017979 18.22321591454277 ;
createNode polyTweak -n "polyTweak19";
	rename -uid "0008522A-4098-57C3-783B-3F9AACE6E00D";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[44:47]" -type "float3"  0 0 -0.035052847 0 0 -0.035052847
		 0 0 0.035052847 0 0 0.035052847;
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "55A30E10-460C-B656-B700-D3BE73371146";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[61:62]" -type "float2" 3.6082248e-13 4.1656285e-06
		 -2.9504177e-13 2.8080674e-06;
createNode polyMergeVert -n "polyMergeVert13";
	rename -uid "23E3981B-4B39-2773-703C-E2B3F4436928";
	setAttr ".ics" -type "componentList" 1 "vtx[49:50]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak20";
	rename -uid "A633F7C7-4D5A-310D-5B02-869365713A65";
	setAttr ".uopa" yes;
	setAttr ".tk[50]" -type "float3"  0.0052647591 0 0.1848526;
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "D1E7DA34-4BB3-5811-0496-4EB2ACD56694";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[60]" -type "float2" 3.7492232e-13 -4.8756729e-06 ;
	setAttr ".uvtk[62]" -type "float2" 5.5777605e-13 -4.0793666e-06 ;
createNode polyMergeVert -n "polyMergeVert14";
	rename -uid "AA7AD363-489F-C19F-F73A-E7BEE0E3DF07";
	setAttr ".ics" -type "componentList" 2 "vtx[48]" "vtx[50]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak21";
	rename -uid "ACA82AA6-4CFB-5D4F-0A29-A5B0178628AD";
	setAttr ".uopa" yes;
	setAttr ".tk[50]" -type "float3"  0.0052647591 0 0.1848526;
createNode polyExtrudeFace -n "polyExtrudeFace15";
	rename -uid "F42624ED-4192-B8E5-7D44-A49E42A83FC4";
	setAttr ".ics" -type "componentList" 1 "f[15]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 27.967506 8.1339378 19.804159 ;
	setAttr ".rs" 32928;
	setAttr ".lt" -type "double3" -0.76368676538819558 2.405324471033273e-16 0.42455351811843201 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 27.885938034987959 7.6339380822017979 19.746704257394612 ;
	setAttr ".cbx" -type "double3" 28.049074821040357 8.6339380822017979 19.861613441598216 ;
createNode polyTweak -n "polyTweak22";
	rename -uid "7102AFE1-4545-3A9D-DC6B-06BCAF678903";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[48:49]" -type "float3"  -0.0079342909 0 -0.040157028
		 -0.0079342909 0 -0.040157028;
createNode polySplit -n "polySplit24";
	rename -uid "7B02B27C-4BFA-C7C4-BD07-1AACAB18ABCC";
	setAttr -s 5 ".e[0:4]"  0.78090203 0.78090203 0.78090203 0.78090203
		 0.78090203;
	setAttr -s 5 ".d[0:4]"  -2147483551 -2147483550 -2147483548 -2147483546 -2147483551;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace16";
	rename -uid "BD174721-4E99-9684-A055-D4B9304E77D8";
	setAttr ".ics" -type "componentList" 1 "f[49]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 27.54768 8.1339378 19.046034 ;
	setAttr ".rs" 51628;
	setAttr ".lt" -type "double3" -0.46094853140720349 3.5507906249566455e-15 0.23996618622314003 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 27.506065558124671 7.6339380822017979 18.959833972829234 ;
	setAttr ".cbx" -type "double3" 27.589294796311744 8.6339380822017979 19.132235635601038 ;
createNode polyExtrudeFace -n "polyExtrudeFace17";
	rename -uid "F1D4F737-46B1-9C3C-A7B2-AC8C4ACA38B4";
	setAttr ".ics" -type "componentList" 1 "f[49]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 27.186129 8.1339378 18.742128 ;
	setAttr ".rs" 58974;
	setAttr ".lt" -type "double3" -2.3592239273284576e-16 3.4984505803349592e-15 0.44309182454338458 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 27.175768772144668 7.6339380822017979 18.701401472407156 ;
	setAttr ".cbx" -type "double3" 27.196487054830158 8.6339380822017979 18.782853610742613 ;
createNode polyTweak -n "polyTweak23";
	rename -uid "A32A2A1F-4922-AAD8-51BF-26B63D943149";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[58:61]" -type "float3"  -0.003443199 0 -0.075711668
		 -0.003443199 0 -0.075711668 0.020445976 0 -0.042727936 0.020445976 0 -0.042727936;
createNode polyExtrudeFace -n "polyExtrudeFace18";
	rename -uid "99673858-46E3-4449-7E74-DE9DCBC43083";
	setAttr ".ics" -type "componentList" 1 "f[49]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 26.756706 8.1339378 18.632906 ;
	setAttr ".rs" 53256;
	setAttr ".lt" -type "double3" 2.643718577388654e-15 -2.1467947036416368e-17 0.17604284042323223 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 26.753185796968591 7.6339380822017979 18.594720937462323 ;
	setAttr ".cbx" -type "double3" 26.760228173698323 8.6339380822017979 18.671090552310144 ;
createNode polyTweak -n "polyTweak24";
	rename -uid "B72C3A8F-4682-A6B7-6BAD-1C9CA2127211";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[62:65]" -type "float3"  0.001719033 0 -0.022913948
		 0.001719033 0 -0.022913948 -0.001719033 0 0.022913948 -0.001719033 0 0.022913948;
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "B4422ECA-4D78-BD16-9856-F9A324F9C587";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[79:80]" -type "float2" -4.5519144e-13 4.1538678e-06
		 2.3536728e-14 2.8506761e-06;
createNode polyMergeVert -n "polyMergeVert15";
	rename -uid "D4C1258D-475D-A2BE-94EF-B8B8ECD4E0F9";
	setAttr ".ics" -type "componentList" 1 "vtx[67:68]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak25";
	rename -uid "EC981F5A-436B-EFF8-1795-ECA1AC108C5C";
	setAttr ".uopa" yes;
	setAttr ".tk[68]" -type "float3"  0.0086503029 0 0.13283157;
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "31C3D543-4464-4D46-08F3-3C9FDBC7C5E0";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[78]" -type "float2" -1.140199e-13 -4.7533317e-06 ;
	setAttr ".uvtk[80]" -type "float2" 5.943579e-13 -4.0078367e-06 ;
createNode polyMergeVert -n "polyMergeVert16";
	rename -uid "42BAC6E1-41E1-8612-26E7-EBB7B03BD0CD";
	setAttr ".ics" -type "componentList" 2 "vtx[66]" "vtx[68]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak26";
	rename -uid "103885DF-4073-0E3B-1356-FA9567DF23D6";
	setAttr ".uopa" yes;
	setAttr ".tk[68]" -type "float3"  0.0086503029 0 0.13283157;
createNode polyExtrudeFace -n "polyExtrudeFace19";
	rename -uid "0041F794-40DE-D783-A0A7-2593B543C2FA";
	setAttr ".ics" -type "componentList" 1 "f[23]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 27.477295 8.1339378 19.458872 ;
	setAttr ".rs" 33714;
	setAttr ".lt" -type "double3" -0.47292633382926191 9.8838883901496939e-17 0.17445627977689718 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 27.403737889343425 7.6339380822017979 19.407061447134293 ;
	setAttr ".cbx" -type "double3" 27.550851241854485 8.6339380822017979 19.510682304417294 ;
createNode polyTweak -n "polyTweak27";
	rename -uid "AFE118EE-4C8F-B7CF-065A-CAAB395A8C28";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[66:67]" -type "float3"  -0.0056817913 0 -0.024109088
		 -0.0056817913 0 -0.024109088;
createNode polySplit -n "polySplit25";
	rename -uid "D0088A96-467D-C4E6-65A2-E8ADD17D08EA";
	setAttr -s 5 ".e[0:4]"  0.68297398 0.68297398 0.68297398 0.68297398
		 0.68297398;
	setAttr -s 5 ".d[0:4]"  -2147483514 -2147483513 -2147483511 -2147483509 -2147483514;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace20";
	rename -uid "1F345ED1-402E-C073-324E-F69728B4E18E";
	setAttr ".ics" -type "componentList" 1 "f[68]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 27.16292 8.1339378 19.057875 ;
	setAttr ".rs" 39850;
	setAttr ".lt" -type "double3" -0.29816671219914287 -4.2756607177067468e-18 0.24804396630817171 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 27.117555597011219 7.6339380822017979 18.992097970142108 ;
	setAttr ".cbx" -type "double3" 27.208282920700388 8.6339380822017979 19.123652245417013 ;
createNode polyExtrudeFace -n "polyExtrudeFace21";
	rename -uid "EA8AEB52-4177-D055-BD30-8196BB70BD50";
	setAttr ".ics" -type "componentList" 1 "f[68]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 26.789444 8.1339378 18.953245 ;
	setAttr ".rs" 54270;
	setAttr ".lt" -type "double3" -0.076706502455334391 -1.1089150537409174e-17 0.13197379019333735 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 26.771448089455735 7.6339380822017979 18.911757615229682 ;
	setAttr ".cbx" -type "double3" 26.807440206262637 8.6339380822017979 18.994734087394889 ;
createNode polyTweak -n "polyTweak28";
	rename -uid "C1150D9B-4491-F66B-1CAC-2CA4C0347DC3";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[76:79]" -type "float3"  -0.0063239262 0 -0.0093211057
		 -0.0063239262 0 -0.0093211057 0.0063239262 0 0.0093211066 0.0063239262 0 0.0093211066;
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "8F685BDE-44B5-F1AE-E29D-699A9B53840E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[93:94]" -type "float2" 2.220446e-16 4.1618059e-06
		 9.9920072e-15 2.8171976e-06;
createNode polyMergeVert -n "polyMergeVert17";
	rename -uid "F282555B-490B-4BDC-FCD9-AC9E2103E7C6";
	setAttr ".ics" -type "componentList" 1 "vtx[81:82]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak29";
	rename -uid "1C3D2DC9-4190-07DD-8E75-228D5209397C";
	setAttr ".uopa" yes;
	setAttr ".tk[82]" -type "float3"  0.013427734 0 0.10718155;
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "5A6652F5-4A9C-07DF-90DE-A5959FED0C24";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[92]" -type "float2" -2.2737368e-13 -4.83243e-06 ;
	setAttr ".uvtk[94]" -type "float2" -2.264855e-13 -4.0688428e-06 ;
createNode polyMergeVert -n "polyMergeVert18";
	rename -uid "496D53D5-4A19-5CD7-5460-D590579AE380";
	setAttr ".ics" -type "componentList" 2 "vtx[80]" "vtx[82]";
	setAttr ".ix" -type "matrix" 4.7007414286518499 0 3.3110173100431988 0 0 1 0 0 -0.25309691940664431 0 0.35932858789658478 0
		 29.462966284993581 8.1339380822017979 21.126299131368977 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak30";
	rename -uid "AD045392-49AB-6D4E-B193-8A9FB22861EB";
	setAttr ".uopa" yes;
	setAttr ".tk[82]" -type "float3"  0.013427734 0 0.10718155;
createNode polyExtrudeFace -n "polyExtrudeFace22";
	rename -uid "78A7BD95-4AD5-04AE-8E54-F0A75C497891";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 39.744595 8.1339998 27.543211 ;
	setAttr ".rs" 37990;
	setAttr ".lt" -type "double3" 2.2065682614424986e-15 -1.7763568394002505e-15 0.78717265392305202 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 39.440670343801145 7.634 27.521277318786659 ;
	setAttr ".cbx" -type "double3" 40.048519680089207 8.634 27.565143745459178 ;
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "B2464FB5-42CD-3A49-562B-DC83500D09E5";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[80:81]" -type "float2" -0.11952429 -3.4377138e-06
		 0.080747224 -3.8526427e-06;
createNode polyMergeVert -n "polyMergeVert19";
	rename -uid "A61BFD48-4003-36EB-1062-FABD8DAEFFF3";
	setAttr ".ics" -type "componentList" 1 "vtx[54:55]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak31";
	rename -uid "8AAC96C3-4732-1B51-EA5D-B886DD13F85A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[52:55]" -type "float3"  0.65126079 0 -0.0072333422
		 -0.22487234 0 -0.0072333422 -0.34873921 0 -0.0072342958 0.65126079 0 -0.0072333422;
createNode polyTweakUV -n "polyTweakUV20";
	rename -uid "8FF4F76C-422F-85BF-FBD5-EEA30843A0B0";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[78]" -type "float2" 0.085716859 3.3655349e-06 ;
	setAttr ".uvtk[79]" -type "float2" -0.12277848 2.8133447e-06 ;
createNode polyMergeVert -n "polyMergeVert20";
	rename -uid "CF055DBD-4824-FC60-7B0E-6FB935411AF8";
	setAttr ".ics" -type "componentList" 1 "vtx[52:53]";
	setAttr ".ix" -type "matrix" 0.60784950652661596 0 0.043864067579428574 0 0 1 0 0
		 -0.35701611647210202 0 4.9473767982564079 0 39.923102985061952 8.1340000000000003 25.069523312541261 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak32";
	rename -uid "24415690-4A52-8C66-9395-04ABFA5C4616";
	setAttr ".uopa" yes;
	setAttr ".tk[53]" -type "float3"  -0.12386686 0 -9.5367432e-07;
createNode polyCube -n "polyCube4";
	rename -uid "FB3DEC15-4B16-8BF5-1AFF-108721A635A9";
	setAttr ".cuv" 4;
createNode polyTweak -n "polyTweak33";
	rename -uid "D8C4E246-4530-FF70-BE03-54A072C43857";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[0]" -type "float3" -0.039796829 0 -0.25559735 ;
	setAttr ".tk[2]" -type "float3" -0.039796829 0 -0.25559735 ;
createNode polySplit -n "polySplit26";
	rename -uid "25CA7DB8-4077-5228-1C1A-EAA1CC2D4174";
	setAttr -s 5 ".e[0:4]"  0.085812502 0.085812502 0.085812502 0.085812502
		 0.085812502;
	setAttr -s 5 ".d[0:4]"  -2147483648 -2147483645 -2147483646 -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace23";
	rename -uid "E7200730-4222-8347-B188-E799939AF227";
	setAttr ".ics" -type "componentList" 1 "f[9]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 37.278999 8.1339998 27.920904 ;
	setAttr ".rs" 51613;
	setAttr ".lt" -type "double3" -2.0412287636116875 -1.7763568394002505e-15 3.4847582686915763 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 37.181939108164947 7.634 27.872117329437785 ;
	setAttr ".cbx" -type "double3" 37.376061604457604 8.634 27.969689717668846 ;
createNode polyExtrudeFace -n "polyExtrudeFace24";
	rename -uid "5B218C1B-4C9A-95AE-F089-B2BB747BF1AD";
	setAttr ".ics" -type "componentList" 1 "f[9]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 37.020184 8.1339998 31.951185 ;
	setAttr ".rs" 33435;
	setAttr ".lt" -type "double3" -0.034665299744849221 0 1.2644642246851512 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.959282001013911 7.634 31.937721441187627 ;
	setAttr ".cbx" -type "double3" 37.081083769635434 8.634 31.964648135495473 ;
createNode polyTweak -n "polyTweak34";
	rename -uid "B258BDAC-425A-3E35-1A93-4FBCE7E3FF89";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  0.025076849 0 -0.19055392
		 -0.025076849 0 0.19055392 -0.025076849 0 0.19055392 0.025076849 0 -0.19055392;
createNode polyTweak -n "polyTweak35";
	rename -uid "F7FD7D72-4D72-DB81-8BA6-CAA9BF7F96EB";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  -0.026734814 0 -0.7906034
		 0.072083473 0 0.21540111 0.072083473 0 0.21540111 -0.026734814 0 -0.7906034;
createNode polySplit -n "polySplit27";
	rename -uid "C92DE0AB-4928-FAEF-73E3-41AAA4476971";
	setAttr -s 7 ".e[0:6]"  0.2 0.5 0.80000001 0.80000001 0.5 0.2 0.2;
	setAttr -s 7 ".d[0:6]"  -2147483614 -2147483614 -2147483614 -2147483618 -2147483618 -2147483618 
		-2147483614;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit28";
	rename -uid "74D0F71E-4141-35D3-E6C2-E99EB57D5CF4";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483612 -2147483609;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit29";
	rename -uid "C0B217C8-4E17-3D2E-3CAC-928C9547174E";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483622 -2147483614;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit30";
	rename -uid "8E557B56-47CC-1896-DB0B-F295F462AF42";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483611 -2147483622;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit31";
	rename -uid "690E85E3-44F6-49E9-866B-AAB0702F3214";
	setAttr -s 2 ".e[0:1]"  0 1;
	setAttr -s 2 ".d[0:1]"  -2147483622 -2147483612;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit32";
	rename -uid "EBC2FD3D-43E5-B8C2-BAFE-D0B6B7335729";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483609 -2147483626;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit33";
	rename -uid "20A2F5CC-462F-1A5C-F485-FE9C9E5054B1";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483626 -2147483607;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit34";
	rename -uid "09102066-4DD8-24B5-FC40-339E3D76D0E9";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483608 -2147483626;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace25";
	rename -uid "01A33F30-4D58-2DD1-71AC-29A07441B529";
	setAttr ".ics" -type "componentList" 1 "f[18]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.563873 8.1339998 33.064594 ;
	setAttr ".rs" 38950;
	setAttr ".lt" -type "double3" -0.17146453843869292 0 0.46229902305141035 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.521014934200203 7.634 33.05532675670262 ;
	setAttr ".cbx" -type "double3" 36.606733650101624 8.634 33.073860459926216 ;
createNode polyExtrudeFace -n "polyExtrudeFace26";
	rename -uid "275D2437-42E4-103A-AD7C-DCA14762716D";
	setAttr ".ics" -type "componentList" 1 "f[18]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.298576 8.1339998 33.480213 ;
	setAttr ".rs" 33628;
	setAttr ".lt" -type "double3" -0.06381105898069965 0 0.44864076945615333 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.254769420957821 7.634 33.478302940691123 ;
	setAttr ".cbx" -type "double3" 36.342385818118174 8.634 33.482121701331423 ;
createNode polyTweak -n "polyTweak36";
	rename -uid "5C48A7E2-49F1-5E65-77F0-60A3BB3C0DDD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[26:29]" -type "float3"  -0.0024888979 0 0.047144908
		 0.0024888979 0 -0.047144908 0.0024888979 0 -0.047144908 -0.0024888979 0 0.047144908;
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "60E630B7-4011-C568-FB59-5D9B93BCBD90";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[39:40]" -type "float2" -0.0042368383 -2.7933702e-06
		 4.2182095e-05 -4.1515091e-06;
createNode polyMergeVert -n "polyMergeVert21";
	rename -uid "D621A604-4742-BFF7-DC78-FEBBDF517F66";
	setAttr ".ics" -type "componentList" 1 "vtx[32:33]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak37";
	rename -uid "6C9830AF-4372-1928-8398-DB8B030E1F5A";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[32:33]" -type "float3"  -0.03255558 0 -0.18751526
		 0 0 0;
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "D90478C8-4F8A-E986-EECF-CAB44F69DDBC";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[37:38]" -type "float2" 5.6728441e-05 4.8376214e-06
		 -0.0042283791 4.0994155e-06;
createNode polyMergeVert -n "polyMergeVert22";
	rename -uid "FE0331F5-4F0F-C6A8-1F00-5EBFDA2D122E";
	setAttr ".ics" -type "componentList" 1 "vtx[30:31]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak38";
	rename -uid "E7EAFB46-46D1-20A4-C22E-51B466E527A6";
	setAttr ".uopa" yes;
	setAttr ".tk[31]" -type "float3"  -0.03255558 0 -0.18751526;
createNode polyExtrudeFace -n "polyExtrudeFace27";
	rename -uid "AFD2382F-4F71-1064-DD84-478EE633F82A";
	setAttr ".ics" -type "componentList" 1 "f[19]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.671024 8.1339998 33.087761 ;
	setAttr ".rs" 47703;
	setAttr ".lt" -type "double3" -0.056524538519153289 0 0.4917338970738252 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.606734006832916 7.634 33.073861135275557 ;
	setAttr ".cbx" -type "double3" 36.735313329244576 8.634 33.101664053833638 ;
createNode polyExtrudeFace -n "polyExtrudeFace28";
	rename -uid "37965C26-4243-D2DF-BCE9-B9B8A93137C9";
	setAttr ".ics" -type "componentList" 1 "f[19]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.494167 8.1339998 33.556442 ;
	setAttr ".rs" 63809;
	setAttr ".lt" -type "double3" 0.05655621932024308 0 0.51351177548158344 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.451511383106592 7.634 33.546464574695136 ;
	setAttr ".cbx" -type "double3" 36.536822127858159 8.634 33.566421773151859 ;
createNode polyTweak -n "polyTweak39";
	rename -uid "B85A3ABA-46C3-720F-D971-E19E4CDCC74F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[32:35]" -type "float3"  0.00068094628 0 0.026535138
		 0.00068094628 0 0.026535138 -0.013528552 0 -0.10904955 -0.013528552 0 -0.10904955;
createNode polyTweakUV -n "polyTweakUV23";
	rename -uid "8D485808-4658-2203-D3C4-11956992E32E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[43]" -type "float2" 6.3070984e-05 -4.1510675e-06 ;
	setAttr ".uvtk[46]" -type "float2" -0.0063566728 -2.7924707e-06 ;
createNode polyMergeVert -n "polyMergeVert23";
	rename -uid "A343A978-4DDA-3010-D135-27BA35A04F46";
	setAttr ".ics" -type "componentList" 2 "vtx[37]" "vtx[39]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak40";
	rename -uid "05F3584C-4F80-7A42-C2DF-16A7472FC60F";
	setAttr ".uopa" yes;
	setAttr ".tk[39]" -type "float3"  -0.027157784 0 -0.28713989;
createNode polyTweakUV -n "polyTweakUV24";
	rename -uid "13FB7D7D-4B85-64AF-9794-E78F00C2237E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[44:45]" -type "float2" 8.4207328e-05 4.8383422e-06
		 -0.00634446 4.1005464e-06;
createNode polyMergeVert -n "polyMergeVert24";
	rename -uid "1A7E7311-4222-068C-C5C4-0081793142D3";
	setAttr ".ics" -type "componentList" 2 "vtx[36]" "vtx[38]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak41";
	rename -uid "BDA67371-4E0B-D3A6-7C27-B7A9467AE08C";
	setAttr ".uopa" yes;
	setAttr ".tk[38]" -type "float3"  -0.027157784 0 -0.28713989;
createNode polyExtrudeFace -n "polyExtrudeFace29";
	rename -uid "720765E5-487E-82D8-DBE2-C4A571532F00";
	setAttr ".ics" -type "componentList" 1 "f[18]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.799603 8.1339998 33.115562 ;
	setAttr ".rs" 50670;
	setAttr ".lt" -type "double3" 0.050475671463033979 0 0.52538435861531252 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.735313329244576 7.634 33.101664053833638 ;
	setAttr ".cbx" -type "double3" 36.86389185068618 8.634 33.129460485229878 ;
createNode polyExtrudeFace -n "polyExtrudeFace30";
	rename -uid "0BC5CC9E-4DCD-7DCB-ED0F-13A8B60CCEE7";
	setAttr ".ics" -type "componentList" 1 "f[18]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.746761 8.1339998 33.64859 ;
	setAttr ".rs" 48221;
	setAttr ".lt" -type "double3" -0.022743443524966669 0 0.44553228129908123 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.70353280476688 7.634 33.640313023668064 ;
	setAttr ".cbx" -type "double3" 36.789991428532623 8.634 33.656866105688003 ;
createNode polyTweak -n "polyTweak42";
	rename -uid "42D5C049-456D-896C-DE85-C282D551FCEE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[38:41]" -type "float3"  0.0080852974 1.1641532e-10
		 0.13363676 0.0080852974 0 0.13363676 -0.0050546643 -1.1641532e-10 -0.014273118 -0.0050546643
		 0 -0.014273118;
createNode polyTweakUV -n "polyTweakUV25";
	rename -uid "E503B491-495D-E0CA-E377-2C91CFB30525";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[49]" -type "float2" 0.0063802907 -2.791623e-06 ;
	setAttr ".uvtk[52]" -type "float2" -3.8086626e-05 -4.1596172e-06 ;
createNode polyMergeVert -n "polyMergeVert25";
	rename -uid "D497B0CE-4596-DCAA-3EBA-53BB60A59357";
	setAttr ".ics" -type "componentList" 2 "vtx[42]" "vtx[45]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak43";
	rename -uid "B2E7A909-4D3A-B4FE-8B46-1A957C0F41BC";
	setAttr ".uopa" yes;
	setAttr ".tk[42]" -type "float3"  0.028227806 0 0.27478027;
createNode polyTweakUV -n "polyTweakUV26";
	rename -uid "23B8CA9D-4306-2DB8-D36A-ED850D89DAED";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk[50:51]" -type "float2" 0.0063699395 4.1043636e-06
		 -5.505735e-05 4.8582365e-06;
createNode polyMergeVert -n "polyMergeVert26";
	rename -uid "35598CE1-4781-9D65-67B4-F2A47E9B9C65";
	setAttr ".ics" -type "componentList" 1 "vtx[43:44]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak44";
	rename -uid "00FE1A90-45D3-E273-437F-D1A455803FFD";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[43:44]" -type "float3"  0.028227806 0 0.27478027 0
		 0 0;
createNode polyExtrudeFace -n "polyExtrudeFace31";
	rename -uid "073CA718-40ED-8D81-C174-B4931DF55E4D";
	setAttr ".ics" -type "componentList" 1 "f[9]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.906757 8.1339998 33.138733 ;
	setAttr ".rs" 43864;
	setAttr ".lt" -type "double3" 0.1659075430107641 0 0.51601169437960048 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.86389185068618 7.634 33.129460485229878 ;
	setAttr ".cbx" -type "double3" 36.94962319014541 8.634 33.1480056598717 ;
createNode polyExtrudeFace -n "polyExtrudeFace32";
	rename -uid "54230BA1-46CB-2F42-A244-7380D9E53C38";
	setAttr ".ics" -type "componentList" 1 "f[9]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 36.977512 8.1339998 33.704685 ;
	setAttr ".rs" 33200;
	setAttr ".lt" -type "double3" 0.034319650720328415 0 0.3872734099627414 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.936926748182614 7.634 33.688068418751641 ;
	setAttr ".cbx" -type "double3" 37.018100657734436 8.634 33.721298690769359 ;
createNode polyTweak -n "polyTweak45";
	rename -uid "B4BEDCE0-4680-4411-F250-E0A079C23ADD";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[44:47]" -type "float3"  0.0035704554 0 0.13129802
		 0.0035704554 0 0.13129802 -0.00090234634 0 0.18553562 -0.00090234634 0 0.18553562;
createNode polyTweakUV -n "polyTweakUV27";
	rename -uid "626BDEC9-4E97-CBF5-579F-2CA9BE5B4DC0";
	setAttr ".uopa" yes;
	setAttr -s 2 ".uvtk";
	setAttr ".uvtk[55]" -type "float2" 0.0042585009 -2.7905928e-06 ;
	setAttr ".uvtk[58]" -type "float2" -2.0834828e-05 -4.1613744e-06 ;
createNode polyMergeVert -n "polyMergeVert27";
	rename -uid "5F5E6409-4532-172B-A7E1-EF83C62116BC";
	setAttr ".ics" -type "componentList" 2 "vtx[48]" "vtx[51]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak46";
	rename -uid "6A3D618C-4742-5CDB-025D-1D83298F064F";
	setAttr ".uopa" yes;
	setAttr ".tk[48]" -type "float3"  0.023107529 0 0.33610535;
createNode polyTweakUV -n "polyTweakUV28";
	rename -uid "A8922921-4135-6EC4-D5A7-67A6327A452C";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[56]" -type "float2" 0.0042521884 4.1063781e-06 ;
	setAttr ".uvtk[57]" -type "float2" -3.1037554e-05 4.8641532e-06 ;
createNode polyMergeVert -n "polyMergeVert28";
	rename -uid "A880293D-4428-0E63-B4B4-B59B42546661";
	setAttr ".ics" -type "componentList" 1 "vtx[49:50]";
	setAttr ".ix" -type "matrix" 2.1526009857354311 0 -1.1370413613658727 0 0 1 0 0 0.093514967646555702 0 0.17703877657993047 0
		 38.321050989219941 8.1340000000000003 27.312649650486385 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak47";
	rename -uid "F5F9D062-4D01-6BCD-A304-53ADE7F505D7";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[49:50]" -type "float3"  0.023107529 0 0.33610535 0
		 0 0;
createNode polyCube -n "polyCube5";
	rename -uid "BD71E1BC-4427-BDD3-A411-2A8628C40EC9";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace33";
	rename -uid "96AAC04B-4026-BA7D-F6EB-0394489E0ED7";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 0.38024690508130543 0 0 0 0 1 0 0 0 0 1 0 40.072732140389448 8.1340000000000003 21.992629752141664 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.136562 8.1339998 21.540503 ;
	setAttr ".rs" 62151;
	setAttr ".lt" -type "double3" 7.2164496600635175e-16 -5.9124077157945837e-17 0.48278472780160231 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 39.975561121464828 7.634 21.439382359964082 ;
	setAttr ".cbx" -type "double3" 40.297564705475502 8.634 21.641623303872773 ;
createNode polyTweak -n "polyTweak48";
	rename -uid "CAF0309A-41D6-3768-D36B-FCBD2F39DB56";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[4:7]" -type "float3"  0.24445285 -3.6082248e-15
		 -0.053247381 0.09128046 -3.6082248e-15 0.14899357 0.24445285 3.6082248e-15 -0.053247381
		 0.09128046 3.6082248e-15 0.14899357;
createNode polyExtrudeFace -n "polyExtrudeFace34";
	rename -uid "AC1038FC-4800-3BBC-8921-4CA4D44EA56E";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 0.38024690508130543 0 0 0 0 1 0 0 0 0 1 0 40.072732140389448 8.1340000000000003 21.992629752141664 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.568871 8.1339998 21.131666 ;
	setAttr ".rs" 56610;
	setAttr ".lt" -type "double3" 5.8564264548977008e-15 -3.4476160518438329e-17 0.28151921470290031 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.453163443693903 7.634 20.98080574033899 ;
	setAttr ".cbx" -type "double3" 40.684578408872369 8.634 21.282528028470704 ;
createNode polyTweak -n "polyTweak49";
	rename -uid "0C6A856D-4A27-5735-2035-8B808031112C";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  0.58075386 -1.9984014e-15
		 -0.04974021 0.34251282 9.3132058e-10 0.049740214 0.34251285 9.3132257e-10 0.049740214
		 0.58075386 9.3132257e-10 -0.049740218;
createNode polyExtrudeFace -n "polyExtrudeFace35";
	rename -uid "C95E8103-4FB7-53DD-FCE6-DE8789151566";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 0.38024690508130543 0 0 0 0 1 0 0 0 0 1 0 40.072732140389448 8.1340000000000003 21.992629752141664 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.887997 8.1339998 20.864592 ;
	setAttr ".rs" 57134;
	setAttr ".lt" -type "double3" -1.8152146452621309e-14 -6.7280067261458538e-17 0.54938344107298165 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.707182512397424 7.634 20.805824623090455 ;
	setAttr ".cbx" -type "double3" 41.068809911153146 8.634 20.923357830030152 ;
createNode polyTweak -n "polyTweak50";
	rename -uid "E46D3B6B-4C39-E2F8-33AF-C6807F875428";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[12:15]" -type "float3"  0.080578089 -3.7192471e-15
		 -0.0036520027 0.42302197 -3.7192471e-15 -0.18784034 0.42302197 3.7192471e-15 -0.18784034
		 0.080578089 3.7192471e-15 -0.0036520027;
createNode polyExtrudeFace -n "polyExtrudeFace36";
	rename -uid "D331441E-4264-D0DF-96EA-958E6C3BEFFE";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 0.38024690508130543 0 0 0 0 1 0 0 0 0 1 0 40.072732140389448 8.1340000000000003 21.992629752141664 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 41.025894 8.1339998 20.310196 ;
	setAttr ".rs" 55919;
	setAttr ".lt" -type "double3" -0.12253727447247502 1.7134803501475814e-15 0.51342549783697711 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.835864005564517 7.634 20.304167137128541 ;
	setAttr ".cbx" -type "double3" 41.215924336632426 8.634 20.316224679929444 ;
createNode polyTweak -n "polyTweak51";
	rename -uid "9737D8CC-45F3-E689-1AB4-1BB40D658DA5";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[16:19]" -type "float3"  -0.10816693 -2.7200464e-15
		 0.032880709 -0.05969974 -2.7200464e-15 -0.096711487 -0.05969974 2.7200464e-15 -0.096711487
		 -0.10816693 2.7200464e-15 0.032880709;
createNode polyExtrudeFace -n "polyExtrudeFace37";
	rename -uid "DB44A2E9-4B96-D52B-3D44-D2AC5CE24268";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 0.38024690508130543 0 0 0 0 1 0 0 0 0 1 0 40.072732140389448 8.1340000000000003 21.992629752141664 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.887135 8.1339998 19.800915 ;
	setAttr ".rs" 33648;
	setAttr ".lt" -type "double3" -0.28860080824161105 -7.0215108540802633e-17 0.57334987189522013 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.719901230253363 7.634 19.710468754750917 ;
	setAttr ".cbx" -type "double3" 41.054371276438602 8.634 19.891360268575379 ;
createNode polyTweak -n "polyTweak52";
	rename -uid "5C123D7D-47CF-DF9C-433F-3A88EA9327E7";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[20:23]" -type "float3"  0.059949487 -2.9976022e-15
		 0.084416643 -0.059949487 -2.9976022e-15 -0.084416635 -0.059949487 2.9976022e-15 -0.084416635
		 0.059949487 2.9976022e-15 0.084416643;
createNode polyExtrudeFace -n "polyExtrudeFace38";
	rename -uid "ADBFB6E2-46C0-BA57-F86C-4E949F93FAEC";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 0.38024690508130543 0 0 0 0 1 0 0 0 0 1 0 40.072732140389448 8.1340000000000003 21.992629752141664 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.408405 8.1339998 19.401976 ;
	setAttr ".rs" 40497;
	setAttr ".lt" -type "double3" -0.056993858118665114 -4.1696836219886894e-17 0.34048050628897941 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.278271474766775 7.634 19.263363585454652 ;
	setAttr ".cbx" -type "double3" 40.538537055943863 8.634 19.540587649327943 ;
createNode polyTweak -n "polyTweak53";
	rename -uid "263B57A3-4B9B-0CDD-C227-BE89CF417178";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[24:27]" -type "float3"  0.22347175 -2.0539126e-15
		 0.016250107 0.028328251 -2.0539126e-15 -0.080080897 0.028328251 2.0539126e-15 -0.080080897
		 0.22347175 2.0539126e-15 0.016250107;
createNode polyCube -n "polyCube6";
	rename -uid "F7D9703B-41FD-E23B-869B-2484F65C7E2F";
	setAttr ".cuv" 4;
createNode polyExtrudeFace -n "polyExtrudeFace39";
	rename -uid "1A06ACB3-41F4-F639-FE0A-28B1C01FB3C3";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 37.559048 8.1339998 17.721563 ;
	setAttr ".rs" 49742;
	setAttr ".lt" -type "double3" -0.0081440222523045391 -9.2871973121690219e-17 0.758357211126925 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 37.444004157015584 7.634 17.477754916778192 ;
	setAttr ".cbx" -type "double3" 37.674089410731099 8.634 17.965372707477197 ;
createNode polyTweak -n "polyTweak54";
	rename -uid "0F76A012-451F-5486-9D6A-808A35762332";
	setAttr ".uopa" yes;
	setAttr -s 8 ".tk[0:7]" -type "float3"  1.21331048 -1.7763568e-15
		 0.42788801 -1.051156759 -1.7763568e-15 -0.82752162 1.21331048 -1.7763568e-15 0.42788801
		 -1.051156759 -1.7763568e-15 -0.82752162 -7.0086340904 0 -2.28596616 -1.28124201 0
		 -0.31513944 -7.0086340904 0 -2.28596616 -1.28124201 0 -0.31513944;
createNode polyTweak -n "polyTweak55";
	rename -uid "E0D7BE64-42EC-53E3-D574-A897D5978084";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[8:11]" -type "float3"  4.86801434 0 0.45708996 -0.43423569
		 0 0.15998155 4.86801434 0 0.45708996 -0.43423569 0 0.15998155;
createNode polySplit -n "polySplit35";
	rename -uid "F96CAF32-4E28-6904-5663-CAA4F52AC323";
	setAttr -s 4 ".e[0:3]"  0.2 0.79940999 0.00073699502 0;
	setAttr -s 4 ".d[0:3]"  -2147483630 -2147483634 -2147483636 -2147483639;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit36";
	rename -uid "9FFB287F-45D8-CC91-0C20-2491DC8B7ED9";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483630 -2147483633;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV29";
	rename -uid "6B724D7D-4B61-5F46-0C76-69A824B7B513";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[7]" -type "float2" 0.018951813 0.035789162 ;
	setAttr ".uvtk[10]" -type "float2" -0.032509468 -0.020583408 ;
	setAttr ".uvtk[20]" -type "float2" -0.1112209 -0.056064907 ;
createNode polyMergeVert -n "polyMergeVert29";
	rename -uid "6723800D-4513-D6A0-BF0A-28AE27EA0335";
	setAttr ".ics" -type "componentList" 2 "vtx[7]" "vtx[14]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak56";
	rename -uid "25F6FE27-44EA-4B43-ACA5-D3BAB1A95793";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[9]" -type "float3" -4.4703484e-08 1.4901161e-08 -6.3329935e-08 ;
	setAttr ".tk[13]" -type "float3" 3.7252903e-09 -1.6763806e-08 -1.4901161e-08 ;
	setAttr ".tk[14]" -type "float3" -0.0040906072 0 -9.2983246e-05 ;
createNode polyExtrudeFace -n "polyExtrudeFace40";
	rename -uid "18DDA22D-4A36-F6FB-4A21-2DB5ED4DC723";
	setAttr ".ics" -type "componentList" 1 "f[4]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.965519 8.1339998 17.680061 ;
	setAttr ".rs" 36746;
	setAttr ".lt" -type "double3" 0.2650539248961204 -5.5164898605300623e-09 0.25009691832978842 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 38.936653354594014 7.634 17.603858496776208 ;
	setAttr ".cbx" -type "double3" 42.994383552500508 8.634 17.756265725722894 ;
createNode polySplit -n "polySplit37";
	rename -uid "11A1BD88-4029-F75B-06ED-8E9698DD0FFE";
	setAttr -s 2 ".e[0:1]"  0.15430699 0.84631699;
	setAttr -s 2 ".d[0:1]"  -2147483616 -2147483619;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit38";
	rename -uid "D744487C-4577-F0DC-5270-3E8718E6EE83";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483616 -2147483629;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit39";
	rename -uid "3ABBCB03-4830-77EC-8DAF-E7BD6DF8D3FE";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483619 -2147483635;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace41";
	rename -uid "4D8103F6-4C79-D5D4-AD58-4EBDBAF7BFA6";
	setAttr ".ics" -type "componentList" 1 "f[17]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 39.523975 8.1339998 17.984478 ;
	setAttr ".rs" 51872;
	setAttr ".lt" -type "double3" 0.26248056294042915 1.4261322245513214e-08 0.31861209391154993 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 39.2109067229717 7.634 17.972719218364343 ;
	setAttr ".cbx" -type "double3" 39.837045410105489 8.634 17.996236767402277 ;
createNode polyTweakUV -n "polyTweakUV30";
	rename -uid "C27F59B3-43EB-70BB-8C8F-CC87A1758D85";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 0.0044248211 0.0015807465 ;
	setAttr ".uvtk[2]" -type "float2" 0.017638147 0.0062982272 ;
	setAttr ".uvtk[8]" -type "float2" 0.016526844 0.031127186 ;
	setAttr ".uvtk[28]" -type "float2" -0.0064633815 2.9882328e-06 ;
	setAttr ".uvtk[29]" -type "float2" -0.0060353507 -3.6158997e-06 ;
createNode polyMergeVert -n "polyMergeVert30";
	rename -uid "23DEBD38-43E9-71C1-427B-7B92253A5412";
	setAttr ".ics" -type "componentList" 3 "vtx[0]" "vtx[2]" "vtx[22:23]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak57";
	rename -uid "1054CC59-4F51-0E34-E32E-E1BC5B0A7B5C";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[22]" -type "float3" -1.1727409 0 0.93952751 ;
	setAttr ".tk[23]" -type "float3" -1.1727409 0 0.93952751 ;
createNode polySplit -n "polySplit40";
	rename -uid "E1E816EA-4883-38FF-B874-B29ED44677C8";
	setAttr -s 2 ".e[0:1]"  0.69999999 0.69999999;
	setAttr -s 2 ".d[0:1]"  -2147483647 -2147483648;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace42";
	rename -uid "D71E1F1A-46B7-7BFE-C5AB-4AAE57320405";
	setAttr ".ics" -type "componentList" 1 "f[24]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 38.495995 8.1339998 18.781389 ;
	setAttr ".rs" 36264;
	setAttr ".lt" -type "double3" 0.64282494180885774 -2.7103708688831248e-17 0.22131857697829208 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 38.053430700132154 7.634 18.341995429030046 ;
	setAttr ".cbx" -type "double3" 38.938556411692403 8.634 19.220783199897394 ;
createNode polyTweakUV -n "polyTweakUV31";
	rename -uid "E5AC3257-42DD-8631-D4E8-30ACA81D4FB8";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[26]" -type "float2" 0.010923338 -3.2660016e-06 ;
	setAttr ".uvtk[27]" -type "float2" 0.0098775178 3.4768614e-06 ;
	setAttr ".uvtk[33]" -type "float2" -0.0045798835 0.00039484713 ;
	setAttr ".uvtk[36]" -type "float2" -0.0053279148 -0.00019967977 ;
createNode polyMergeVert -n "polyMergeVert31";
	rename -uid "F9CBFFC3-49F4-5E07-E016-619E43D0C58E";
	setAttr ".ics" -type "componentList" 3 "vtx[20:21]" "vtx[24]" "vtx[27]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak58";
	rename -uid "FDD6994A-4F73-5B06-FE11-11BB5A668BC1";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[24]" -type "float3" -0.062507629 0 -1.2119751 ;
	setAttr ".tk[27]" -type "float3" -0.065502167 0 -1.2118626 ;
createNode polyExtrudeFace -n "polyExtrudeFace43";
	rename -uid "BE8EE026-4E88-0946-AA82-8187D467674E";
	setAttr ".ics" -type "componentList" 1 "f[15]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 42.852058 8.1339998 17.800058 ;
	setAttr ".rs" 48880;
	setAttr ".lt" -type "double3" -3.1086244689504383e-15 0 1.3321969851499897 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 42.709729888865255 7.634 17.603858914008722 ;
	setAttr ".cbx" -type "double3" 42.994384506174825 8.634 17.99625965558587 ;
createNode polyTweak -n "polyTweak59";
	rename -uid "E16D0F13-4196-AE5C-4E62-018D9ADF607B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[16]" -type "float3" -0.5589084 0 0.15242957 ;
	setAttr ".tk[17]" -type "float3" -0.5589084 0 0.15242957 ;
	setAttr ".tk[20]" -type "float3" -2.3841858e-07 0 1.7881393e-07 ;
createNode polyExtrudeFace -n "polyExtrudeFace44";
	rename -uid "232EDCA9-43B1-CB60-09A7-96AA98BA28E7";
	setAttr ".ics" -type "componentList" 1 "f[15]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 45.746857 8.1339998 18.836357 ;
	setAttr ".rs" 58727;
	setAttr ".lt" -type "double3" -1.0352829704629585e-14 -3.7968859985702014e-15 1.9938182987919684 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 45.724051216074727 7.634 18.58934774600564 ;
	setAttr ".cbx" -type "double3" 45.769662597605489 8.634 19.083368267646417 ;
createNode polyTweak -n "polyTweak60";
	rename -uid "7CC9407D-4530-B28A-3822-9E9888E6349E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[26:29]" -type "float3"  1.65132034 0 0.20323938 1.65132034
		 0 0.20323938 1.98158431 0 0.30485913 1.98158431 0 0.30485913;
createNode polyExtrudeFace -n "polyExtrudeFace45";
	rename -uid "598FE445-4E45-9BFB-54B5-75955A0E91A5";
	setAttr ".ics" -type "componentList" 1 "f[15]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 47.426922 8.1339998 18.865456 ;
	setAttr ".rs" 46248;
	setAttr ".lt" -type "double3" -4.5310977192514201e-15 3.5174191917214026e-15 0.28820135816850706 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 47.390843131968282 7.634 18.64499321185647 ;
	setAttr ".cbx" -type "double3" 47.463000038096212 8.634 19.085916366210565 ;
createNode polyTweak -n "polyTweak61";
	rename -uid "D0C6978C-454D-EF97-A2AB-3EB141481EAA";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[30:33]" -type "float3"  -0.31858373 0 0.23893778 -0.31858373
		 0 0.23893778 -0.29203507 0 0.1858405 -0.29203507 0 0.1858405;
createNode polyExtrudeFace -n "polyExtrudeFace46";
	rename -uid "00FED342-46AF-F344-E90B-D98B384A2583";
	setAttr ".ics" -type "componentList" 1 "f[31]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 44.239697 8.1339998 18.539814 ;
	setAttr ".rs" 53554;
	setAttr ".lt" -type "double3" -3.0531133177191805e-15 1.7763568394002505e-15 1.2414385692643732 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 42.709731796213887 7.634 17.99625965558587 ;
	setAttr ".cbx" -type "double3" 45.769660690256856 8.634 19.083368267646417 ;
createNode polyTweak -n "polyTweak62";
	rename -uid "A2474126-42E0-658F-3E1D-319E03FF10C8";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[34:37]" -type "float3"  -0.017516064 -1.1641532e-10
		 0.15806687 -0.017516058 9.3132257e-10 0.15806691 -0.078159198 0 -0.12617514 -0.078159206
		 -3.4924597e-10 -0.12617512;
createNode polyTweak -n "polyTweak63";
	rename -uid "029D6AC4-4FCB-0A27-EF46-0F8F468938C0";
	setAttr ".uopa" yes;
	setAttr -s 7 ".tk";
	setAttr ".tk[38]" -type "float3" -2.0454612 0 -1.1402767 ;
	setAttr ".tk[39]" -type "float3" -2.0454612 0 -1.1402767 ;
	setAttr ".tk[40]" -type "float3" -2.3043377 0 -0.89925373 ;
	setAttr ".tk[41]" -type "float3" -2.3043377 0 -0.89925373 ;
createNode polySplit -n "polySplit41";
	rename -uid "8D5A6AE3-4A53-D30D-22CD-EBA9FBB96E5B";
	setAttr -s 2 ".e[0:1]"  0.14055701 0.85944301;
	setAttr -s 2 ".d[0:1]"  -2147483615 -2147483619;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV32";
	rename -uid "15D823B3-4C4D-4EE0-AB85-39A3A9E74681";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[49]" -type "float2" 3.3306691e-16 -4.1644644e-06 ;
	setAttr ".uvtk[50]" -type "float2" -9.9920072e-16 3.5071207e-06 ;
	setAttr ".uvtk[53]" -type "float2" 0.0029880868 -3.4450172e-06 ;
	setAttr ".uvtk[54]" -type "float2" 0.0030447128 3.3759495e-06 ;
createNode polyMergeVert -n "polyMergeVert32";
	rename -uid "F847A12F-40D0-4AAA-9DFA-78AA264392FB";
	setAttr ".ics" -type "componentList" 2 "vtx[38:39]" "vtx[42:43]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak64";
	rename -uid "3E443B35-4EEB-3222-1072-D3A8498D0A38";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[38]" -type "float3" -0.0078470707 0 -0.049762487 ;
	setAttr ".tk[39]" -type "float3" -0.0078470707 0 -0.049762487 ;
createNode polyExtrudeFace -n "polyExtrudeFace47";
	rename -uid "96C0396F-4EE6-45D2-63DC-CFB0BCFAD34A";
	setAttr ".ics" -type "componentList" 1 "f[31]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 41.525288 8.1339998 18.365017 ;
	setAttr ".rs" 50025;
	setAttr ".lt" -type "double3" -1.2212453270876722e-15 0 0.67309761723464367 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.240819909998677 7.634 17.97602793180047 ;
	setAttr ".cbx" -type "double3" 42.809753158518575 8.634 18.754007007709131 ;
createNode polyTweak -n "polyTweak65";
	rename -uid "15915CD7-456B-1FAC-424D-E89208698B5A";
	setAttr ".uopa" yes;
	setAttr -s 3 ".tk";
	setAttr ".tk[40]" -type "float3" -0.23996633 0 -0.59991497 ;
	setAttr ".tk[41]" -type "float3" -0.23996633 0 -0.59991497 ;
createNode polyExtrudeFace -n "polyExtrudeFace48";
	rename -uid "FF92F915-4F3D-798B-E438-3389E80E8B3E";
	setAttr ".ics" -type "componentList" 1 "f[31]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.923588 8.1339998 18.675936 ;
	setAttr ".rs" 53494;
	setAttr ".lt" -type "double3" 2.4286128663675299e-15 -1.7763568394002505e-15 0.35745213046745128 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.299028613993428 7.634 18.433591451278314 ;
	setAttr ".cbx" -type "double3" 41.548147418925069 8.634 18.918278660407648 ;
createNode polyTweak -n "polyTweak66";
	rename -uid "D8E429AF-4053-F868-2CFE-7EB6CDDF7283";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[42:45]" -type "float3"  0.25329831 0 -0.18664014 0.25329831
		 0 -0.18664014 -1.066514373 0 -0.47993183 -1.066514373 0 -0.47993183;
createNode polyExtrudeFace -n "polyExtrudeFace49";
	rename -uid "8C76835B-41AD-ADE0-CE84-4397CD12E5EA";
	setAttr ".ics" -type "componentList" 1 "f[50]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 38.225246169993184 8.1340000000000003 18.292894329658136 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.487675 8.1339998 18.673536 ;
	setAttr ".rs" 41978;
	setAttr ".lt" -type "double3" -6.9388939039072284e-15 0 0.28152116101203284 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 40.299030044504903 7.634 18.433591808906183 ;
	setAttr ".cbx" -type "double3" 40.676321723887227 8.634 18.913481917014703 ;
createNode polyTweak -n "polyTweak67";
	rename -uid "D73D3FDA-4B77-D35A-74E6-1A9BA6FC3839";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[46]" -type "float3" 0.50659478 0 0.14664581 ;
	setAttr ".tk[47]" -type "float3" 0.50659478 0 0.14664581 ;
	setAttr ".tk[48]" -type "float3" -0.23996562 0 -0.066657133 ;
	setAttr ".tk[49]" -type "float3" -0.23996562 0 -0.066657133 ;
createNode polyUnite -n "polyUnite1";
	rename -uid "681586CC-4E00-2F29-B0EC-67AB2499F376";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId1";
	rename -uid "3031DB5C-419F-1DAB-352C-F8991C1D1B0C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "5776FBC4-4B84-53B8-045A-8BA5FD118E2B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:57]";
createNode groupId -n "groupId2";
	rename -uid "2F0DABED-40FE-E685-7BF5-789AF96C3834";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "6AFE123D-489B-4590-D3AF-4BB824E4D882";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "89F2543F-40A3-B7F0-C737-5D87FF0272E0";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:29]";
createNode groupId -n "groupId4";
	rename -uid "F780BCBA-4155-A1BE-A88D-E0B1ADA1746B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "441328C6-4082-C4E3-85E4-48A009CC6448";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "BF3EFB93-4820-3875-152D-ACBBD3ED5F5D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:87]";
createNode polyTweakUV -n "polyTweakUV33";
	rename -uid "77577C16-41E2-1970-7B26-76B96CD61EE8";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[65]" -type "float2" 1.9000357e-12 3.6352963e-06 ;
	setAttr ".uvtk[66]" -type "float2" -4.2398307e-12 -4.3217478e-06 ;
	setAttr ".uvtk[74]" -type "float2" -2.8583274e-05 4.1659464e-06 ;
	setAttr ".uvtk[75]" -type "float2" -3.334921e-05 -3.470893e-06 ;
createNode polyMergeVert -n "polyMergeVert33";
	rename -uid "8E118C11-4D58-97E4-0F61-15A94DC6DC1D";
	setAttr ".ics" -type "componentList" 2 "vtx[52:53]" "vtx[83:84]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak68";
	rename -uid "C8543D0F-443B-3795-A0E8-BAB56FC179AA";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[52]" -type "float3" -0.021297455 0 0.026666641 ;
	setAttr ".tk[53]" -type "float3" -0.021297455 0 0.026666641 ;
createNode polyExtrudeFace -n "polyExtrudeFace50";
	rename -uid "C67C4215-4732-DC42-4FA9-3DA6CF66046D";
	setAttr ".ics" -type "componentList" 1 "f[50]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 40.127678 8.1339998 18.959055 ;
	setAttr ".rs" 58527;
	setAttr ".lt" -type "double3" 1.8041124150158794e-15 1.7763568394002505e-15 0.31143012855456942 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 39.888301849365234 7.6339998245239258 18.817298889160156 ;
	setAttr ".cbx" -type "double3" 40.367053985595703 8.6339998245239258 19.100812911987305 ;
createNode polyTweak -n "polyTweak69";
	rename -uid "A42AC454-46A5-34FD-F305-8E8E81E19CFB";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[50:51]" -type "float3"  -0.18941568 0 0.20971021 -0.18941568
		 0 0.20971021;
createNode polyTweakUV -n "polyTweakUV34";
	rename -uid "815637D3-4315-4ECD-CE67-FCAD7E5F2F05";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[73]" -type "float2" 2.7042968e-05 5.1492671e-06 ;
	setAttr ".uvtk[108]" -type "float2" -1.1782797e-12 -3.3336628e-06 ;
createNode polyMergeVert -n "polyMergeVert34";
	rename -uid "81E43927-4193-E77E-024A-3BA1D22D1725";
	setAttr ".ics" -type "componentList" 2 "vtx[82]" "vtx[87]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak70";
	rename -uid "2D2308D7-4117-2903-86D5-E1ABAA081932";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[87]" -type "float3" -0.10157394 0 0.0092582703 ;
createNode polyTweakUV -n "polyTweakUV35";
	rename -uid "D184B958-4810-B49B-D402-D08DB682E872";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[76]" -type "float2" 3.0353913e-05 -4.2357642e-06 ;
	setAttr ".uvtk[107]" -type "float2" -7.2216677e-12 2.9906219e-06 ;
createNode polyMergeVert -n "polyMergeVert35";
	rename -uid "F275C852-4AD6-39E9-ACDC-D39B8F5B81B2";
	setAttr ".ics" -type "componentList" 2 "vtx[83]" "vtx[86]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak71";
	rename -uid "6E698F1C-4EC8-5619-7DC4-FB84BCC2812F";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[86]" -type "float3" -0.10157394 0 0.0092582703 ;
createNode polyExtrudeFace -n "polyExtrudeFace51";
	rename -uid "2A8370BC-4F25-5148-CF33-6C8568DBAF87";
	setAttr ".ics" -type "componentList" 1 "f[88]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 39.731163 8.1339998 19.086578 ;
	setAttr ".rs" 47446;
	setAttr ".lt" -type "double3" 1.2490009027033011e-15 -2.0138028489903614e-16 0.35544771386643942 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 39.574020385742188 7.6339998245239258 18.817298889160156 ;
	setAttr ".cbx" -type "double3" 39.888301849365234 8.6339998245239258 19.355859756469727 ;
createNode polyTweak -n "polyTweak72";
	rename -uid "F83006EF-4DBB-D40D-8C62-4C853498D11F";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[84:85]" -type "float3"  -0.15559144 0 0.27059385 -0.15559144
		 0 0.27059385;
createNode polyTweakUV -n "polyTweakUV36";
	rename -uid "1A4BE19D-435A-C4B4-A115-32B4EAAD0C19";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" 0.060800485 -0.00038191595 ;
	setAttr ".uvtk[2]" -type "float2" 0.057093665 -0.00277446 ;
	setAttr ".uvtk[8]" -type "float2" 0.024601242 0.046372563 ;
	setAttr ".uvtk[28]" -type "float2" -0.0015136721 2.7536998e-06 ;
	setAttr ".uvtk[29]" -type "float2" -0.0011992886 -3.401339e-06 ;
	setAttr ".uvtk[111]" -type "float2" 4.4267923e-12 5.0340959e-06 ;
	setAttr ".uvtk[112]" -type "float2" -2.3692159e-12 -5.6408803e-06 ;
createNode polyMergeVert -n "polyMergeVert36";
	rename -uid "94F569E5-4224-965B-6E97-03A1B6E20C76";
	setAttr ".ics" -type "componentList" 3 "vtx[0]" "vtx[2]" "vtx[88:89]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak73";
	rename -uid "ED3DFFBD-4EE7-8D47-9F6F-53BE8FBB9495";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[88]" -type "float3" -0.32846451 0 0.044073105 ;
	setAttr ".tk[89]" -type "float3" -0.32846451 0 0.044073105 ;
createNode polySplit -n "polySplit42";
	rename -uid "2DEC322D-4DA6-1364-EF10-899EA5AAEA3A";
	setAttr -s 2 ".e[0:1]"  0.5 0.5;
	setAttr -s 2 ".d[0:1]"  -2147483606 -2147483608;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV37";
	rename -uid "DB3F293E-44B3-B753-D696-639FC457E217";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[109]" -type "float2" 1.2267964e-13 -6.6604966e-06 ;
	setAttr ".uvtk[110]" -type "float2" -1.8972601e-12 5.7626794e-06 ;
	setAttr ".uvtk[113]" -type "float2" -0.0012600779 -3.3519441e-06 ;
	setAttr ".uvtk[114]" -type "float2" -0.0013459561 3.4092648e-06 ;
createNode polyMergeVert -n "polyMergeVert37";
	rename -uid "E5FBF0C8-494B-16FB-9570-0389C89456A8";
	setAttr ".ics" -type "componentList" 1 "vtx[86:89]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak74";
	rename -uid "1B00BF61-4275-E4A4-C902-90B45D9AB437";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[86:89]" -type "float3"  -0.19350052 0 -0.041397095
		 -0.19350052 0 -0.041397095 0 0 0 0 0 0;
createNode polySplit -n "polySplit43";
	rename -uid "6620D78E-4EC3-C51B-411A-EA9619B97773";
	setAttr -s 2 ".e[0:1]"  0.28399101 0.71600902;
	setAttr -s 2 ".d[0:1]"  -2147483642 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace52";
	rename -uid "A824C5CA-4649-FE0D-8EBA-A382131DA91D";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 37.77108 8.1339998 18.693432 ;
	setAttr ".rs" 37891;
	setAttr ".lt" -type "double3" -9.1732177409653559e-15 -1.6441260230785692e-15 0.14357736125108964 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 36.603599548339844 7.6339998245239258 18.166080474853516 ;
	setAttr ".cbx" -type "double3" 38.938556671142578 8.6339998245239258 19.220783233642578 ;
createNode polyExtrudeFace -n "polyExtrudeFace53";
	rename -uid "E5DC4F88-4A60-6E21-B574-2E85668F5B61";
	setAttr ".ics" -type "componentList" 1 "f[5]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 34.825756 8.1339998 17.613602 ;
	setAttr ".rs" 60299;
	setAttr ".lt" -type "double3" -3.7261860263981816e-15 6.4090387964994052e-16 0.17158094274685182 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 30.772062301635742 7.6339998245239258 15.87557315826416 ;
	setAttr ".cbx" -type "double3" 38.879451751708984 8.6339998245239258 19.351631164550781 ;
createNode polyTweak -n "polyTweak75";
	rename -uid "1AFCF96A-478D-D2E1-4561-8484A16FC78E";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[90:91]" -type "float3"  -5.7724328 0 -2.42135525 -5.7724328
		 0 -2.42135525;
createNode polyTweakUV -n "polyTweakUV38";
	rename -uid "BDABBFBF-4FF6-BB97-F263-9788F21956D1";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[119]" -type "float2" -0.0082809087 -9.32845e-05 ;
	setAttr ".uvtk[120]" -type "float2" -0.0083778482 -2.8797203e-05 ;
	setAttr ".uvtk[123]" -type "float2" 0.00037478196 8.8874578e-05 ;
	setAttr ".uvtk[124]" -type "float2" -0.0011155148 -0.00012995428 ;
createNode polyMergeVert -n "polyMergeVert38";
	rename -uid "0F78EEFD-4B69-F97A-F75A-A7BB33E73B32";
	setAttr ".ics" -type "componentList" 2 "vtx[90:91]" "vtx[94:95]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak76";
	rename -uid "135D91A1-4C9A-3EF8-373E-3FAD0CA33F65";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[94]" -type "float3" 0.067611694 0 -0.15769672 ;
	setAttr ".tk[95]" -type "float3" 0.067611694 0 -0.15769672 ;
createNode polySplit -n "polySplit44";
	rename -uid "1AE4A621-4375-87C6-1E54-678D81D425EC";
	setAttr -s 2 ".e[0:1]"  0.33714199 0.66285801;
	setAttr -s 2 ".d[0:1]"  -2147483453 -2147483456;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyExtrudeFace -n "polyExtrudeFace54";
	rename -uid "9A76F169-4239-2B3D-D59F-1F8FA0DBCE80";
	setAttr ".ics" -type "componentList" 1 "f[105]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 33.43668 8.1339998 17.079905 ;
	setAttr ".rs" 55587;
	setAttr ".lt" -type "double3" -4.3576253716537394e-15 -2.9082410375855412e-16 0.43918983251995208 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 30.772062301635742 7.6339998245239258 15.87557315826416 ;
	setAttr ".cbx" -type "double3" 36.101295471191406 8.6339998245239258 18.284236907958984 ;
createNode polyTweakUV -n "polyTweakUV39";
	rename -uid "74326A98-4780-0E0D-0780-448694F73C63";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[119]" -type "float2" -0.0060338462 -7.2550589e-05 ;
	setAttr ".uvtk[120]" -type "float2" -0.0059344391 -1.3978388e-05 ;
	setAttr ".uvtk[127]" -type "float2" 0.00017540545 5.0830582e-05 ;
	setAttr ".uvtk[128]" -type "float2" -0.0006764353 -7.5278032e-05 ;
createNode polyMergeVert -n "polyMergeVert39";
	rename -uid "3C3A20E2-4703-77D3-2A46-39AE50020CF2";
	setAttr ".ics" -type "componentList" 2 "vtx[90:91]" "vtx[98:99]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak77";
	rename -uid "9915F5DD-4058-99FF-8174-AE841498474A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[98]" -type "float3" 0.18088531 0 -0.40021038 ;
	setAttr ".tk[99]" -type "float3" 0.18088531 9.5367432e-07 -0.40021038 ;
createNode polyExtrudeFace -n "polyExtrudeFace55";
	rename -uid "D9A55DDB-4D35-06D4-70AC-8B9DEC1F147D";
	setAttr ".ics" -type "componentList" 1 "f[103]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 38.845646 8.1339998 19.430479 ;
	setAttr ".rs" 48860;
	setAttr ".lt" -type "double3" -6.591949208711867e-15 -1.7763568394002505e-15 0.19489931746937958 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 38.811840057373047 7.6339998245239258 19.351631164550781 ;
	setAttr ".cbx" -type "double3" 38.879451751708984 8.6339998245239258 19.509326934814453 ;
createNode polyTweak -n "polyTweak78";
	rename -uid "8A52B612-4FF0-5217-2365-0980B374C32F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[96:99]" -type "float3"  -0.020057503 0 0.18051751
		 -0.020057503 0 0.18051751 -0.21968883 0 0.0091597885 -0.21968883 0 0.0091597885;
createNode polyTweak -n "polyTweak79";
	rename -uid "6120317D-48C8-EF86-EB79-9CA2F93570AE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[100:103]" -type "float3"  0 0 0.18365963 0 0 0.18365963
		 0 0 0.057030395 0 0 0.057030395;
createNode polySplit -n "polySplit45";
	rename -uid "81557B82-41F8-A4E6-3644-A5B6575AC2F4";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483641 -2147483646;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit46";
	rename -uid "968FB269-44E6-5792-FCA0-7E850BB54056";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483647 -2147483468;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit47";
	rename -uid "09AF84DA-46D3-0EF5-8885-15977C9B4CAB";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483637 -2147483645;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit48";
	rename -uid "ABA6B43A-4B62-2A87-848A-97A65B7E2C0B";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483603 -2147483638;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit49";
	rename -uid "2393FE2D-4ABB-8BEF-F3F3-D0944AB21654";
	setAttr -s 2 ".e[0:1]"  1 1;
	setAttr -s 2 ".d[0:1]"  -2147483611 -2147483617;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit50";
	rename -uid "C052CF44-4380-CF15-68B7-7DA6DAA7A467";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483610 -2147483615;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit51";
	rename -uid "E0B2F2AD-4AF8-EC88-EB48-548835EDF063";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483613 -2147483619;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit52";
	rename -uid "2E150303-4B5B-A61E-D758-5AA8D3D5DA72";
	setAttr -s 2 ".e[0:1]"  1 0;
	setAttr -s 2 ".d[0:1]"  -2147483564 -2147483635;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyCut -n "polyCut1";
	rename -uid "63037DD1-4C19-2F41-5C5F-29864CF1C493";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "f[1:3]" "f[97:99]" "f[101:102]" "f[104:108]" "f[113:116]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pc" -type "double3" 35.256767789999998 1007.14132871 16.31413628 ;
	setAttr ".ro" -type "double3" 118.48583609000001 0 -90 ;
createNode polyCut -n "polyCut2";
	rename -uid "D10F75F5-4397-4F2E-4AD9-6489690DF6B6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[2:3]" "f[97]" "f[102]" "f[105:106]" "f[113:116]" "f[121]" "f[125:127]" "f[129]" "f[132]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pc" -type "double3" 33.475623749999997 1007.14132871 15.962049670000001 ;
	setAttr ".ro" -type "double3" -180 67.619864949999993 0 ;
createNode polySplit -n "polySplit53";
	rename -uid "4BA13ABD-446D-7BFD-12E8-51A4DD1DEFDF";
	setAttr -s 6 ".e[0:5]"  0.35252601 0.35252601 0.64747399 0.64747399
		 0.35252601 0.35252601;
	setAttr -s 6 ".d[0:5]"  -2147483435 -2147483436 -2147483428 -2147483427 -2147483434 -2147483433;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit54";
	rename -uid "A0A39AF9-48D0-154F-FA69-36906CB5396B";
	setAttr -s 5 ".e[0:4]"  1 0.81695998 0.581824 0.319493 0.530415;
	setAttr -s 5 ".d[0:4]"  -2147483435 -2147483642 -2147483458 -2147483452 -2147483445;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyTweakUV -n "polyTweakUV40";
	rename -uid "CD64F584-4425-BD9F-6365-04AA4810E74C";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[146]" -type "float2" -6.0934312e-06 -5.029824e-06 ;
	setAttr ".uvtk[179]" -type "float2" -0.008387154 -7.8303761e-05 ;
createNode polyMergeVert -n "polyMergeVert40";
	rename -uid "37016DEF-4CE7-85D9-B623-0FB5A62B1B5B";
	setAttr ".ics" -type "componentList" 2 "vtx[115]" "vtx[145]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 1e-06;
createNode polyTweak -n "polyTweak80";
	rename -uid "7E4BC55F-4F31-E2D4-33F5-A8B537B29652";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[136]" -type "float3" -4.7683716e-07 0 0 ;
	setAttr ".tk[145]" -type "float3" -0.62185669 0 -0.35555649 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 2 ".u";
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
	setAttr -s 2 ".tx";
select -ne :lambert1;
select -ne :initialShadingGroup;
	setAttr -s 122 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :initialMaterialInfo;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "ref_plane01.di" "pPlane1.do";
connectAttr "polyPlane1.out" "pPlaneShape1.i";
connectAttr "polyCube1.out" "pCubeShape1.i";
connectAttr "polyExtrudeFace8.out" "pCubeShape101.i";
connectAttr "polyMergeVert18.out" "pCubeShape103.i";
connectAttr "polyTweakUV18.uvtk[0]" "pCubeShape103.uvst[0].uvtw";
connectAttr "polyMergeVert12.out" "pCubeShape107.i";
connectAttr "polyTweakUV12.uvtk[0]" "pCubeShape107.uvst[0].uvtw";
connectAttr "polyMergeVert10.out" "pCubeShape108.i";
connectAttr "polyTweakUV10.uvtk[0]" "pCubeShape108.uvst[0].uvtw";
connectAttr "polyExtrudeFace10.out" "pCubeShape114.i";
connectAttr "polyMergeVert20.out" "pCubeShape115.i";
connectAttr "polyTweakUV20.uvtk[0]" "pCubeShape115.uvst[0].uvtw";
connectAttr "polyMergeVert28.out" "pCubeShape117.i";
connectAttr "polyTweakUV28.uvtk[0]" "pCubeShape117.uvst[0].uvtw";
connectAttr "groupParts2.og" "pCubeShape119.i";
connectAttr "groupId3.id" "pCubeShape119.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape119.iog.og[0].gco";
connectAttr "groupId4.id" "pCubeShape119.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pCubeShape120.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCubeShape120.iog.og[0].gco";
connectAttr "groupParts1.og" "pCubeShape120.i";
connectAttr "polyTweakUV32.uvtk[0]" "pCubeShape120.uvst[0].uvtw";
connectAttr "groupId2.id" "pCubeShape120.ciog.cog[0].cgid";
connectAttr "polyMergeVert40.out" "pCube121Shape.i";
connectAttr "groupId5.id" "pCube121Shape.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pCube121Shape.iog.og[0].gco";
connectAttr "polyTweakUV40.uvtk[0]" "pCube121Shape.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultColorMgtGlobals.cme" "file1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file1.ws";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "file2.oc" "lambert2.c";
connectAttr "lambert2.oc" "lambert2SG.ss";
connectAttr "pPlaneShape1.iog" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "file2.msg" "materialInfo1.t" -na;
connectAttr ":defaultColorMgtGlobals.cme" "file2.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file2.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file2.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file2.ws";
connectAttr "place2dTexture2.c" "file2.c";
connectAttr "place2dTexture2.tf" "file2.tf";
connectAttr "place2dTexture2.rf" "file2.rf";
connectAttr "place2dTexture2.mu" "file2.mu";
connectAttr "place2dTexture2.mv" "file2.mv";
connectAttr "place2dTexture2.s" "file2.s";
connectAttr "place2dTexture2.wu" "file2.wu";
connectAttr "place2dTexture2.wv" "file2.wv";
connectAttr "place2dTexture2.re" "file2.re";
connectAttr "place2dTexture2.of" "file2.of";
connectAttr "place2dTexture2.r" "file2.ro";
connectAttr "place2dTexture2.n" "file2.n";
connectAttr "place2dTexture2.vt1" "file2.vt1";
connectAttr "place2dTexture2.vt2" "file2.vt2";
connectAttr "place2dTexture2.vt3" "file2.vt3";
connectAttr "place2dTexture2.vc1" "file2.vc1";
connectAttr "place2dTexture2.o" "file2.uv";
connectAttr "place2dTexture2.ofs" "file2.fs";
connectAttr "layerManager.dli[1]" "ref_plane01.id";
connectAttr "polyCube3.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape115.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape115.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak1.ip";
connectAttr "polyTweak1.out" "polySplit5.ip";
connectAttr "polySplit5.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape115.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace3.out" "polyTweakUV1.ip";
connectAttr "polyTweak2.out" "polyMergeVert1.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert1.mp";
connectAttr "polyTweakUV1.out" "polyTweak2.ip";
connectAttr "polyMergeVert1.out" "polyTweakUV2.ip";
connectAttr "polyTweak3.out" "polyMergeVert2.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert2.mp";
connectAttr "polyTweakUV2.out" "polyTweak3.ip";
connectAttr "polyMergeVert2.out" "polyTweakUV3.ip";
connectAttr "polyTweak4.out" "polyMergeVert3.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert3.mp";
connectAttr "polyTweakUV3.out" "polyTweak4.ip";
connectAttr "polyMergeVert3.out" "polyTweakUV4.ip";
connectAttr "polyTweak5.out" "polyMergeVert4.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert4.mp";
connectAttr "polyTweakUV4.out" "polyTweak5.ip";
connectAttr "polyMergeVert4.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape115.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak6.ip";
connectAttr "polyTweak6.out" "polySplit6.ip";
connectAttr "polySplit6.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape115.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace5.out" "polyTweak7.ip";
connectAttr "polyTweak7.out" "polySplit7.ip";
connectAttr "polySplit7.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape115.wm" "polyExtrudeFace6.mp";
connectAttr "polyExtrudeFace6.out" "polyTweakUV5.ip";
connectAttr "polyTweak8.out" "polyMergeVert5.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert5.mp";
connectAttr "polyTweakUV5.out" "polyTweak8.ip";
connectAttr "polyMergeVert5.out" "polyTweakUV6.ip";
connectAttr "polyTweak9.out" "polyMergeVert6.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert6.mp";
connectAttr "polyTweakUV6.out" "polyTweak9.ip";
connectAttr "polyMergeVert6.out" "polyTweakUV7.ip";
connectAttr "polyTweak10.out" "polyMergeVert7.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert7.mp";
connectAttr "polyTweakUV7.out" "polyTweak10.ip";
connectAttr "polyMergeVert7.out" "polyTweakUV8.ip";
connectAttr "polyTweak11.out" "polyMergeVert8.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert8.mp";
connectAttr "polyTweakUV8.out" "polyTweak11.ip";
connectAttr "polyCube2.out" "polySplit8.ip";
connectAttr "polySplit8.out" "polySplit9.ip";
connectAttr "polySplit9.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape101.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak12.ip";
connectAttr "polyTweak12.out" "polySplit10.ip";
connectAttr "polySplit10.out" "polyExtrudeFace8.ip";
connectAttr "pCubeShape101.wm" "polyExtrudeFace8.mp";
connectAttr "polySurfaceShape1.o" "polySplit11.ip";
connectAttr "polySplit11.out" "polySplit12.ip";
connectAttr "polySplit12.out" "polyExtrudeFace9.ip";
connectAttr "pCubeShape114.wm" "polyExtrudeFace9.mp";
connectAttr "polyExtrudeFace9.out" "polyTweak13.ip";
connectAttr "polyTweak13.out" "polySplit13.ip";
connectAttr "polySplit13.out" "polyExtrudeFace10.ip";
connectAttr "pCubeShape114.wm" "polyExtrudeFace10.mp";
connectAttr "polySurfaceShape2.o" "polySplit14.ip";
connectAttr "polySplit14.out" "polyTweakUV9.ip";
connectAttr "polyTweak14.out" "polyMergeVert9.ip";
connectAttr "pCubeShape108.wm" "polyMergeVert9.mp";
connectAttr "polyTweakUV9.out" "polyTweak14.ip";
connectAttr "polyMergeVert9.out" "polyTweakUV10.ip";
connectAttr "polyTweak15.out" "polyMergeVert10.ip";
connectAttr "pCubeShape108.wm" "polyMergeVert10.mp";
connectAttr "polyTweakUV10.out" "polyTweak15.ip";
connectAttr "polySurfaceShape3.o" "polySplit15.ip";
connectAttr "polySplit15.out" "polyTweakUV11.ip";
connectAttr "polyTweak16.out" "polyMergeVert11.ip";
connectAttr "pCubeShape107.wm" "polyMergeVert11.mp";
connectAttr "polyTweakUV11.out" "polyTweak16.ip";
connectAttr "polyMergeVert11.out" "polyTweakUV12.ip";
connectAttr "polyTweak17.out" "polyMergeVert12.ip";
connectAttr "pCubeShape107.wm" "polyMergeVert12.mp";
connectAttr "polyTweakUV12.out" "polyTweak17.ip";
connectAttr "|pCube103|polySurfaceShape4.o" "polySplit16.ip";
connectAttr "polySplit16.out" "polySplit18.ip";
connectAttr "polySplit18.out" "polySplit19.ip";
connectAttr "polySplit19.out" "polySplit20.ip";
connectAttr "polySplit20.out" "polySplit21.ip";
connectAttr "polySplit21.out" "polySplit22.ip";
connectAttr "polySplit22.out" "polyExtrudeFace11.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace11.mp";
connectAttr "polyExtrudeFace11.out" "polySplit23.ip";
connectAttr "polySplit23.out" "polyExtrudeFace12.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace12.mp";
connectAttr "polyTweak18.out" "polyExtrudeFace13.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace13.mp";
connectAttr "polyExtrudeFace12.out" "polyTweak18.ip";
connectAttr "polyTweak19.out" "polyExtrudeFace14.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace14.mp";
connectAttr "polyExtrudeFace13.out" "polyTweak19.ip";
connectAttr "polyExtrudeFace14.out" "polyTweakUV13.ip";
connectAttr "polyTweak20.out" "polyMergeVert13.ip";
connectAttr "pCubeShape103.wm" "polyMergeVert13.mp";
connectAttr "polyTweakUV13.out" "polyTweak20.ip";
connectAttr "polyMergeVert13.out" "polyTweakUV14.ip";
connectAttr "polyTweak21.out" "polyMergeVert14.ip";
connectAttr "pCubeShape103.wm" "polyMergeVert14.mp";
connectAttr "polyTweakUV14.out" "polyTweak21.ip";
connectAttr "polyTweak22.out" "polyExtrudeFace15.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace15.mp";
connectAttr "polyMergeVert14.out" "polyTweak22.ip";
connectAttr "polyExtrudeFace15.out" "polySplit24.ip";
connectAttr "polySplit24.out" "polyExtrudeFace16.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace16.mp";
connectAttr "polyTweak23.out" "polyExtrudeFace17.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace17.mp";
connectAttr "polyExtrudeFace16.out" "polyTweak23.ip";
connectAttr "polyTweak24.out" "polyExtrudeFace18.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace18.mp";
connectAttr "polyExtrudeFace17.out" "polyTweak24.ip";
connectAttr "polyExtrudeFace18.out" "polyTweakUV15.ip";
connectAttr "polyTweak25.out" "polyMergeVert15.ip";
connectAttr "pCubeShape103.wm" "polyMergeVert15.mp";
connectAttr "polyTweakUV15.out" "polyTweak25.ip";
connectAttr "polyMergeVert15.out" "polyTweakUV16.ip";
connectAttr "polyTweak26.out" "polyMergeVert16.ip";
connectAttr "pCubeShape103.wm" "polyMergeVert16.mp";
connectAttr "polyTweakUV16.out" "polyTweak26.ip";
connectAttr "polyTweak27.out" "polyExtrudeFace19.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace19.mp";
connectAttr "polyMergeVert16.out" "polyTweak27.ip";
connectAttr "polyExtrudeFace19.out" "polySplit25.ip";
connectAttr "polySplit25.out" "polyExtrudeFace20.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace20.mp";
connectAttr "polyTweak28.out" "polyExtrudeFace21.ip";
connectAttr "pCubeShape103.wm" "polyExtrudeFace21.mp";
connectAttr "polyExtrudeFace20.out" "polyTweak28.ip";
connectAttr "polyExtrudeFace21.out" "polyTweakUV17.ip";
connectAttr "polyTweak29.out" "polyMergeVert17.ip";
connectAttr "pCubeShape103.wm" "polyMergeVert17.mp";
connectAttr "polyTweakUV17.out" "polyTweak29.ip";
connectAttr "polyMergeVert17.out" "polyTweakUV18.ip";
connectAttr "polyTweak30.out" "polyMergeVert18.ip";
connectAttr "pCubeShape103.wm" "polyMergeVert18.mp";
connectAttr "polyTweakUV18.out" "polyTweak30.ip";
connectAttr "polyMergeVert8.out" "polyExtrudeFace22.ip";
connectAttr "pCubeShape115.wm" "polyExtrudeFace22.mp";
connectAttr "polyExtrudeFace22.out" "polyTweakUV19.ip";
connectAttr "polyTweak31.out" "polyMergeVert19.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert19.mp";
connectAttr "polyTweakUV19.out" "polyTweak31.ip";
connectAttr "polyMergeVert19.out" "polyTweakUV20.ip";
connectAttr "polyTweak32.out" "polyMergeVert20.ip";
connectAttr "pCubeShape115.wm" "polyMergeVert20.mp";
connectAttr "polyTweakUV20.out" "polyTweak32.ip";
connectAttr "polyCube4.out" "polyTweak33.ip";
connectAttr "polyTweak33.out" "polySplit26.ip";
connectAttr "polySplit26.out" "polyExtrudeFace23.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace23.mp";
connectAttr "polyTweak34.out" "polyExtrudeFace24.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace24.mp";
connectAttr "polyExtrudeFace23.out" "polyTweak34.ip";
connectAttr "polyExtrudeFace24.out" "polyTweak35.ip";
connectAttr "polyTweak35.out" "polySplit27.ip";
connectAttr "polySplit27.out" "polySplit28.ip";
connectAttr "polySplit28.out" "polySplit29.ip";
connectAttr "polySplit29.out" "polySplit30.ip";
connectAttr "polySplit30.out" "polySplit31.ip";
connectAttr "polySplit31.out" "polySplit32.ip";
connectAttr "polySplit32.out" "polySplit33.ip";
connectAttr "polySplit33.out" "polySplit34.ip";
connectAttr "polySplit34.out" "polyExtrudeFace25.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace25.mp";
connectAttr "polyTweak36.out" "polyExtrudeFace26.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace26.mp";
connectAttr "polyExtrudeFace25.out" "polyTweak36.ip";
connectAttr "polyExtrudeFace26.out" "polyTweakUV21.ip";
connectAttr "polyTweak37.out" "polyMergeVert21.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert21.mp";
connectAttr "polyTweakUV21.out" "polyTweak37.ip";
connectAttr "polyMergeVert21.out" "polyTweakUV22.ip";
connectAttr "polyTweak38.out" "polyMergeVert22.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert22.mp";
connectAttr "polyTweakUV22.out" "polyTweak38.ip";
connectAttr "polyMergeVert22.out" "polyExtrudeFace27.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace27.mp";
connectAttr "polyTweak39.out" "polyExtrudeFace28.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace28.mp";
connectAttr "polyExtrudeFace27.out" "polyTweak39.ip";
connectAttr "polyExtrudeFace28.out" "polyTweakUV23.ip";
connectAttr "polyTweak40.out" "polyMergeVert23.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert23.mp";
connectAttr "polyTweakUV23.out" "polyTweak40.ip";
connectAttr "polyMergeVert23.out" "polyTweakUV24.ip";
connectAttr "polyTweak41.out" "polyMergeVert24.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert24.mp";
connectAttr "polyTweakUV24.out" "polyTweak41.ip";
connectAttr "polyMergeVert24.out" "polyExtrudeFace29.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace29.mp";
connectAttr "polyTweak42.out" "polyExtrudeFace30.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace30.mp";
connectAttr "polyExtrudeFace29.out" "polyTweak42.ip";
connectAttr "polyExtrudeFace30.out" "polyTweakUV25.ip";
connectAttr "polyTweak43.out" "polyMergeVert25.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert25.mp";
connectAttr "polyTweakUV25.out" "polyTweak43.ip";
connectAttr "polyMergeVert25.out" "polyTweakUV26.ip";
connectAttr "polyTweak44.out" "polyMergeVert26.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert26.mp";
connectAttr "polyTweakUV26.out" "polyTweak44.ip";
connectAttr "polyMergeVert26.out" "polyExtrudeFace31.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace31.mp";
connectAttr "polyTweak45.out" "polyExtrudeFace32.ip";
connectAttr "pCubeShape117.wm" "polyExtrudeFace32.mp";
connectAttr "polyExtrudeFace31.out" "polyTweak45.ip";
connectAttr "polyExtrudeFace32.out" "polyTweakUV27.ip";
connectAttr "polyTweak46.out" "polyMergeVert27.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert27.mp";
connectAttr "polyTweakUV27.out" "polyTweak46.ip";
connectAttr "polyMergeVert27.out" "polyTweakUV28.ip";
connectAttr "polyTweak47.out" "polyMergeVert28.ip";
connectAttr "pCubeShape117.wm" "polyMergeVert28.mp";
connectAttr "polyTweakUV28.out" "polyTweak47.ip";
connectAttr "polyTweak48.out" "polyExtrudeFace33.ip";
connectAttr "pCubeShape119.wm" "polyExtrudeFace33.mp";
connectAttr "polyCube5.out" "polyTweak48.ip";
connectAttr "polyTweak49.out" "polyExtrudeFace34.ip";
connectAttr "pCubeShape119.wm" "polyExtrudeFace34.mp";
connectAttr "polyExtrudeFace33.out" "polyTweak49.ip";
connectAttr "polyTweak50.out" "polyExtrudeFace35.ip";
connectAttr "pCubeShape119.wm" "polyExtrudeFace35.mp";
connectAttr "polyExtrudeFace34.out" "polyTweak50.ip";
connectAttr "polyTweak51.out" "polyExtrudeFace36.ip";
connectAttr "pCubeShape119.wm" "polyExtrudeFace36.mp";
connectAttr "polyExtrudeFace35.out" "polyTweak51.ip";
connectAttr "polyTweak52.out" "polyExtrudeFace37.ip";
connectAttr "pCubeShape119.wm" "polyExtrudeFace37.mp";
connectAttr "polyExtrudeFace36.out" "polyTweak52.ip";
connectAttr "polyTweak53.out" "polyExtrudeFace38.ip";
connectAttr "pCubeShape119.wm" "polyExtrudeFace38.mp";
connectAttr "polyExtrudeFace37.out" "polyTweak53.ip";
connectAttr "polyTweak54.out" "polyExtrudeFace39.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace39.mp";
connectAttr "polyCube6.out" "polyTweak54.ip";
connectAttr "polyExtrudeFace39.out" "polyTweak55.ip";
connectAttr "polyTweak55.out" "polySplit35.ip";
connectAttr "polySplit35.out" "polySplit36.ip";
connectAttr "polySplit36.out" "polyTweakUV29.ip";
connectAttr "polyTweak56.out" "polyMergeVert29.ip";
connectAttr "pCubeShape120.wm" "polyMergeVert29.mp";
connectAttr "polyTweakUV29.out" "polyTweak56.ip";
connectAttr "polyMergeVert29.out" "polyExtrudeFace40.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace40.mp";
connectAttr "polyExtrudeFace40.out" "polySplit37.ip";
connectAttr "polySplit37.out" "polySplit38.ip";
connectAttr "polySplit38.out" "polySplit39.ip";
connectAttr "polySplit39.out" "polyExtrudeFace41.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace41.mp";
connectAttr "polyExtrudeFace41.out" "polyTweakUV30.ip";
connectAttr "polyTweak57.out" "polyMergeVert30.ip";
connectAttr "pCubeShape120.wm" "polyMergeVert30.mp";
connectAttr "polyTweakUV30.out" "polyTweak57.ip";
connectAttr "polyMergeVert30.out" "polySplit40.ip";
connectAttr "polySplit40.out" "polyExtrudeFace42.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace42.mp";
connectAttr "polyExtrudeFace42.out" "polyTweakUV31.ip";
connectAttr "polyTweak58.out" "polyMergeVert31.ip";
connectAttr "pCubeShape120.wm" "polyMergeVert31.mp";
connectAttr "polyTweakUV31.out" "polyTweak58.ip";
connectAttr "polyTweak59.out" "polyExtrudeFace43.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace43.mp";
connectAttr "polyMergeVert31.out" "polyTweak59.ip";
connectAttr "polyTweak60.out" "polyExtrudeFace44.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace44.mp";
connectAttr "polyExtrudeFace43.out" "polyTweak60.ip";
connectAttr "polyTweak61.out" "polyExtrudeFace45.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace45.mp";
connectAttr "polyExtrudeFace44.out" "polyTweak61.ip";
connectAttr "polyTweak62.out" "polyExtrudeFace46.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace46.mp";
connectAttr "polyExtrudeFace45.out" "polyTweak62.ip";
connectAttr "polyExtrudeFace46.out" "polyTweak63.ip";
connectAttr "polyTweak63.out" "polySplit41.ip";
connectAttr "polySplit41.out" "polyTweakUV32.ip";
connectAttr "polyTweak64.out" "polyMergeVert32.ip";
connectAttr "pCubeShape120.wm" "polyMergeVert32.mp";
connectAttr "polyTweakUV32.out" "polyTweak64.ip";
connectAttr "polyTweak65.out" "polyExtrudeFace47.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace47.mp";
connectAttr "polyMergeVert32.out" "polyTweak65.ip";
connectAttr "polyTweak66.out" "polyExtrudeFace48.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace48.mp";
connectAttr "polyExtrudeFace47.out" "polyTweak66.ip";
connectAttr "polyTweak67.out" "polyExtrudeFace49.ip";
connectAttr "pCubeShape120.wm" "polyExtrudeFace49.mp";
connectAttr "polyExtrudeFace48.out" "polyTweak67.ip";
connectAttr "pCubeShape120.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape119.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape120.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape119.wm" "polyUnite1.im[1]";
connectAttr "polyExtrudeFace49.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyExtrudeFace38.out" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polyUnite1.out" "groupParts3.ig";
connectAttr "groupId5.id" "groupParts3.gi";
connectAttr "groupParts3.og" "polyTweakUV33.ip";
connectAttr "polyTweak68.out" "polyMergeVert33.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert33.mp";
connectAttr "polyTweakUV33.out" "polyTweak68.ip";
connectAttr "polyTweak69.out" "polyExtrudeFace50.ip";
connectAttr "pCube121Shape.wm" "polyExtrudeFace50.mp";
connectAttr "polyMergeVert33.out" "polyTweak69.ip";
connectAttr "polyExtrudeFace50.out" "polyTweakUV34.ip";
connectAttr "polyTweak70.out" "polyMergeVert34.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert34.mp";
connectAttr "polyTweakUV34.out" "polyTweak70.ip";
connectAttr "polyMergeVert34.out" "polyTweakUV35.ip";
connectAttr "polyTweak71.out" "polyMergeVert35.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert35.mp";
connectAttr "polyTweakUV35.out" "polyTweak71.ip";
connectAttr "polyTweak72.out" "polyExtrudeFace51.ip";
connectAttr "pCube121Shape.wm" "polyExtrudeFace51.mp";
connectAttr "polyMergeVert35.out" "polyTweak72.ip";
connectAttr "polyExtrudeFace51.out" "polyTweakUV36.ip";
connectAttr "polyTweak73.out" "polyMergeVert36.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert36.mp";
connectAttr "polyTweakUV36.out" "polyTweak73.ip";
connectAttr "polyMergeVert36.out" "polySplit42.ip";
connectAttr "polySplit42.out" "polyTweakUV37.ip";
connectAttr "polyTweak74.out" "polyMergeVert37.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert37.mp";
connectAttr "polyTweakUV37.out" "polyTweak74.ip";
connectAttr "polyMergeVert37.out" "polySplit43.ip";
connectAttr "polySplit43.out" "polyExtrudeFace52.ip";
connectAttr "pCube121Shape.wm" "polyExtrudeFace52.mp";
connectAttr "polyTweak75.out" "polyExtrudeFace53.ip";
connectAttr "pCube121Shape.wm" "polyExtrudeFace53.mp";
connectAttr "polyExtrudeFace52.out" "polyTweak75.ip";
connectAttr "polyExtrudeFace53.out" "polyTweakUV38.ip";
connectAttr "polyTweak76.out" "polyMergeVert38.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert38.mp";
connectAttr "polyTweakUV38.out" "polyTweak76.ip";
connectAttr "polyMergeVert38.out" "polySplit44.ip";
connectAttr "polySplit44.out" "polyExtrudeFace54.ip";
connectAttr "pCube121Shape.wm" "polyExtrudeFace54.mp";
connectAttr "polyExtrudeFace54.out" "polyTweakUV39.ip";
connectAttr "polyTweak77.out" "polyMergeVert39.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert39.mp";
connectAttr "polyTweakUV39.out" "polyTweak77.ip";
connectAttr "polyTweak78.out" "polyExtrudeFace55.ip";
connectAttr "pCube121Shape.wm" "polyExtrudeFace55.mp";
connectAttr "polyMergeVert39.out" "polyTweak78.ip";
connectAttr "polyExtrudeFace55.out" "polyTweak79.ip";
connectAttr "polyTweak79.out" "polySplit45.ip";
connectAttr "polySplit45.out" "polySplit46.ip";
connectAttr "polySplit46.out" "polySplit47.ip";
connectAttr "polySplit47.out" "polySplit48.ip";
connectAttr "polySplit48.out" "polySplit49.ip";
connectAttr "polySplit49.out" "polySplit50.ip";
connectAttr "polySplit50.out" "polySplit51.ip";
connectAttr "polySplit51.out" "polySplit52.ip";
connectAttr "polySplit52.out" "polyCut1.ip";
connectAttr "pCube121Shape.wm" "polyCut1.mp";
connectAttr "polyCut1.out" "polyCut2.ip";
connectAttr "pCube121Shape.wm" "polyCut2.mp";
connectAttr "polyCut2.out" "polySplit53.ip";
connectAttr "polySplit53.out" "polySplit54.ip";
connectAttr "polySplit54.out" "polyTweakUV40.ip";
connectAttr "polyTweak80.out" "polyMergeVert40.ip";
connectAttr "pCube121Shape.wm" "polyMergeVert40.mp";
connectAttr "polyTweakUV40.out" "polyTweak80.ip";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture2.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file2.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1.oc" ":lambert1.c";
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape6.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape8.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape9.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape10.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape11.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape12.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape13.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape14.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape15.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape16.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape17.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape18.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape19.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape20.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape21.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape22.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape23.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape24.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape25.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape26.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape27.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape28.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape29.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape30.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape31.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape32.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape33.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape34.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape35.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape36.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape37.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape38.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape39.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape40.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape41.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape42.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape43.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape44.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape45.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape46.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape47.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape48.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape49.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape50.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape51.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape52.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape53.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape54.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape55.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape56.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape57.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape58.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape59.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape60.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape61.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape62.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape63.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape64.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape65.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape66.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape67.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape68.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape69.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape70.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape71.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape72.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape73.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape74.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape75.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape76.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape77.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape78.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape79.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape80.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape81.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape82.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape83.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape84.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape85.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape86.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape87.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape88.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape89.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape90.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape91.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape92.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape93.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape94.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape95.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape96.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape97.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape98.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape99.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape100.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape101.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape102.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape103.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape104.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape105.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape106.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape107.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape108.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape109.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape110.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape111.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape113.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape114.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape115.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape116.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape117.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape118.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape120.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape120.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape119.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape119.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCube121Shape.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId1.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId2.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId3.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId4.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId5.msg" ":initialShadingGroup.gn" -na;
connectAttr "file1.msg" ":initialMaterialInfo.t" -na;
// End of Pteranodon_Fossil.0002.ma
