﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DinoDetector : MonoBehaviour {

    List<GameObject> tiles = new List<GameObject>();
    public List<GameObject> fossils = new List<GameObject>();
    public Collider dino, dino2;
    public Material stoneMat;
    List<Collider> boneTiles = new List<Collider>();
    public GameObject winScreen, secondWinScreen;

    float timer = 0;

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start()
    {
        SetUpFossilList();
    }

    void SetUpFossilList()
    {
        if (GM_Dupe.instance.godValue == 1)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                tiles.Add(transform.GetChild(i).gameObject);
            }

            foreach (GameObject tile in tiles)
            {
                boneTiles.AddRange(Physics.OverlapBox(tile.transform.position, tile.GetComponent<BoxCollider>().bounds.extents));

                for (int i = 0; i < boneTiles.Count; i++)
                {
                    if (boneTiles[i].gameObject == dino.gameObject)
                    {
                        tile.GetComponent<Renderer>().material = stoneMat;
                        tile.tag = "BottomFossil";
                        fossils.Add(tile);
                    }
                }
                boneTiles.Clear();
            }
        }
        if (GM_Dupe.instance.godValue == 2)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                tiles.Add(transform.GetChild(i).gameObject);
            }

            foreach (GameObject tile in tiles)
            {
                boneTiles.AddRange(Physics.OverlapBox(tile.transform.position, tile.GetComponent<BoxCollider>().bounds.extents));

                for (int i = 0; i < boneTiles.Count; i++)
                {
                    if (boneTiles[i].gameObject == dino2.gameObject)
                    {
                        tile.GetComponent<Renderer>().material = stoneMat;
                        tile.tag = "BottomFossil";
                        fossils.Add(tile);
                    }
                }
                boneTiles.Clear();
            }
        }
    }
	
	// Update is called once per frame
	void Update () {

        RaycastHit hit;

        foreach(GameObject tile in fossils)
        {
            if (Physics.Raycast(tile.transform.position, Vector3.up, out hit, 5f))
            {
                return;
            }
        }

        WinCondition();
	}

    void WinCondition()
    {
        timer += Time.deltaTime;

        if (winScreen.activeInHierarchy == false && timer > 5f && GM_Dupe.instance.godValue == 1)
        {
            winScreen.SetActive(true);
        }
        if (winScreen.activeInHierarchy == false && timer > 5f && GM_Dupe.instance.godValue == 2)
        {
            secondWinScreen.SetActive(true);
        }
        if (timer > 60)
        {
            //winScreen.SetActive(false);
            //secondWinScreen.SetActive(false);
            //GM_Dupe.instance.FossilDecider();
            //GM_Dupe.instance.BoardReset();
            //SetUpFossilList();
            SceneManager.LoadScene("DinoDig");
            timer = 0;
        }
    }
}
