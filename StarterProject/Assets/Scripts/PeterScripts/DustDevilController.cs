﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustDevilController : MonoBehaviour
{

    [HideInInspector]
    public GameObject destination;
    public float speed;
    public GM_Dupe myGM;
    bool overSand;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, destination.transform.position, Time.deltaTime * speed);

        if (transform.position == destination.transform.position)
        {
            myGM.devilSpawned = false;
            Destroy(gameObject);
        }

        if (overSand && !transform.GetChild(0).gameObject.activeInHierarchy)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else if (!overSand && transform.GetChild(0).gameObject.activeInHierarchy)
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tile")
        {
            overSand = true;
            other.gameObject.GetComponent<TO_Dupe>().ManualCheck();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Tile")
        {
            overSand = false;
        }
    }
}
