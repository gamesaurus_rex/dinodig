﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureController : MonoBehaviour {
    public GameObject common, uncommon, rare;
    public Transform[] tCommon, tUncommon, tRare;
    public Transform cArc, uArc, rArc;
    public bool[] bCommon, bUncommon, bRare;
    BoxCollider myBC;
    public List<GameObject> foundDiamonds, foundBullions, foundRubys = new List<GameObject>();

    private void Awake()
    {
        myBC = GetComponent<BoxCollider>();
    }

    // Use this for initialization
    void Start () {
        SpawnCommon();
        SpawnUncommon();
        SpawnRare();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnCommon()
    {
        Vector3 spawnLoc;
        float rX, rY, rZ;
        GameObject spawn;

        for (int i = 0; i < 7; i++)
        {
            rX = Random.Range(transform.position.x - myBC.bounds.extents.x, transform.position.x + myBC.bounds.extents.x);
            rY = Random.Range(transform.position.y - myBC.bounds.extents.y, transform.position.y + myBC.bounds.extents.y);
            rZ = Random.Range(transform.position.z - myBC.bounds.extents.z, transform.position.z + myBC.bounds.extents.z);
            spawnLoc = new Vector3(rX, rY, rZ);

            spawn = Instantiate(common, spawnLoc, Random.rotation, transform) as GameObject;
            spawn.GetComponent<TreasureCheck>().myTC = gameObject.GetComponent<TreasureController>();
            spawn.GetComponent<TreasureCheck>().myType = "common";
            spawn.GetComponent<TreasureCheck>().arcGoTo = cArc;
        }
    }

    void SpawnUncommon()
    {
        Vector3 spawnLoc;
        float rX, rY, rZ;
        GameObject spawn;

        for (int i = 0; i < 5; i++)
        {
            rX = Random.Range(transform.position.x - myBC.bounds.extents.x, transform.position.x + myBC.bounds.extents.x);
            rY = Random.Range(transform.position.y - myBC.bounds.extents.y, transform.position.y + myBC.bounds.extents.y);
            rZ = Random.Range(transform.position.z - myBC.bounds.extents.z, transform.position.z + myBC.bounds.extents.z);
            spawnLoc = new Vector3(rX, rY, rZ);

            spawn = Instantiate(uncommon, spawnLoc, Random.rotation, transform) as GameObject;
            spawn.GetComponent<TreasureCheck>().myTC = gameObject.GetComponent<TreasureController>();
            spawn.GetComponent<TreasureCheck>().myType = "uncommon";
            spawn.GetComponent<TreasureCheck>().arcGoTo = uArc;
        }
    }

    void SpawnRare()
    {
        Vector3 spawnLoc;
        float rX, rY, rZ;
        GameObject spawn;

        for (int i = 0; i < 3; i++)
        {
            rX = Random.Range(transform.position.x - myBC.bounds.extents.x, transform.position.x + myBC.bounds.extents.x);
            rY = Random.Range(transform.position.y - myBC.bounds.extents.y, transform.position.y + myBC.bounds.extents.y);
            rZ = Random.Range(transform.position.z - myBC.bounds.extents.z, transform.position.z + myBC.bounds.extents.z);
            spawnLoc = new Vector3(rX, rY, rZ);

            spawn = Instantiate(rare, spawnLoc, Random.rotation, transform) as GameObject;
            spawn.GetComponent<TreasureCheck>().myTC = gameObject.GetComponent<TreasureController>();
            spawn.GetComponent<TreasureCheck>().myType = "rare";
            spawn.GetComponent<TreasureCheck>().arcGoTo = rArc;
        }
    }

    public void DestroyAllCollectibles()
    {
        int childCount = transform.childCount;
        List<GameObject> children = new List<GameObject>();

        for(int i = 0; i < childCount; i++)
        {
            children.Add(transform.GetChild(i).gameObject);
        }

        foreach(GameObject treasure in children)
        {
            if(treasure != null && treasure.tag != "Arc")
            {
                Destroy(treasure);
            }
        }

        SpawnCommon();
        SpawnUncommon();
        SpawnRare();
    }
}
