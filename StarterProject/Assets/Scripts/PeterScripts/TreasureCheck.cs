﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TreasureCheck : MonoBehaviour
{

    [HideInInspector]
    public TreasureController myTC;
    [HideInInspector]
    public string myType;
    bool move = false;
    bool chosenTransform;
    bool moveArc = false;
    Quaternion goToRot;
    Vector3 goToPos;
    public Transform arcGoTo;
    float arcY;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!move && Uncovered())
        {
            move = true;
        }
        else if (move)
        {
            Invoke("MoveToPile", 1f);
        }
    }

    bool Uncovered()
    {
        Ray ray = new Ray(transform.position, Vector3.up);

        if (Physics.Raycast(ray, 20f))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void MoveToPile()
    {
        if (!chosenTransform)
        {
            if (myType == "common")
            {
                for (int i = 0; i < myTC.tCommon.Length; i++)
                {
                    if (myTC.bCommon[i])
                    {
                        continue;
                    }
                    else if (!myTC.bCommon[i])
                    {
                        myTC.bCommon[i] = true;
                        chosenTransform = true;
                        goToPos = myTC.tCommon[i].position;
                        goToRot = myTC.tCommon[i].rotation;
                        myTC.foundRubys.Add(gameObject);
                       // GM_Dupe.instance.canvas.SendMessage("UpdateTreasureText");
                        break;
                    }
                }
            }
            else if (myType == "uncommon")
            {
                for (int i = 0; i < myTC.tUncommon.Length; i++)
                {
                    if (myTC.bUncommon[i])
                    {
                        continue;
                    }
                    else if (!myTC.bUncommon[i])
                    {
                        myTC.bUncommon[i] = true;
                        chosenTransform = true;
                        goToPos = myTC.tUncommon[i].position;
                        goToRot = myTC.tUncommon[i].rotation;
                        myTC.foundBullions.Add(gameObject);
                        //GM_Dupe.instance.canvas.SendMessage("UpdateTreasureText");
                        break;
                    }
                }
            }
            else if (myType == "rare")
            {
                for (int i = 0; i < myTC.tRare.Length; i++)
                {
                    if (myTC.bRare[i])
                    {
                        continue;
                    }
                    else if (!myTC.bRare[i])
                    {
                        myTC.bRare[i] = true;
                        chosenTransform = true;
                        goToPos = myTC.tRare[i].position;
                        goToRot = myTC.tRare[i].rotation;
                        myTC.foundDiamonds.Add(gameObject);
                       // GM_Dupe.instance.canvas.SendMessage("UpdateTreasureText");
                        break;
                    }
                }
            }
        }
        else
        {
            if(!moveArc)
            {
                transform.position = Vector3.Lerp(transform.position, arcGoTo.position, Time.deltaTime * 2f);
                if (transform.position.y >= 3f)
                {
                    moveArc = true;
                }
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, goToPos, Time.deltaTime * 1.5f);
            }
            transform.rotation = Quaternion.Lerp(transform.rotation, goToRot, Time.deltaTime * 1.5f);
        }
    }
}
