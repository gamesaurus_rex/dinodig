﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAnimation : MonoBehaviour {

    public GameObject particle;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisableBlock()
    {
        Instantiate(particle, transform.position, Quaternion.Euler(-90,0,0));
        gameObject.SetActive(false);
    }
}
