﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM_Dupe : MonoBehaviour {

    public GameObject TC_Dupe;
    public List<GameObject> pressedTiles = new List<GameObject>();
    public List<GameObject> inactiveTiles = new List<GameObject>();
    public List<GameObject> allTiles = new List<GameObject>();
    public static GM_Dupe instance = null;
    public float bottomLayerReturnTimeSeconds, midLayer4ReturnTimeSeconds, midLayer3ReturnTimeSeconds, midLayer2ReturnTimeSeconds, topLayerReturnTimeSeconds, ddTimer;
    public float tileReturnTimer = 0f;
    public Canvas canvas;
    public GameObject[] devilWaypoints;
    public GameObject[] devilDestinations;
    public GameObject dustDevil;
    GameObject spawn;
    //[HideInInspector]
    public bool devilSpawned = false;
    public bool awaitingInput = false;
    public TreasureController myTC;

    public GameObject trexModel, pteradonModel;
    public int godValue;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        FossilDecider();

        allTiles.AddRange(GameObject.FindGameObjectsWithTag("Tile"));

        Application.runInBackground = true;
    }

    public void FossilDecider()
    {
        godValue = Random.Range(1, 3);
        print("God Value Chosen");
        if (godValue == 1)
        {
            trexModel.SetActive(true);
        }
        else
        {
            trexModel.SetActive(false);
        }
        if (godValue == 2)
        {
            pteradonModel.SetActive(true);
        }
        else
        {
            pteradonModel.SetActive(false);
        }

    }

    void Update()
    {
        FloorPadInput.GetEvents(gameObject);
        tileReturnTimer += Time.deltaTime;
        if (tileReturnTimer >= bottomLayerReturnTimeSeconds && !awaitingInput)
        {
            TileRespawn();
        }
        else if(tileReturnTimer >= bottomLayerReturnTimeSeconds / 10f && awaitingInput)
        {
            TileRespawn();
        }

        if(tileReturnTimer >= ddTimer && !devilSpawned)
        {
            int randSpawn = Random.Range(0, devilWaypoints.Length);

            devilSpawned = true;
            awaitingInput = true;

            print("got in");
            spawn = Instantiate(dustDevil, devilWaypoints[randSpawn].transform.position, Quaternion.Euler(-90, 0, 0)) as GameObject;
            spawn.GetComponent<DustDevilController>().destination = devilDestinations[randSpawn];
            spawn.GetComponent<DustDevilController>().myGM = this;
            
        }
        else if(tileReturnTimer >= ddTimer / 10 && awaitingInput && !devilSpawned)
        {
            int randSpawn = Random.Range(0, devilWaypoints.Length);

            devilSpawned = true;
            awaitingInput = true;

            spawn = Instantiate(dustDevil, devilWaypoints[randSpawn].transform.position, Quaternion.Euler(-90, 0, 0)) as GameObject;
            spawn.GetComponent<DustDevilController>().destination = devilDestinations[randSpawn];
            spawn.GetComponent<DustDevilController>().myGM = this;
        }

    }


    void OnTileDown(Vector2 coords)
    {
        //tileReturnTimer = 0;
        //Debug.Log(coords);
        //tileNum++;
        if(awaitingInput)
        {
            awaitingInput = false;
            BoardReset();
            devilSpawned = false;
        }
        DropTileChecker(new Vector3(coords.x, 1, -coords.y));
    }

    void OnTileUp(Vector2 coords)
    {
        List<GameObject> itemsToRemove = new List<GameObject>();

        if (pressedTiles.Count != 0)
        {
            foreach (GameObject tcd in pressedTiles)
            {
                if (tcd != null)
                {
                    if (tcd.transform.position.x == coords.x && -tcd.transform.position.z == coords.y)
                    {
                        itemsToRemove.Add(tcd);
                        //tcd.GetComponent<TC_Dupe>().DestroySelf();
                    }
                }
            }

            foreach(GameObject item in itemsToRemove)
            {
                pressedTiles.Remove(item);
                item.GetComponent<TC_Dupe>().DestroySelf();
            }
        }
    }

    public void DropTileChecker(Vector3 coordinates)
    {
        GameObject spawned;

        if (FloorPadInput.GetTile((int)coordinates.x, -(int)coordinates.z))
        {
            spawned = Instantiate(TC_Dupe, coordinates, TC_Dupe.transform.rotation) as GameObject;
            pressedTiles.Add(spawned);
        }
    }

    void TileRespawn()
    {
        foreach (GameObject tile in inactiveTiles)
        {
            if (tile.transform.parent.tag == "Bottom Layer")
            {
                tile.SetActive(true);
            }

            if (tileReturnTimer >= midLayer4ReturnTimeSeconds && tile.transform.parent.tag == "Mid Layer 4" && !awaitingInput)
            {
                tile.SetActive(true);
            }
            else if(tileReturnTimer >= midLayer4ReturnTimeSeconds / 10 && tile.transform.parent.tag == "Mid Layer 4" && awaitingInput)
            {
                tile.SetActive(true);
            }

            if (tileReturnTimer >= midLayer3ReturnTimeSeconds && tile.transform.parent.tag == "Mid Layer 3" && !awaitingInput)
            {
                tile.SetActive(true);
            }
            else if(tileReturnTimer >= midLayer3ReturnTimeSeconds / 10 && tile.transform.parent.tag == "Mid Layer 3" && awaitingInput)
            {
                tile.SetActive(true);
            }

            if (tileReturnTimer >= midLayer2ReturnTimeSeconds && tile.transform.parent.tag == "Mid Layer 2" && !awaitingInput)
            {
                tile.SetActive(true);
            }
            else if(tileReturnTimer >= midLayer2ReturnTimeSeconds / 10 && tile.transform.parent.tag == "Mid Layer 2" && awaitingInput)
            {
                tile.SetActive(true);
            }

            if (tileReturnTimer >= topLayerReturnTimeSeconds && tile.transform.parent.tag == "Top Layer" && !awaitingInput)
            {
                tile.SetActive(true);
            }
            else if(tileReturnTimer >= topLayerReturnTimeSeconds / 10 && tile.transform.parent.tag == "Top Layer" && awaitingInput)
            {
                tile.SetActive(true);
            }

            tile.GetComponent<TO_Dupe>().RestoreHealth();
            tile.GetComponent<TO_Dupe>().canDamage = true;
        }
        for (int i = 0; i < inactiveTiles.Count; i++)
        {
            if (inactiveTiles[i].activeSelf == true)
            {
                inactiveTiles.Remove(inactiveTiles[i]);
            }
        }
    }

    public void BoardReset()
    {
        if(spawn != null)
        {
            Destroy(spawn);
        }

        myTC.DestroyAllCollectibles();

        foreach(GameObject tile in allTiles)
        {
            tile.SetActive(true);
            tile.GetComponent<TO_Dupe>().RestoreHealth();
            tile.GetComponent<TO_Dupe>().canDamage = true;
        }
        awaitingInput = false;
    }
}
