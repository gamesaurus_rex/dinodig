﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TC_Dupe : MonoBehaviour
{
    float waitTimer = 0f;
    public GameObject boxPrefab;
    bool timerActive = true;
    bool lavaParticleSpawned;
    public GameObject  lavaParticleSystem ;


    // Use this for initialization
    void Start()
    {
        lavaParticleSpawned = false;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DestroySelf()
    {
        //Debug.Log("Born and Died: " + transform.position.x.ToString() + " , " + transform.position.z.ToString());
        Destroy(this.gameObject);
    }

    public void FixedUpdate()
    {
        Ray ray = new Ray(transform.position, -Vector3.up);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 40f, 1 << 8, QueryTriggerInteraction.Collide))
        {
            if (hit.collider.isTrigger && hit.collider.gameObject.tag == "Tile" && hit.collider.gameObject != null)
            {
                hit.collider.gameObject.GetComponent<TO_Dupe>().RayCastHit();
                //Debug.Log("hit something: " + hit.point);
                //Debug.DrawLine(transform.position, hit.point, Color.red);
            }
            else if((hit.collider.gameObject.tag == "Bottom Layer" || hit.collider.gameObject.tag== "BottomFossil") && timerActive)
            {
                waitTimer += Time.deltaTime;
                if(waitTimer >= 20f)
                {
                    Vector3 spawnHere = new Vector3(hit.transform.position.x, hit.transform.position.y + 0.3f, hit.transform.position.z);
                    Instantiate(boxPrefab, spawnHere, Quaternion.Euler(0, 0, 0), transform);
                    timerActive = false;
                }
                if (hit.collider.gameObject.tag == "Bottom Layer" && !lavaParticleSpawned)
                {
                    Instantiate(lavaParticleSystem, hit.transform.position + new Vector3( 0,1,0), Quaternion.Euler(0, 0, 0), transform);
                    lavaParticleSpawned = true;
                }
            }
        }
    }

    /*
    public void CheckTiles()
    {
        Ray ray = new Ray(transform.position, -Vector3.up);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 10f, 1 << 8, QueryTriggerInteraction.Collide))
        {
            if (hit.collider.isTrigger && hit.collider.gameObject.tag == "Tile")
            {
                hit.collider.gameObject.GetComponent<TO_Dupe>().RayCastHit();
                //Debug.Log("hit something: " + hit.point);
                //Debug.DrawLine(transform.position, hit.point, Color.red);
            }
        }
    }
    */
}
