﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitController : MonoBehaviour {

    public float speed;
    public List<GameObject> layer1 = new List<GameObject>();
    public List<GameObject> layer2 = new List<GameObject>();
    public List<GameObject> layer3 = new List<GameObject>();
    public List<GameObject> layer4 = new List<GameObject>();
    bool inProgress = false;

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.localScale = new Vector3(transform.localScale.x + Time.deltaTime * speed, transform.localScale.y, transform.localScale.z + Time.deltaTime * speed);
	}

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Tile")
        {
            
            if (other.transform.parent.tag == "Top Layer")
            {
                
                if (!layer1.Contains(other.gameObject))
                {
                    layer1.Add(other.gameObject);
                }
            }
            else if (other.transform.parent.tag == "Mid Layer 2")
            {
                
                if (!layer2.Contains(other.gameObject))
                {
                    layer2.Add(other.gameObject);
                }
            }
            else if (other.transform.parent.tag == "Mid Layer 3")
            {
                
                if (!layer3.Contains(other.gameObject))
                {
                    layer3.Add(other.gameObject);
                }
            }
            else if (other.transform.parent.tag == "Mid Layer 4")
            {
                
                if (!layer4.Contains(other.gameObject))
                {
                    layer4.Add(other.gameObject);
                }
            }
            if (!inProgress)
            {
                inProgress = true;
                DestroyTiles();
            }
        }
    }

    void DestroyTiles()
    {
        GameObject checkingThis;

        if (layer1.Count > 0)
        {
            for (int i = 0; i < layer1.Count; i++)
            {
                checkingThis = layer1[i];
                if (layer1[i].activeSelf == false)
                {
                    layer1.Remove(checkingThis);
                }
                else
                {
                    layer1[i].GetComponent<TO_Dupe>().ManualCheck();
                    break;
                }
            }
        }
        else if(layer2.Count > 0 && layer1.Count == 0)
        {
            for (int i = 0; i < layer2.Count; i++)
            {
                checkingThis = layer2[i];
                if (layer2[i].activeSelf == false)
                {
                    layer2.Remove(checkingThis);
                }
                else
                {
                    layer2[i].GetComponent<TO_Dupe>().ManualCheck();
                    break;
                }
            }
        }
        else if(layer3.Count > 0 && layer1.Count == 0 && layer2.Count == 0)
        {
            for (int i = 0; i < layer3.Count; i++)
            {
                checkingThis = layer3[i];
                if (layer3[i].activeSelf == false)
                {
                    layer3.Remove(checkingThis);
                }
                else
                {
                    layer3[i].GetComponent<TO_Dupe>().ManualCheck();
                    break;
                }
            }
        }
        else if(layer4.Count > 0 && layer1.Count == 0 && layer2.Count == 0 && layer3.Count == 0)
        {
            for (int i = 0; i < layer4.Count; i++)
            {
                checkingThis = layer4[i];
                if (layer4[i].activeSelf == false)
                {
                    layer4.Remove(checkingThis);
                }
                else
                {
                    layer4[i].GetComponent<TO_Dupe>().ManualCheck();
                    break;
                }
            }
        }
        inProgress = false;
    }
}
