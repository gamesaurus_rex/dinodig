﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TO_Dupe : MonoBehaviour
{

    public GameObject particle;
    Animator myAnim;
    public bool canDamage = true;

    int health;
    public int maxHealth;

    private void Awake()
    {
        myAnim = GetComponent<Animator>();
    }

    // Use this for initialization
    void Start()
    {
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void RayCastHit()
    {
        if (canDamage)
        {
            canDamage = false;
            AnimateDeath();
            //Debug.Log("tileHit");
        }
    }

    public void ManualCheck()
    {
        if (canDamage)
        {
            canDamage = false;
            AnimateDeath();
            //Debug.Log("tileHit");
        }
    }

    public void AnimateDeath()
    {
        //Destroy(this.gameObject);
        //gameObject.SetActive(false);
        myAnim.SetTrigger("BlockDeath");
    }

    void ParticleSpawn()
    {
        //print(health);
        if (health <= 0)
        {
            Instantiate(particle, transform.position, Quaternion.Euler(-90, 0, 0));
            GM_Dupe.instance.inactiveTiles.Add(gameObject);
            GM_Dupe.instance.tileReturnTimer = 0f;
            gameObject.SetActive(false);
        }
    }

    void TakeDamage()
    {
        canDamage = true;
    }

    void IgnoreDamage()
    {
        canDamage = false;
        health -= 1;
    }

    public void RestoreHealth()
    {
        health = maxHealth;
    }
}
