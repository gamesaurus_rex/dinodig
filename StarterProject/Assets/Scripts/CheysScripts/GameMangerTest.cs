﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMangerTest : MonoBehaviour {

    public GameObject TileChecker;

    // Use this for initialization
    void Awake()
    {
        Application.runInBackground = true;
    }

    void Update()
    {
        FloorPadInput.GetEvents(gameObject);
    }


    void OnTileDown( Vector2 coords)
    {
        Debug.Log(coords);
        DropTileChecker( new Vector3 (coords.x, 1, -coords.y));
    }

    public void DropTileChecker( Vector3 coordinates)
    {
        Instantiate(TileChecker, coordinates, TileChecker.transform.rotation);
    }

    void OnTileUp(Vector2 coords)
    {

    }


}
