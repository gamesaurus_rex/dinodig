﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileChecker : MonoBehaviour {

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		
	}




    void DestroySelf()
    {
        Debug.Log("Born and Died: " + transform.position.x.ToString() + " , " + transform.position.z.ToString());
        Destroy(this.gameObject);

    }



    public void FixedUpdate()
    {


        Ray ray = new Ray(transform.position, -Vector3.up);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit,10f,1<<8 , QueryTriggerInteraction.Collide  ))
        {
            if (hit.collider.isTrigger && hit.collider.gameObject .tag=="Tile")
            {
                hit.collider.gameObject.GetComponent<TileObject>().RayCastHit(); 
                Debug.Log("hit something: " + hit.point);
                Debug.DrawLine(transform.position, hit.point, Color.red);
            }

        }
        DestroySelf();
    }


}
