﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileObject : MonoBehaviour {
    
    public int Health;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Health <= 0)
        {
            Death();
        }
	}


    public void RayCastHit()
    {
        Health -= 1;

        Debug.Log("tileHit");

    }

    public void Death()
    {
        Destroy(this.gameObject);
    }

}
