﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetFoundTreasure : MonoBehaviour {

    public TreasureController TC;
    Text treasureFound;
    int setTreasure;

    void Start()
    {
        treasureFound = GetComponent<Text>();
        if (gameObject.name == "Rubys Found")
        {
            setTreasure = 1;
            treasureFound.text = TC.foundRubys.Count + "/7";
        }
        if (gameObject.name == "Bullions Found")
        {
            setTreasure = 2;
            treasureFound.text = TC.foundBullions.Count + "/5";
        }
        if (gameObject.name == "Diamonds Found")
        {
            setTreasure = 3;
            treasureFound.text = TC.foundDiamonds.Count + "/3";
        }
    }

    void SetText()
    {
        if (setTreasure == 1)
        {
            treasureFound.text = TC.foundRubys.Count + "/7";
        }
        if (setTreasure == 2)
        {
            treasureFound.text = TC.foundBullions.Count + "/5";
        }
        if (setTreasure == 3)
        {
            treasureFound.text = TC.foundDiamonds.Count + "/3";
        }

    }
}
